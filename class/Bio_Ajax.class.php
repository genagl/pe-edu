<?php
class ExceptionNotLogged extends Exception
{
    public function Send()
    {
        // отсылаем содержимое ошибки на email админа
    }
}
class ExceptionNotAdmin extends Exception
{
    public function Send()
    {
        // отсылаем содержимое ошибки на email админа
    }
}
class Bio_ajax
{
	static $instance;
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	function __construct()
	{
		add_action('wp_ajax_nopriv_myajax',		array(__CLASS__, 'ajax_submit') );
		add_action('wp_ajax_myajax',			array(__CLASS__, 'ajax_submit') );
		add_action('wp_ajax_myajax-admin', 		array(__CLASS__, 'ajax_submit'));		
	}
	static function ajax_submit()
	{
		try
		{
			static::myajax_submit();
		}
		catch(ExceptionNotLogged $ew)
		{
			$d	=  array(	
				-1,
				array(
						'a_alert'	=> __("Only logged users may do this!", BIO)
					  )
			);
			$d_obj	= json_encode(apply_filters("bio_ajax_data_not_owner", $d, $params));
			print $d_obj;
			exit;
		}
		catch(ExceptionNotAdmin $ew)
		{
			$d	=  array(	
				-2,
				array(
						'a_alert'	=> __("Only Administrator may do this!", BIO)
					  )
			);
			$d_obj	= json_encode(apply_filters("bio_ajax_data_not_admin", $d, $params));
			print $d_obj;
			exit;
		}
		catch(Error $ex)
		{
			$d = [	
				"Error",
				array(
					'msg'	=> $ex->getMessage (),
					'log'	=> $ex->getTrace ()
				  )
			];
			$d_obj		= json_encode( $d );				
			wp_die( $d_obj );
		}
		wp_die();
	}
	static function myajax_submit()
	{
		global $wpdb;
		$nonce = $_POST['nonce'];
		if ( !wp_verify_nonce( $nonce, 'myajax-nonce' ) ) die ( $_POST['params'][0] );
		
		$params	= $_POST['params'];	
		$d		= array( $_POST['params'][0], array() );				
		switch($params[0])
		{				
			case "test":	
				$d = array(	
					$params[0],
					array( 
						"text"		=> 'testing',
					)
				);
				break;		
			case "bio_article_more":
				$aid 	= $params[1];
				$art	= Bio_Article::get_instance( $aid );
				$d = array(	
					$params[0],
					array( 
						"id"		=> $aid,
						"text"		=> 'testing',
					)
				);
				break;			
			case "bio_change_cat_article":	// admin
				if( !current_user_can( "manage_options" ) )	
					throw new ExceptionNotAdmin();
				$id 	= $params[1];
				$cat 	= $params[2];
				$ffv	= wp_set_object_terms(
					$id,
					(int)$cat,
					"category"
				);
				$d = array(	
					$params[0],
					array( 
						"msg"		=> __( $ffv ? "Successfull" : "Error", BIO),
						"text"		=> $ffv,
					)
				);
				break;			
			case "bio_edit_mailing":
				$data		= $params[1];
				$post_id	= $params[2];

				if($post_id > 1)
				{
					$mailing = Bio_Mailing::get_instance($post_id);
					$id = Bio_Mailing::update( $data, $post_id );
					$text = __("Update Mailing successfull", BIO);
				}
				else
				{
					$data['post_name'] = $data['post_title'];
					$mailing = Bio_Mailing::insert($data);
					$text = __("Insert new Mailing", BIO);
				}
				$d = array(	
					$params[0],
					array( 
						"msg"		=> $text,
						"text"		=> $mailing->get_admin_form()
					)
				);
				break;				
			case "bio_edit_question":
				$data 	= $params[1];
				$qid	= $params[2];
				$data	=  Bio_Question::edit($data, $qid);
				$d = array(	
					$params[0],
					array( 
						"qid"		=> $qid,
						"data"		=> $data,
						"msg"		=> __("Question saccessful updated", BIO)
					)
				);
				break;				
			case "bio_event_request":	
				$data		= $params[1];
				$event		= Bio_Event::get_instance($data["event_id"]);
				$u			= $event->send_request($data);
				$d = array(	
					$params[0],   
					[
						"u" => $u
					]
				);
				break;					
			case "bio_conglom_user_access":	
				$post_id	= $params[1];
				$user_id	= $params[2];
				$post		= get_post($post_id);
				$class		= Bio_Conglomerath::get_class( $post->post_type );
				$ret		= forward_static_call_array( [
					Bio_Conglomerath::get_class( $post->post_type ), 
					"set_access_request"
				], 
				[ 
					$post_id, 
					$user_id 
				]);
				$d = array(	
					$params[0],
					array( 
						"text" 			=> forward_static_call_array( [$class, "users_box_func"], [ $post, false ] ),
						"indic_id"		=> "indic_lead_events",
						"indic"			=> forward_static_call_array( [$class, "get_requests_count"], [ ] ),
						"msg"			=> Bio_Messages::successful_conglom_access()
					)
				);
				break;			
			case "bio_course_user_access":	
				require_once( BIO_REAL_PATH . "class/Bio_Cabinet.class.php");
				$course_id = $params[1];
				$user_id = $params[2];
				if(Bio_Course::is_user_author($course_id))
				{
					$succ = Bio_Course::accept_permission($course_id, $user_id);
					$msg = __($succ ?  "The user has become a listener for your course. A message about changing his status sent to his email" : "Unknown error!" , BIO);
				}
				else
				{
					$msg = __("You ar HUCKER! It's very bad!", BIO);
				}
				$d = array(	
					$params[0],
					array( 
						"msg"		=> $msg,
						"indic"		=> $wpdb->get_var( Bio_Cabinet::get_tab( "lead_courses" )[ "req" ] ),
						"req"		=> Bio_Cabinet::get_tab( "lead_courses" )[ "req" ],
						"text"		=> Bio_Course::get_admin_pupils_form( $course_id )
					)
				);
				break;	
			case "bio_admin_test_question":	
				$n = $params[1];
				$question_id = $params[2];
				$question	= Bio_Question::get_instance( $question_id );
				$d = array(	
					$params[0],
					array( 
						"text"		=> $question->get_admin_form2(),
						"n"			=> $n
					)
				);
				break;				
			case "bio_edit_course":	
				$data		= $params[1];
				$post_id	= $params[2];
				$data['lock']	= (int)$data['lock'];
				if($post_id>1)
				{
					$id = Bio_Course::update( $data, $post_id );
					$text = __("Update Course successfull", BIO);
				}
				else
				{
					$data['post_type'] 	= Bio_Course::get_type();
					$data['name'] 		= $data['name'];
					$id = Bio_Course::insert($data);
					$text = __("Insert new Course", BIO);
				}
				$d = array(	
					$params[0],
					array( 
						"msg"		=> $text,
						"id"		=> $id,
						"data"		=> $data
					)
				);
				break;					
			case "bio_edit_article":	
				$data		= $params[1];
				$post_id	= $params[2];
				if($post_id>1)
				{
					$id = Bio_Article::update( $data, $post_id );
					$text = __("Update Article successfull", BIO);
				}
				else
				{
					$data['post_type'] = Bio_Article::get_type();
					$data['post_name'] = $data['post_title'];
					$id = Bio_Article::insert($data);
					$text = __("Insert new Article", BIO);
				}
				$d = array(	
					$params[0],
					array( 
						"msg"		=> $text,
					)
				);
				break;		
			case "get_lead_articles_cab":	
				$val = $params[1];
				$article = Bio_Article::get_instance($val);
				if($val == -1 || $article->is_user_author( ))
				{
					$html = $article->get_admin_form( );
				}
				else
				{
					$html = __("You not owner!", BIO);
				}
				$d = array(	
					$params[0],
					array( 
						"text"		=> $html
					)
				);
				break;	
			case "get_lead_question_cab":	
				$val = $params[1];
				$event = Bio_Question::get_instance($val);
				$html = $event->get_admin_form( );
				$d = array(	
					$params[0],
					array( 
						"text"		=> $html
					)
				);
				break;	
			case "get_lead_events_cab":	
				$val = $params[1];
				$event = Bio_Event::get_instance($val);
				if($val == -1 || $event->is_user_author( ))
				{
					$html = $event->get_admin_form( );
				}
				else
				{
					$html = __("You not owner!", BIO);
				}
				$d = array(	
					$params[0],
					array( 
						"text"		=> $html
					)
				);
				break;	
			case "get_lead_mailing_cab":	
				$val = $params[1];
				$event = Bio_Mailing::get_instance($val);
				if($val == -1 || $event->is_user_author( ))
				{
					$html = $event->get_admin_form( );
				}
				else
				{
					$html = __("You not owner!", BIO);
				}
				$d = array(	
					$params[0],
					array( 
						"text"		=> $html
					)
				);
				break;	
			case "get_lead_tests_cab":	
				$val = $params[1];
				$test = Bio_Test::get_instance($val);
				if($val == -1 || $test->is_user_author( ))
				{
					$html = $test->get_admin_form( );
				}
				else
				{
					$html = __("You not owner!", BIO);
				}
				$d = array(	
					$params[0],
					array( 
						"text"		=> $html
					)
				);
				break;			
			case "cab_choose_lead_course":	
				$val = $params[1];
				if($val == -1 || Bio_Course::is_user_author($val))
				{
					$html = Bio_Course::get_admin_form( $val );
				}
				else
				{
					$html = __("You not owner!", BIO);
				}
				$d = array(	
					$params[0],
					array( 
						"text"		=> $html
					)
				);
				break;			
			case "bio_publish_id":
				$post_id  	= $params[1];
				$is_publish = $params[2];
				wp_update_post([
					"ID"			=> $post_id,
					"post_status"	=> $is_publish ? "publish" : "draft"
				]);
				$d = array(	
					$params[0],
					array( 
						"text"		=> $is_publish,
					)
				);
				break;			
			case "get_lead_courses_cab":
				require_once BIO_REAL_PATH . "class/Bio_Cabinet.class.php";
				
				$d = array(	
					$params[0],
					array( 
						"text"		=> Bio_Cabinet::get_lead_courses_cab(),
					)
				);
				break;				
			case "choose_lead_courses_cab":
				require_once BIO_REAL_PATH . "class/Bio_Cabinet.class.php";
				
				$d = array(	
					$params[0],
					array( 
						"text"		=> Bio_Cabinet::get_lead_courses_cab(),
					)
				);
				break;			
			case "bio_export_android":
				$d = array(	
					$params[0],
					array( 
						"msg"		=> 'This Ajax command not work now',
					)
				);
				break;			
			case "get_my_courses_cab":
				require_once BIO_REAL_PATH . "class/Bio_Cabinet.class.php";
					
				$d = array(	
					$params[0],
					array( 
						"msg"		=> 'get_my_courses_cab',
					)
				);
				break;			
			case "add_course":
				if(!is_user_logged_in())	throw new ExceptionNotLogged();
				$course_id = $params[1];
				$is = Bio_Course::request_permission($course_id);
				$d = array(	
					$params[0],
					array( 
						"msg"	=> __($is ? "Succesful send your request. Wait for answer from Course Leader.":"Error!", BIO),
						"text"	=> Bio_Messages::alredy_request($course_id),
						'is' => $is
					)
				);
				break;			
			case "leave_req":
			case "leave_course":
				if(!is_user_logged_in())	throw new ExceptionNotLogged();
				$course_id = $params[1];
				$is = Bio_Course::withdraw_permission($course_id);
				$d = array(	
					$params[0],
					array( 
						"msg"	=> __($is ? "Succesful leave your request.":"Error!", BIO),
						"text"	=> Bio_Messages::send_request($course_id)
					)
				);
				break;					
			case "bio_role_user_id":	
				$data = $params[1];
				$user = get_user_by("id", $data['role_user_id']);
				if( $data['is_check'] )
				{					
					$user->add_role($data['role']);
					$text = sprintf(__("Add %s role for %s", BIO), "<b>".$data['role']."</b>", $user->display_name);
				}
				else
				{
					$user->remove_role($data['role']);
					$text = sprintf(__("Remove %s role for %s", BIO), "<b>".$data['role']."</b>", $user->display_name);
				}
				$d = array(	
					$params[0],
					array( 
						"msg"		=> $text,
					)
				);
				break;				
			case "bio_cab_page":	
				$page_id = $params[1];
				require_once BIO_REAL_PATH. "class/Bio_Cabinet.class.php";
				
				$d = array(	
					$params[0],
					array( 
						"text"		=> Bio_Cabinet::get_plate( $page_id ),
						"page_id"	=> $page_id
					)
				);
				break;			
			case "bio_options":	
				$name = $params[1];
				$val  = $params[2];
				Bio::$options[$name] = $val;
				update_option(BIO, Bio::$options);
				if( $name == "menu")
				{
					$vw 	= [];
					foreach($val as $k => $v)
					{						
						$f				= $v;
						$thumbnail		= wp_get_attachment_image_src( $v['icon'], "full" )[0];
						$f['icon']		= $thumbnail ? $thumbnail : BIO_EMPTY_IMG;
						$vw[]			= $f;
					}
					file_put_contents( ABSPATH."static/json/menu.json", json_encode( $vw ) );
				}
				$d = array(	
					$params[0],
					array( 
						"text"		=> 'bio_options',
						$name		=> $vw
					)
				);
				break;		
			case "bio_test_next":
				if(!is_user_logged_in())	throw new ExceptionNotLogged();
				$data 		= $params[1];
				$test_id 	= $data['test_id'];
				$current 	= get_post_meta((int)$data['current'], "order", true);
				$chk 		= $data['chk'];
				$test 		= Bio_Test::get_instance( $test_id );
				$d = array(	
					$params[0],
					array( 
						"chk"		=> $chk,
						"current"	=> $current,
						"text"		=> $test->get_count_questions() >= $current + 1 ? 
							$test->draw( $current + 1 ) : 
							"<div class='wobble_2 col-12' style=' align-items: center; justify-content: center; display: flex; flex-direction: column;'>
								<div class='display-4'>" .
									__("Thanks. It's all.", BIO) .
								"</div>
							</div>"
					)
				);
				break;	
			case "bio_exec_ajax":
			{
				if(!current_user_can("manage_options"))
						throw new ExceptionNotAdmin();
				$d = apply_filters("", [], $params);
				break;
			}				
			case "favor_article":	
				if(!is_user_logged_in())	throw new ExceptionNotLogged();
				$article_id = $params[1]['id']; 
				$check		= $params[1]['chk'];
				$article	= Bio_Article::get_instance($article_id);
				$is_check	= $article->add_to_favorite($check);
				$label = !$check ? "<i class='fas fa-heart text-danger' title='" . __('In my favorites', BIO)."'></i>"  : "<i class='far fa-heart'' title='" . __("Add to favorites", BIO) ."'></i>" ;
				$d = array(	
					$params[0],
					array( 
						"msg"		=> !$check ? __('Added to your favorites', BIO) : __('Remove from your favorites', BIO),
						"label"		=> $label,
						"chk"		=> $check,
						"id"		=> $article_id,
						"is_check"	=> $is_check
					)
				);
				break;		
			default:
				$d = apply_filters("bio_ajax_submit", $d, $params);
				break;
		}
		$d_obj		= json_encode(apply_filters("bio_ajax_data", $d, $params));				
		print $d_obj;
		wp_die();
	}
}
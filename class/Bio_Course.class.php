<?php
class Bio_Course extends SMC_Taxonomy
{
	
	static function get_type()
	{
		return BIO_COURSE_TYPE;
	}
	static function init()
	{
		add_action( 'init', 					array( __CLASS__, 'create_taxonomy'), 12);
		add_action( 'parent_file',				array( __CLASS__, 'tax_menu_correction'), 1);	
		add_action( 'admin_menu', 				array( __CLASS__, 'tax_add_admin_menus'), 11);
		
		//add_filter( "bio_gq_term_meta", 	array( __CLASS__,'bio_gq_term_meta'), 10, 4); 
		add_filter( "manage_edit-".BIO_COURSE_TYPE."_columns", 	[ __CLASS__, 'ctg_columns']); 
		add_filter( "manage_".BIO_COURSE_TYPE."_custom_column",	[ __CLASS__, 'manage_ctg_columns'], 11.234, 3);
		add_action( BIO_COURSE_TYPE.'_edit_form_fields', 		[ __CLASS__, 'add_ctg'], 2, 2 );
		add_action( 'edit_'.BIO_COURSE_TYPE, 					[ __CLASS__, 'save_ctg'], 10);  
		add_action( 'create_'.BIO_COURSE_TYPE, 					[ __CLASS__, 'save_ctg'], 10);			
		add_action( 'delete_'.BIO_COURSE_TYPE,					[ __CLASS__, "delete_term"], 10, 5);
		
		add_filter( "get_all_taxonomies_args",					[ __CLASS__, "get_all_taxonomies_args"], 10, 2);
	}
	
	static function get_all_taxonomies_args( $args, $pars )
	{
		if( current_user_can("manage_options"))	return $args;
		if( $pars['is_admin'] && $args['taxonomy']  == static::get_type() )
		{			
			if( !isset($args['metas']) )
			{
				$args['metas'] = [];
			}
			$facultet_ids = get_terms([
				'taxonomy' 		=> BIO_FACULTET_TYPE,
				"fields"		=> "ids",
				'meta_query'	=> [
					"relation"	=> "OR",
					[
						"key"		=> "post_author", 
						"value"		=> get_current_user_id(), 
						"type"		=> "NUMERIC",  
						"compare"	=> "=" 
					]
				]
			]);
			$args['meta_query'][] = [
				"key" 			=> BIO_FACULTET_TYPE, 
				"value" 		=> $facultet_ids, 
				"type"			=> "NUMERIC",  
				"operator" 		=> "IN" 
			];
			//wp_die($args );
		}
		return $args;
	}
	static function bio_gq_term_meta($single_matrix, $instance, $classs, $key)
	{
		//if($classs == "" 
	}
	static function delete_term( $course_id, $tax_id, $deleted_term, $object_ids )
	{
		global $wpdb;
		$query = "DELETE FROM `".$wpdb->prefix."course_user_requests` WHERE course_id='$course_id'";
		$wpdb->query($query);
		$query = "DELETE FROM `".$wpdb->prefix."course_user` WHERE course_id='$course_id'";
		$wpdb->query($query);
	}
	static function create_taxonomy()
	{
		register_taxonomy(
			static::get_type(), 
			array( "post", BIO_ARTICLE_TYPE, BIO_EVENT_TYPE, BIO_TEST_TYPE, BIO_TESTINOMIAL_TYPE ), 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Course", BIO),
					'singular_name'     => __("Course", BIO),
					'search_items'      => __('search Course', BIO),
					'all_items'         => __('all Courses', BIO),
					'view_item '        => __('view Course', BIO),
					'parent_item'       => __('parent Course', BIO),
					'parent_item_colon' => __('parent Course:', BIO),
					'edit_item'         => __('edit Course', BIO),
					'update_item'       => __('update Course', BIO),
					'add_new_item'      => __('add new Course', BIO),
					'new_item_name'     => __('new Course Name', BIO),
					'menu_name'         => __('Course', BIO),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_in_nav_menus' 	=> true,
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => true,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
		//wp_nav_menu_taxonomy_meta_boxes() ;
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == static::get_type() )
			$parent_file = 'pe_edu_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'pe_edu_page', 
			__("Courses", BIO), 
			__("Courses", BIO), 
			'manage_options', 
			'edit-tags.php?taxonomy=' . static::get_type()
		);
		add_meta_box( "add-{BIO_COURSE_TYPE}", __("Courses", BIO), 'wp_nav_menu_item_taxonomy_meta_box', 'nav-menus', 'side', 'default', static::get_type() );	
    }
	static function ctg_columns($theme_columns) 
	{
		$new_columns = array
		(
			'cb' 			=> ' ',
			//'id' 			=> 'id',
			'name' 			=> __('Name'),
			'thumbnail' 	=> __('Icon'),
			'logotype'		=> __("logotype", BIO),
			//'color'		=> __("Color", BIO),
			//'adress'		=> __("Adress", BIO),
			//'author'		=> __("Author", BIO),
			'locked'		=> __("Locked", BIO),
			'order'			=> __("Order", BIO),
		);
		return $new_columns;
	}
	static function manage_ctg_columns($out, $column_name, $term_id) 
	{
		switch ($column_name) {
			case 'id':
				$out 		.= $term_id;
				break;
			case 'logotype': 
				$logotype = get_term_meta( $term_id, 'logotype', true ); 
				$out 		.= "<img src='" . wp_get_attachment_url($logotype) . "' style='width:auto; height:60px;' />";
				break;
			case 'order': 
				$order = get_term_meta( $term_id, 'order', true ); 
				$out 		.= $order;
				break;	
			case 'adress': 
				$adress = get_term_meta( $term_id, 'adress', true ); 
				$out 		.= $adress;
				break;	
			case 'author': 
				$author = get_term_meta( $term_id, 'author', true ); 
				$out 		.= get_user_by("id", $author)->display_name;
				break;	
			case 'color': 
				$color = get_term_meta( $term_id, 'color', true ); 
				$out 		.= "<div class='clr' style='background-color:$color;'></div>";
				break;	 
			case 'locked': 
				$locked = get_term_meta( $term_id, 'locked', true ); 
				$out 		.= $locked ? "<img src='" . BIO_URLPATH . "assets/img/check_checked.png'> <span class='smc-label-782px'>" . $obj[$column_name]['name'] . "</span>" 
				: 
				"<img src='" . BIO_URLPATH . "assets/img/check_unchecked.png'> <span class='smc-label-782px'>" . $obj[$column_name]['name'] . "</span>";
				break;	  
			case "thumbnail":
				$icon = get_term_meta( $term_id, 'thumbnail', true ); 
				$logo = wp_get_attachment_image_src($icon, "full")[0];
				echo "<img src='$logo' style='width:auto; height:60px; margin:10px;' />";
				break;	
			default:
				break;
		}
		return $out;    
	}
	
	static function add_ctg( $term, $tax_name )
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		if($term)
		{
			$term_id 		= $term->term_id;
			$is_closed 		= get_term_meta($term_id, "is_closed", true);
			$color 			= get_term_meta($term_id, "color", true);
			$bio_facultet 	= get_term_meta($term_id, "bio_facultet", true);
			$adress			= get_term_meta($term_id, "adress", true);
			$order 			= get_term_meta($term_id, "order", true);
			$thumbnail  	= get_term_meta($term_id, "thumbnail", true);
			$thumbnail  	= is_wp_error($thumbnail) ? "" :  $thumbnail;
			$include  		= get_term_meta($term_id, "include", true);
			$post_author  	= get_term_meta($term_id, "post_author", true);
			$locked  		= get_term_meta($term_id, "locked", true);
			$logotype		= get_term_meta($term->term_id, "logotype", true);
		}
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="include">
					<?php echo __("Include File", BIO);  ?>
				</label> 
			</th>
			<td>
				<input name="include" value="<?php echo $include; ?>" type="text" />
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="thumbnail">
					<?php echo __("Icon", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php
					echo get_input_file_form2( "group_icon", $thumbnail, "group_icon", 0 );
				?>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="logotype">
					<?php echo __("Logotype", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php
					echo get_input_file_form2( "group_icon", $logotype, "group_icon", "logotype" );
				?>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="bio_facultet">
					<?php echo __("Facultet", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php 
					echo Bio_Facultet::wp_dropdown([
						"selected" 	=> $bio_facultet,
						"class"		=> "form-control",
						"name"		=> 'bio_facultet'
					]);
				?>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="adress">
					<?php echo __("Adress", BIO);  ?>
				</label> 
			</th>
			<td>
				<input name="adress" value="<?php echo $adress; ?>" type="text" />
			</td>
		</tr>

		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="order">
					<?php echo __("Order", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="number" name="order" value="<?php echo $order; ?>" />
			</td>
		</tr>

		<tr class="form-field">
			<th scope="row" valign="top">
				<label >
					<?php echo __("Course is locked for free subscribe", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="checkbox" class="checkbox" id="locked"  name="locked" value="1" "<?php checked(1,  $locked, 1); ?>" />				
				<label for="locked"> </label> 
			</td>
		</tr>

		<tr class="form-field">
			<th scope="row" valign="top">
				<label >
					<?php echo __("Users mayn't request membership", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="checkbox" class="checkbox" id="is_closed"  name="is_closed" value="1" "<?php checked(1,  $is_closed, 1); ?>" />				
				<label for="is_closed"> </label> 
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="post_author">
					<?php echo __("Rector", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php
					wp_dropdown_users( array(
						'show_option_none'        => '--',
						'orderby'                 => 'display_name',
						'order'                   => 'ASC',
						'multi'                   => false,
						'show'                    => 'display_name',
						'echo'                    => true,
						'selected'                => $post_author,
						'include_selected'        => false,
						'name'                    => 'post_author',
						'id'                      => 'post_author',
						'class'                   => 'w-100',
						'blog_id'                 => $GLOBALS['blog_id'],
						'who'                     => '',
						'role'                    => 'CourseLeader',
						'role__in'                => array(),
						'role__not_in'            => array(),
					) );
				?>
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		update_term_meta($term_id, "color", 		$_POST['color']);
		update_term_meta($term_id, "is_closed", 	$_POST['is_closed']? 1 : 0);
		update_term_meta($term_id, "adress",		$_POST['adress']);
		update_term_meta($term_id, "order",			$_POST['order']);
		update_term_meta($term_id, "bio_facultet",	$_POST['bio_facultet']);
		update_term_meta($term_id, "post_author",	$_POST['post_author']);
		update_term_meta($term_id, "locked",		$_POST['locked'] ? 1 : 0);
		if($_POST['group_icon0'])
		{
			update_term_meta($term_id, "thumbnail",	$_POST['group_icon0']);
		}
		if($_POST['group_iconlogotype'])
		{
			update_term_meta($term_id, "logotype",  $_POST['group_iconlogotype']);
		}
		update_term_meta($term_id, "include",  		$_POST['include']);
	}
	
	
	
	static function is_locked($term_id)
	{
		if( get_term_meta($term_id, "locked", true) )
		{
			return is_array( $term_id ) ? 
				!Bio_User::is_subscribe_courses( $term_id ) : 
				!Bio_User::is_subscribe_course( $term_id );
		}
		else
			return false;
		
	}
	static function is_user_author($term_id, $admn_may=false)
	{
		if($admn_may  && Bio_User::is_user_roles(['administrator', 'editor']))
		{
			return true;
		}
		return get_current_user_id() == get_term_meta($term_id, "author", true);
	}
	static function is_request_permission($term_id, $user_id=null)
	{
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		global $wpdb;
		$query	= "SELECT COUNT(*) FROM `".$wpdb->prefix."course_user_requests` WHERE course_id='$term_id' AND user_id='$user->ID';";
		return $wpdb->get_var($query);
	}
	static function refuse_permission($course_id, $user_id, $comment)
	{
		global $wpdb;
		if(static::is_user_author($course_id, true))
		{
			$query="DELETE FROM `".$wpdb->prefix."course_user_requests` WHERE course_id='$course_id' AND user_id='$user_id'";
			$wpdb->query($query);
			
			$course	= get_term_by("id", $course_id, static::get_type());
			$user	= get_user_by("id", $user_id);
			Bio_Mailing::send_mail(
				sprintf( __("You were denied participation in the course %s", BIO),  $course->name), 
				sprintf( __("You have received this letter because you have been required to %s course on the pe-edu portal. You are denied. Rejection reason: %s", BIO), $course->name, $comment) ,
				wp_get_current_user(), 
				[ $user->user_email ]
			);
		}
		else
			return false;
	}
	static function addCourseUser($course_id, $user_id)
	{
		global $wpdb;
		$query="DELETE FROM `".$wpdb->prefix."course_user_requests` WHERE course_id='$course_id' AND user_id='$user_id'";
		$wpdb->query($query);
		$query = "INSERT INTO `".$wpdb->prefix."course_user` (`ID`, `course_id`, `user_id`, `date`) VALUES (NULL, '$course_id', '$user_id', CURRENT_TIMESTAMP);";
		
		
		$course	= get_term_by("id", $course_id, static::get_type());
		$user	= get_user_by("id", $user_id);
		Bio_Mailing::send_mail(
			sprintf( __("You are added to Course %s", BIO),  $course->name), 
			sprintf( __("You have received this letter because you have been required to %s course on the pe-edu portal. You have received an application for the Course.", BIO), $course->name) ,
			wp_get_current_user(), 
			[ $user->user_email ]
		);
		return $wpdb->query($query);
	}
	static function accept_permission($course_id, $user_id)
	{
		if( static::is_user_author($course_id, true) )
		{
			return static::addCourseUser($course_id, $user_id);
		}
		else
			return false;
	}
	static function request_permission($term_id)
	{
		if(!is_user_logged_in()) return;
		$user_id = get_current_user_id();
		$course		= get_term_by( "term_id", $term_id, BIO_COURSE_TYPE );
		//send mail
		//$adresse	= wp_get_current_user()->user_email;
		$author 	= get_user_by( "id", get_term_meta($term_id, "author", true))->user_email;
		Bio_Mailing::send_mail(
				sprintf( __("Send request for %s", BIO),  $course->name), 
				sprintf( __("You have received this letter because you have been registered as the moderator of the %s course on the pe-edu portal. You have received an application for the Course.", BIO), $course->name) . 
			"<p>". 
				sprintf( __("Requester - %s", BIO), wp_get_current_user()->display_name) . 
			"<p>". 
				sprintf( 
					__("Go to %s for answering to request", BIO), 
					"<a href='#'>".
						__("Cabinet page", BIO).
					"</a>" 
				) ,
			wp_get_current_user(), 
			[$author]
		);
		
		global $wpdb;
		$query	= "DELETE FROM `".$wpdb->prefix."course_user_requests` WHERE course_id='$term_id' AND user_id='$user_id'";
		$wpdb->query($query);
		$query	= "INSERT INTO  `".$wpdb->prefix."course_user_requests` (`ID`, `course_id`, `user_id`, `date`, `accessed`) VALUES (NULL, '$term_id', '$user_id', CURRENT_TIMESTAMP, '0');";
		return $wpdb->query($query);
	}
	static function withdraw_permission($term_id)
	{
		if(!is_user_logged_in()) return;
		$user_id = get_current_user_id();
		global $wpdb;
		$query	= "DELETE FROM `".$wpdb->prefix."course_user` WHERE course_id='$term_id' AND user_id='$user_id'";
		$boo = $wpdb->query($query);
		$query	= "DELETE FROM `".$wpdb->prefix."course_user_requests` WHERE course_id='$term_id' AND user_id='$user_id'";
		return $wpdb->query($query) || $boo;
	}
	static function request_permission_leave($term_id)
	{
		if(!is_user_logged_in()) return;
		$user_id = get_current_user_id();
		global $wpdb;
		$query	= "DELETE FROM `".$wpdb->prefix."course_user_requests` WHERE course_id='$term_id' AND user_id='$user_id'";
		$wpdb->query($query);
		$query	= "INSERT INTO `".$wpdb->prefix."course_user_requests` (`ID`, `course_id`, `user_id`, `date`, `join`) VALUES (NULL, '$term_id', '$user_id', CURRENT_TIMESTAMP, '1');";
		return $wpdb->query($query);
	}
	static function get_all_per_author($user_id = -1)
	{
		$user_id = $user_id < 1 ? get_current_user_id() : $user_id;
		$terms = get_terms( array(
			'taxonomy'      => BIO_COURSE_TYPE, 
			'orderby'       => 'name', 
			'order'         => 'ASC',
			'hide_empty'    => false, 
			'fields'        => 'all', 
			'meta_query'    => [
				[
					"key"	=> "author",
					"value"	=> $user_id
				]
			]
		) );
		return $terms;
	}
	static function get_admin_form($term_id)
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		$thumbnail = get_term_meta($term_id, "thumbnail", true);
		$lock = get_term_meta($term_id, "locked", true);
		$adress = get_term_meta($term_id, "adress", true);
		$term = get_term($term_id,  static::get_type() );
		$name = $term->name;
		$descr= $term->description;
		$pupils	= static::get_admin_pupils_form( $term_id );
		$html = "
		
		<div class='row justify-content-md-center' post_id='$term_id' command='bio_edit_course'>
			<ul class='nav nav-pills mb-3 justify-content-center text-center' id='pills-tab' role='tablist'>
				<li class='nav-item'>
					<a class='nav-link active' id='pills-home-tab' data-toggle='pill' href='#pills-home' role='tab' aria-controls='pills-home' aria-selected='true'>".
					  __("Settings" ).
					"</a>
				</li>
				<li class='nav-item'>
					<a class='nav-link' id='pills-profile-tab' data-toggle='pill' href='#pills-profile' role='tab' aria-controls='pills-profile' aria-selected='false'>".
					  __("Pupils", BIO).
					"</a>
				</li>
			</ul>
			<div class='tab-content col-12' id='pills-tabContent'>
				<div class='tab-pane fade show active' id='pills-home' role='tabpanel' aria-labelledby='pills-home-tab'>
					<div class='card'>
						<div class='card-body'>
							<div class='row'>
								<form class='cab_edit' >
									<small class='col-12'>". __("Icon", BIO). "</small>
									<div class='col-12' style='display: flex;'>".
										get_input_file_form2( "thumbnail", $thumbnail, "thumbnail", "" ).
									"</div>	
									<div class='spacer-10'></div>
									
									<small class='col-12'>". __("Title", BIO). "</small>
									<div class='col-12'>
										<input type='text' class='form-control' name='name' value='$name' />
									</div>		
									<div class='spacer-10'></div>
									
									<small class='col-12'>". __("Description", BIO). "</small>
									<div class='col-12'>
										<textarea class='form-control' name='description' rows='5' >$descr</textarea>
									</div>	
									<div class='spacer-10'></div>
									
									<small class='col-12'>". __("Adress", BIO). "</small>
									<div class='col-12'>
										<input type='text' class='form-control' name='adress' value='$adress' />
									</div>		
									<div class='spacer-10'></div>
										
									
									<small class='col-12'>". __("Lock", BIO). "</small>
									<div class='col-12'>
										<input type='checkbox' class='checkbox' name='lock' id='lock' value='1' ".checked(1, $lock,0)." /> <label for='lock'> </label>
										<div class='bio_descr'>".
											__("Only your pupils must see Matherials of this Course", BIO) .
										"</div>
									</div>		
									<div class='spacer-10'></div>
									
									<div class='col-12'>
										<input type='submit' class='btn btn-primary value='". __("Update", BIO) . "' />		
									</div>	
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class='tab-pane fade' id='pills-profile' role='tabpanel' aria-labelledby='pills-profile-tab'>
					<div class='card'>
						<div class='card-body' id='course_pupils'>							
							$pupils	
						</div>
					</div>			
				</div>
			</div>
		</div>
		";
		return $html;
	}
	
	static function wp_dropdown($params=-1)
	{
		if(!is_array($params))
			$params = [];
		if($params['terms'])
		{
			$terms		=  $params['terms'];
		}
		else
		{
			$terms = get_terms( array(
				'taxonomy'      => static::get_type(), 
				'orderby'       => 'name', 
				'order'         => 'ASC',
				'hide_empty'    => false, 
				'fields'        => 'all', 
			) );
		}
		$html		= "<select ";
		if($params['class'])
			$html	.= "class='".$params['class']."' ";
		if($params['style'])
			$html	.= "style='".$params['style']."' ";
		if($params['name'])
			$html	.= "name='".$params['name']."' ";
		if($params['id'])
			$html	.= "id='".$params['id']."' ";
		if($params['special'])
		{
			$pars  	= explode(",", $params['special']);
			$html	.= "$pars[0]='$pars[1]' ";
		}
		$html		.= " >";
		$zero 		= $params['select_none'] ? $params['select_none'] : "---";
			if(!$params['none_zero'])
				$html	.= "<option value='-1' selected>$zero</option>";			
			
		if(count($terms))
		{
			foreach($terms as $term)
			{
				$html	.= "
				<option " . selected($term->term_id, $params['selected'], 0) . " value='".$term->term_id."'>".
					$term->name.
				"</option>";
			}
		}
		$html		.= apply_filters("bio_course_last_dropdown", "", $params, $terms) . "
		</select>";
		return $html;
	}
	static function get_admin_pupils_form( $term_id )
	{
		$requests 	= static::get_pupils_requests( $term_id );
		$pupils 	= static::get_pupils( $term_id );
		
		$html  = "
		<subtitle class='col-12'>" . __("Users who request", BIO) . "</subtitle><div>
		<table class='table' >";
		if(count($requests))
		{
			$html .= "
				<div class='alert noalert alert-secondary' role='users_requires'>
					".
					static::get_bulk_select() .
					" <descr></descr>
					<div class='btn btn-outline-secondary btn-sm bio-sl hidden'>" . __("Bulk action", BIO) . "</div>
				</div>
				<table class='table table-striped' users_requires> 
				
					<tr class='bg-secondary text-white'>
						<td scope='col'><input type='checkbox' name='users'/><label></label></td>
						<td scope='col'>" .  __("User") . "</td>
						<td scope='col'>" .  __("e-mail") . "</td>
						<td scope='col'>
							<div class='btn btn-outline-link btn-sm bio_user_filters'>" .  
								__("Filters", BIO) . 
							"</div>
							<div class='btn btn-outline-link btn-sm bio_user_order'>" .
								__("Order", BIO) . 
							"</div>
						</td>
					</tr>";
			foreach($requests as $p)
			{
				$user = get_user_by("id", $p->ID);
				$html .= "
				<tr user_id='$p->ID'>
					<td><input type='checkbox' name='users[]' uid='$p->ID'/><label></label></td>
					<td>" . $p->display_name . "</td>
					<td>" . $p->user_email . "</td>
					<td>
						<div class='btn btn-outline-success btn-sm bio_course_user_access'>" . 
							__("agree", BIO) . 
						"</div>
						<div class='btn btn-outline-danger  btn-sm bio_course_user_refuse'>" . 
							__("refuse request", BIO) . 
						"</div>
					</td>
				</tr>";
			}
		}
		else
		{
			$html .= "
			<div class='alert alert-danger' role='alert'>
				No subscribers
			</div>
			";
		}
		$html .= "</table>";
		$html  .= "
		<subtitle class='col-12'>" . __("Members", BIO) . "</subtitle>
		<table class='table' >";
		if(count($pupils))
		{
			$html .= "
				<div class='alert noalert alert-info' role='users_requires'>
					".
					static::get_bulk_select() .
					" <descr></descr>
					<div class='btn btn-outline-info btn-sm bio-sl hidden'>" . __("Bulk action", BIO) . "</div>
				</div>
				<table class='table table-striped' users_requires> 
				
					<tr class='bg-info text-white'>
						<td scope='col'><input type='checkbox' name='users'/><label></label></td>
						<td scope='col'>" .  __("User") . "</td>
						<td scope='col'>" .  __("e-mail") . "</td>
						<td scope='col'>
							<div class='btn btn-outline-link btn-sm bio_user_filters'>" .  
								__("Filters", BIO) . 
							"</div>
							<div class='btn btn-outline-link btn-sm bio_user_order'>" .
								__("Order", BIO) . 
							"</div>
						</td>
					</tr>";
			foreach($pupils as $p)
			{
				$html .= "
				<tr user_id='$p->ID'>
					<td><input type='checkbox' name='users[]' uid='$p->ID'/><label></label></td>
					<td>" . $p->display_name . "</td>
					<td>" . $p->user_email . "</td>
					<td>
						<div class='btn btn-success btn-sm bio_conglom_user_deff'>" . 
							__("Manipulations", BIO) . 
						"</div>
					</td>
				</tr>";
			}
		}
		else
		{
			$html .= "
			<div class='alert alert-danger' role='alert'>
				No subscribers
			</div>
			";
		}
		$html .= "</table>";
		return $html;
	}
	
	static function get_bulk_select( $params=-1)
	{
		if(!is_array($params))
		{
			$params = [ "name" => "bio_bulk" ];
		}
		$t = apply_filters("bio_bukl_select", [
			"-1"			=> __("-", BIO),
			"refuse"		=> __("Refuse", BIO),
			"add_mail"		=> __("Add to Mailing", BIO),
			"add_course"	=> __("Add to Course", BIO),
			"add_event"		=> __("Add to Event", BIO),
		]);
		$html = "<selectclass='padding_4px bio_user_bulk'>";
		foreach($t as $at=>$val)
		{
			$html .= "<option >$val</option>";
		}
		$html .= "</select>";
		$html = "
		<div class='btn-group'>
			<button class='btn btn-outline-info btn-sm dropdown-toggle w-200px' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'  name='" . $params['name'] . "' >".
				"--".
			"</button>
		<div class='dropdown-menu'>
		
		<!--select name='" . $params['name'] . "' class='padding_4px bio_user_bulk'-->";
		foreach($t as $at=>$val)
		{
			$html .= " <a class='dropdown-item small' href='#' value='$at'>$val</a>";
		}
		$html .= "
			</div>
		</div>
		<!--/select-->";
		return $html;
	}
	
	static function get_pupils_requests( $term_id )
	{
		global $wpdb;
		$query = "
		SELECT u.display_name, u.ID, u.user_email FROM  ". $wpdb->prefix ."course_user_requests as cur
		LEFT JOIN ". $wpdb->prefix ."users as u ON u.ID=cur.user_id
		WHERE course_id=$term_id";
		
		return $wpdb->get_results($query);
	}
	static function get_pupils( $term_id )
	{
		global $wpdb;
		$query = "
		SELECT u.display_name, u.ID, u.user_email FROM  ". $wpdb->prefix ."course_user as cur
		LEFT JOIN ". $wpdb->prefix ."users as u ON u.ID=cur.user_id
		WHERE course_id=$term_id";
		return $wpdb->get_results($query);
	}
	static function get_permission($course_id)
	{
		switch(true)
		{
			case Bio_User::is_member_course( $course_id ):
				$permission = 1; // user is course member
				break;
			case Bio_Course::is_request_permission( $course_id ):
				$permission = 2; // user call permission already
				break;
			case Bio_Course::is_locked( $course_id ):
				$permission = 3; // course locked for user
				break;
			default:
				$permission = 4; // course open and user see
				break;
		}
		return $permission;
	}
	static function is_course_lock($term_id)
	{
		return get_term_meta($term_id, "locked", true);
	}
	static function get_article_count($term_id)
	{
		global $wpdb;
		$query = "SELECT COUNT(*) FROM " . $wpdb->prefix . "posts AS p
		LEFT JOIN " . $wpdb->prefix . "term_relationships AS tr ON tr.object_id=p.ID 
		WHERE tr.term_taxonomy_id=$term_id AND p.post_type='".BIO_ARTICLE_TYPE."'  AND p.post_status='publish';";
		return $wpdb->get_var($query);
	}
	static function get_test_count($term_id)
	{
		global $wpdb, $rest_log;
		$query = "SELECT COUNT(*) FROM " . $wpdb->prefix . "posts AS p
		LEFT JOIN " . $wpdb->prefix . "term_relationships AS tr ON tr.object_id=p.ID 
		WHERE tr.term_taxonomy_id=$term_id AND p.post_type='".BIO_TEST_TYPE."' AND p.post_status='publish';";
		$rest_log = $query;
		return $wpdb->get_var($query);
	}

	static function get_requests_count()
	{
		global $wpdb;
		$in = Bio_User::is_user_roles(['administrator', 'editor']) 
			? "1=1" 
			: "course_id IN ( SELECT term_id FROM ".$wpdb->prefix."termmeta WHERE meta_key='author' AND meta_value=" . get_current_user_id() . " )";
		$query = "SELECT COUNT(*) FROM ".$wpdb->prefix."course_user_requests WHERE $in ;";
		return (int)$wpdb->get_var($query);
	}


    static function get_course($p, $is_admin=false, $is_full=false)
    {
        if(is_numeric($p))
        {
            $course = get_term($p, BIO_COURSE_TYPE);
        }
        else
        {
            $course = $p;
        }
        $c = [];
        if(is_wp_error($course) || !$course)
            return $c;
        $c['id']			= $course->term_id;
        $c['post_title']	= $course->name;
        $c['parent']		= $course->parent;
        $c['count']			= $course->count;
        $c['post_content']	= $course->description;
        $c['thumbnail_id']	= (int)get_term_meta( $course->term_id, "thumbnail", true);
        $c['thumbnail']		= wp_get_attachment_image_src( $c['thumbnail_id'], "full" )[0];
        
        $c['locked']		= (int)get_term_meta( $course->term_id, "locked", true);
        $c['include']		=  get_term_meta( $course->term_id, "include", true);
        $author				= get_user_by("id", get_term_meta( $course->term_id, "author", true));
        $c['author']		= [
            "id"			=> $author->ID,
            "display_name"	=> $author->display_name,
        ];
		//parent
		{
			if($course->parent > 0)
			{
				$parent = get_term($course->parent, BIO_COURSE_TYPE);
				$c['parent_title'] = $parent->name;
			}
		}
		//children
		$children = [];
		$ch = get_terms([
			"taxonomy" 		=> BIO_COURSE_TYPE,
			'hide_empty' 	=> false,
			'orderby'       => 'name', 
			'order'         => 'ASC',
			"parent"		=> $course->term_id
		]);
		foreach($ch as $chh)
		{
			$children[] = [
				"id"		=> $chh->term_id,
				"post_title"=> $chh->name,
				'count' 	=> $chh->count
			];
		}
		$c['children']		= $children;
		
		if($is_full)
		{
			$c['articles_count']	= static::get_article_count( $course->term_id );
			$c['test_count']		= static::get_test_count( $course->term_id );
		}
		if($is_admin)
		{
			$c['requests']	= static::get_pupils_requests( $course->term_id );
			$c['members']	= static::get_pupils( $course->term_id );
			$event			= Bio_Event::get_instance(-1);
			$over 			= $event->get_my_over();
			$byUser			= Bio_Event::get_by_user( );
			$c['over']		= [];
			foreach($over as $e)
			{
				if( !in_array($e->ID, $byUser))
					$c['over'][] = Bio_Event::get_event($e);
			}
		}
        return $c;
    }
	/**/
	static function update( $data, $post_id )
	{
		global $borr;
		$post_id = (int)$post_id;
		$term = wp_update_term( 
			$post_id, 
			static::get_type(), 
			[
				'name' 			=> $data["post_title"],
				'description' 	=> $data["post_content"],
			]
		);
		
		if( substr($data['logotype'],0, 4) != "http"  && $data['logotype'] )
        {
            $media = Bio_Assistants::insert_media(
				[
					"data" => $data['logotype'],
					"media_name"=> $data['logotype_name']
                ], 
				$post_id
			);
			wp_set_object_terms( $media['id'], (int)Bio::$options['icon_media_term'], BIO_MEDIA_TAXONOMY_TYPE );
			update_term_meta( $post_id, "logotype", $media  ? $media['id'] : "" );
        }
		
		if($data['logotype'] == "" )
		{
			update_term_meta( $post_id, "logotype", false  );
		}
		
		if( substr($data['thumbnail'],0, 4) != "http"  && $data['thumbnail'] )
        {
            $media = Bio_Assistants::insert_media(
				[
					"data" => $data['thumbnail'],
					"media_name"=> $data['thumbnail_name']
                ], 
				$post_id
			);
			wp_set_object_terms( $media['id'], (int)Bio::$options['icon_media_term'], BIO_MEDIA_TAXONOMY_TYPE );
			update_term_meta( $post_id, "thumbnail", $media  ? $media['id'] : "" );
        }
		
		if($data['thumbnail'] == "" )
		{
			update_term_meta( $post_id, "thumbnail", false  );
		}
		if(isset($data[ BIO_FACULTET_TYPE ]))
		{
			update_term_meta($post_id, BIO_FACULTET_TYPE, $data[BIO_FACULTET_TYPE]);
		}
		update_term_meta($post_id, "author", $data['author'] ? $data['author'] : get_current_user_id());
		update_term_meta($post_id, "locked", $data["locked"]);
		update_term_meta($post_id, "order", $data["order"]);
		update_term_meta($post_id, "include", $data["include"]);
		
		$post_id = apply_filters("smc_edited_term", $post_id, $data, static::get_type());
		$borr = $data;
		return $post_id;
	}
	
	
	
	static function insert( $data )
	{
		//
		$post_id = wp_insert_term( 
			$data["post_title"], 
			static::get_type(), 
			array(
				'description' => $data["post_content"],
				'slug' => $data["post_name"],
				'parent' => $data["parent"]
			) 
		);
		if(!is_wp_error($post_id))
		{
			if(  $data['thumbnail'] )
			{
				$media = Bio_Assistants::insert_media(
					[
						"data" => $data['thumbnail'],
						"media_name"=> $data['thumbnail_name']
					], 
					$post_id['term_id']
				);
				wp_set_object_terms( $media['id'], (int)Bio::$options['icon_media_term'], BIO_MEDIA_TAXONOMY_TYPE );
				update_term_meta( $post_id['term_id'], "thumbnail", $media  ? $media['id'] : "" );
			}
			update_term_meta($post_id['term_id'], "order",  $data["order"]);
			update_term_meta($post_id['term_id'], "locked", $data["locked"]);
			update_term_meta($post_id['term_id'], "author", $data["author"]);			
			update_term_meta($post_id['term_id'], "price",  $data["price"]);			
			if(isset($data[ BIO_FACULTET_TYPE ]))
			{
				update_term_meta($post_id['term_id'], BIO_FACULTET_TYPE, $data[BIO_FACULTET_TYPE]);
			}
		}
		return $post_id;
	}
    public static function api_action($type, $methods, $code, $pars, $user)
    {
        /* type - "bio_course" */
        $courses	= [];
        switch($methods) {
            case "update":

                if(is_numeric($code)) 
				{
					
                }
				else
				{
                    
                }
                break;
            case "delete":
				if(is_numeric($code)) 
				{
					Bio_User::access_caps(BIO_COURSE_DELETE, "Delete Course");
					$id = wp_delete_term((int)$code, static::get_type());
					if(is_wp_error($id))
					{
						$text = $id->get_error_message();
					}
					else if(!$id)
					{
						$text = __("This course not may delete", BIO);
					}
					else
					{
						$text = __("Course deleted", BIO);
					}
				}
                break;
            case "create":
                if(is_numeric($code)) 
				{
					Bio_User::access_caps(BIO_COURSE_EDIT, "Update Course");
					$data			= $pars;
                    $post_id		= (int)$code;
                    $pars['locked']	= (int)$pars['locked'];
                    if($post_id > 1)
                    {
                        $id = static::update( $pars, $post_id );
                        $text = __("Update Course successfull", BIO);
                    } 
                }
				else
				{
					Bio_User::access_caps(BIO_COURSE_CREATE, "Insert Course");
					$pars['post_type'] 		= Bio_Course::get_type();
					$pars['name'] 			= $pars['post_title'];
					$pars['description'] 	= $pars['post_content'];
                    $pars['locked']			= (int)$pars['locked'];
					$id 					= Bio_Course::insert($pars);
					$text 					= __("Insert new Course", BIO);
				}
				$courses[]	= Bio_Course::get_course( $id, 1, $pars['is_full'] );
                break;
            case "read":
            default:
				$articles = [];
                if(is_numeric($code)) 
				{
                    $c					= Bio_Course::get_course( $code, $pars['is_admin'], $pars['is_full'] );
                    $c['permission']	= Bio_Course::get_permission($code);
                    $courses[] 			= $c;                    
                    break;
                }
				else
				{
					$meta_query = "";
					if( $pars['author'] > 0 )
						$author = $pars['author'];
					if( $pars['author'] == -2) 
					{
						$author = get_current_user_id();
					}
					if($author && !Bio_User::is_user_roles(['administrator', 'editor']) )
					{
						$meta_query = [
							'relation' => 'OR',
							[
								"key"	=> "author",
								"value"	=> $author,
								'type'	=> 'NUMERIC'
							]
						];
					}
                    $terms = get_terms( array(
                        'taxonomy'      => BIO_COURSE_TYPE,
                        'orderby'       => 'id',
                        'order'         => 'DESC',
                        'hide_empty'    => false,
                        'object_ids'    => null,
                        'include'       => array(),
                        'exclude'       => array(),
                        'exclude_tree'  => array(),
                        'number'        => '',
                        'fields'        => 'all',
                        'count'         => false,
                        'slug'          => '',
                        'parent'         => $pars['parent'],
                        'hierarchical'  => true,
                        'child_of'      => 0,
                        'offset'        => '',
                        'name'          => '',
                        'childless'     => false,
                        'update_term_meta_cache' => true,
                        'meta_query'    => $meta_query,
                    ) );
                    foreach($terms as $c)
                    {
                        $courses[]	= static::get_course( $c, $pars['is_admin'], $pars['is_full'] );
                    }
                }
				$text = $pars;
        }

        return [
            "msg" => $text,
            "courses" => $courses,
            "articles" => $articles,
            "id" => $code
        ];
    }
	
    static function get_classes($p)
    {
        if(is_numeric($p))
        {
            $course = get_term($p, BIO_COURSE_TYPE);
        }
        else
        {
            $course = $p;
        }
        $c = [];
        if(is_wp_error($course) || !$course)
            return $c;

        $c['id']			= $course->term_id;
        $c['post_title']	= $course->name;
        $c['thumbnail']		= get_term_meta($course->term_id, "thumbnail", true);
        $c['thumbnail_id']	= $c['thumbnail'];
        $c['thumbnail']		= wp_get_attachment_url($c['thumbnail']);
        $c['post_content']	= $course->description;

        return $c;
    }

    static function get_class($class)
    {
        if(is_numeric($class))
        {
            $class = get_term($class, BIO_COURSE_TYPE);

        }
        $c = [];
        $c['ID']			= $class->term_id;
        $c['id']			= $class->term_id;
        $c['post_title']	= $class->name;
        $c['thumbnail']		= get_term_meta($class->term_id, "thumbnail", true);
        $c['price']			= (int)get_term_meta($class->term_id, "price", true);
        $c['raiting']		= (int)get_term_meta($class->term_id, "raiting", true);
        $c['thumbnail_id']	= $c['thumbnail'];
        $c['thumbnail']		= wp_get_attachment_url($c['thumbnail']);
        return $c;

    }
	function get_single_matrix()
	{
		$matrix = parent::get_single_matrix();
		$matrix['is_member']		= (bool)Bio_User::is_member_course($this->id);
		$matrix['order']			= intval($this->get_meta("order"));
		$matrix['locked']			= (bool)$this->get_meta("locked");
		$matrix['is_closed']		= (bool)$this->get_meta("is_closed");
		$matrix['jitsi']			= $this->get_meta("jitsi");
		$matrix['jitsi_password']	= $this->get_meta("jitsi_password");
		$matrix['include']			= Bio_Facultet::is_user_subscribe(($this->get_meta(BIO_FACULTET_TYPE))) || current_user_can("manage_options") ? $this->get_meta("include") : null;
		return $matrix;
	}

}
<?php
use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class Bio_Facultet extends SMC_Taxonomy
{	
	static function get_type()
	{
		return BIO_FACULTET_TYPE;
	}
	static function init()
	{
		add_action( 'init', 				array( __CLASS__, 'create_taxonomy'), 12);
		add_action( 'parent_file',			array( __CLASS__, 'tax_menu_correction'), 1);	
		add_action( 'admin_menu', 			array( __CLASS__, 'tax_add_admin_menus'), 11);
		add_filter( "get_single_matrix", 	array( __CLASS__,'get_single_matrix_handler'), 10, 3); 
		add_filter( "manage_edit-".BIO_FACULTET_TYPE."_columns", 	array( __CLASS__,'ctg_columns')); 
		add_filter( "manage_".BIO_FACULTET_TYPE."_custom_column",	array( __CLASS__,'manage_ctg_columns'), 11.234, 3);
		add_action( BIO_FACULTET_TYPE.'_edit_form_fields', 			array( __CLASS__, 'add_ctg'), 2, 2 );
		add_action( 'edit_'.BIO_FACULTET_TYPE, 						array( __CLASS__, 'save_ctg'), 10);  
		add_action( 'create_'.BIO_FACULTET_TYPE, 					array( __CLASS__, 'save_ctg'), 10);			
		add_action( 'delete_'.BIO_FACULTET_TYPE,					[__CLASS__, "delete_term"], 10, 5);
		//add_action( 'parse_tax_query',							[__CLASS__, 'parse_tax_query'], 10, 2 );
		//add_filter( 'posts_where',								[__CLASS__, 'posts_where'], 10, 1 );
		//add_filter( 'posts_where_paged',							[__CLASS__, 'posts_where_paged'], 10, 2 );
		//add_filter( 'posts_join_request',							[__CLASS__, 'posts_join_request'], 10, 2 );
		
		
		
		add_filter( "smc_edited_term",					[ __CLASS__, "smc_edited_term"], 20, 3 );
		add_filter( "bio_get_user",						[ __CLASS__, "bio_get_user"], 20, 2 );
		add_filter( "pe_graphql_user_fields",			[ __CLASS__, "pe_graphql_user_fields"], 20, 2 );
		add_filter( "pe_graphql_get_users",				[ __CLASS__, "pe_graphql_get_users"], 20, 2 );
		add_filter( "pe_graphql_get_user",				[ __CLASS__, "pe_graphql_get_user"], 21);
		add_filter( "pe_graphql_change_meta_user",		[ __CLASS__, "pe_graphql_change_meta_user"], 20, 2);
		add_action( "pe_graphql_change_current_user", 	[ __CLASS__, "pe_graphql_change_current_user"]);
		
		//
		add_filter( "get_all_taxonomies_args", 			[ __CLASS__, "get_all_taxonomies_args"], 10, 2);
		add_filter( "get_all_taxonomies", 				[ __CLASS__, "get_all_taxonomies"], 10, 3);
		
		
	}
	static function get_all_taxonomies_args( $args, $pars )
	{
		if(current_user_can("manage_options"))	return $args;
		if( $pars['is_admin'] && $args['taxonomy']  == static::get_type() )
		{
			if( !isset($args['metas']) )
			{
				$args['metas'] = [];
			}
			$args['meta_query'][] = [
				"key" 			=> "post_author", 
				"value" 		=> get_current_user_id(), 
				"type"			=> "NUMERIC",  
				"compare" 		=> "=" 
			];
			//wp_die($args );
		}
		return $args;
	}
	static function get_all_taxonomies( $all, $args, $pars )
	{
		
		return $all;
	}
	
	static function smc_edited_term( $post_id, $data, $post_type )
	{ 
		update_term_meta( $post_id, "title_descr", $data['title_descr'] );
		update_term_meta( $post_id, "description", $data['description'] );
		update_term_meta( $post_id, "order", $data['order'] );
		 
		if( substr( $data['icon'],0, 4) != "http"  &&  $data['icon'] )
		{ 
			$media = Bio_Assistants::insert_media(
				[
					"data" => $data['icon'],
					"media_name"=>  $data[ 'icon_name']
				], 
				$post_id
			);
			wp_set_object_terms( $media['id'], (int)Bio::$options['icon_media_term'], BIO_MEDIA_TAXONOMY_TYPE );
			update_term_meta( $post_id, 'icon', $media  ? $media['id'] : "" );
		}
		return $post_id;
	}
	static function bio_get_user($args, $user)
	{
		$args['bio_facultet']		= static::get_user_facultet( null, true );
		return $args;
	}
	
	static function pe_graphql_user_fields( $fields, $isForInput )
	{
		$fields['bio_facultet'] = Type::listOf( $isForInput ? Type::int() : PEGraphql::object_type( "Bio_Facultet" ) );
		return $fields;
	}
	
	static function pe_graphql_get_user($user)
	{
		$bio_facultets	= static::get_user_facultet( $user->id, false );
		
		$bf				= [];
		foreach( $bio_facultets as $bio_facultet )
		{
			$fac		= Bio_Facultet::get_instance( $bio_facultet->facultet_id );
			if( $facultet_id = apply_filters("bio_facultet_user",  $fac->get_single_matrix( ), $bio_facultet, $user))
			{
				$bf[] 	= $facultet_id;
			}
		}
		$user->bio_facultet = $bf;
		return $user;
	}
	static function pe_graphql_get_users($users)
	{
		foreach($users as $user)
		{
			$user = static::pe_graphql_get_user($user);
		}
		return $users;
	}
	static function pe_graphql_change_meta_user($user_id, $fields)
	{
		if( isset($fields['input']['bio_facultet']) )
		{
			//update_user_meta($user_id, "bio_facultet", $fields['input']['bio_facultet'] );
		}
		return $user_id;
	}
	static function pe_graphql_change_current_user($fields)
	{
		if(isset($fields['input']['bio_facultet']))
		{
			//update_user_meta(get_current_user_id(), "bio_facultet", $fields['input']['bio_facultet'] );
		}
	}
	
	
	
	
	
	static function parse_tax_query( $query )
	{
		return;
		if ( $query->query['post_type'] == BIO_ARTICLE_TYPE)
		{
			$user_id	= get_current_user_id();
			$courses	= static::get_facultet_courses( static::get_user_facultet( null, true ) , true);
			$push = [
				"taxonomy" 	=> "bio_course",
				"field"		=> "id",
				"terms"		=> [$courses],
				"include_children" => true,
				"operator"	=> "IN"
			];
			if($query->query['tax_query'])
				array_push($query->query['tax_query'] , $push);
			else
				$query->query['tax_query'] = ["relation" => "AND", $push];
			if( $query->query_vars['tax_query'] )
				array_push($query->query_vars['tax_query'] , $push);
			else $query->query_vars['tax_query'] = ["relation" => "AND", $push];
				
			if($query->tax_query->queries)
				array_push($query->tax_query->queries , $push);
			else
				$query->tax_query->queries = ["relation" => "AND", $push];
			if($query->tax_query->queried_terms)
				null;//array_push($query->tax_query->queried_terms , $push);
			else
				$query->tax_query->queried_terms = ["relation" => "AND", $push];
			//array_push($query->query["tax_query"], $push);
			/*
			wp_die( $query );
			
			wp_die( $query->tax_query->queries );
			 
			//wp_die( $query->tax_query->queried_terms  );
			foreach($query->tax_query->queries as $q)
			{
				wp_die($q['taxonomy'] == BIO_COURSE_TYPE );
			}
			wp_die( $query->tax_query->queries[0] );
			*/
			
		}
	}
	static function posts_join_request( $join, $query )
	{
		return $join;
		$post_type =  $query->query[ 'post_type' ];
		if($post_type == BIO_ARTICLE_TYPE)
		{
			//if(current_user_can("manage_options")) return $join;
			wp_die( [ $join, $query ] );
			
		}
	}
	static function posts_where( $where )
	{
		//wp_die($where);
		return $where;
	}
	static function posts_where_paged( $where, $query )
	{
		return $where;
		$post_type =  $query->query[ 'post_type' ];
		// wp_die( Bio_User::is_user_cap( get_current_user_id(), BIO_ARTICLE_EDIT) );
		$if  	= current_user_can("manage_options");
		$if 	= $if || static::is_user_subscribe(1);
		wp_die( [$if, $query] );
	}
	
	static function get_article_facultet_ids( $article_id )
	{
		$article = Bio_Article::get_instance( $article_id );
		$bio_courses = wp_get_object_terms( $article_id, BIO_COURSE_TYPE, ["fields"=>"ids"] );
		$facultets = [];
		foreach($bio_courses as $bio_course)
		{
			$facultets[] = (int)get_term_meta($bio_course, BIO_FACULTET_TYPE, true);
		}
		return $facultets;
	}
	static function get_facultet_courses($facultet_ids, $byID=false)
	{
		if(!is_array($facultet_ids))
			$facultet_ids = [$facultet_ids];
		
		if( !count($facultet_ids) )
		{
			return [];
		}
		
		$metas = [];
		foreach($facultet_ids as $id)
		{
			$metas[] = [
				"key" => static::get_type(), 
				"value_numeric" => $id
			];
		}
		// TODO - прописать автора Факультета
		if( $byID)
		{
			$result = Bio_Course::get_all([ "metas" => $metas ]);
			$res = [];
			foreach($result as $r)
			{
				$res[] = $r->term_id;
			}
			return $res;
		}
		return Bio_Course::get_all([ "parent" => 0,  "metas" => $metas ]);
	}
	
	static function add_user_facultet($facultet_id, $user_id)
	{
		global $wpdb;
		$query = "INSERT INTO `".$wpdb->prefix."facultet_user` (`ID`, `facultet_id`, `user_id`, `date`) VALUES (NULL, '$facultet_id', '$user_id', CURRENT_TIMESTAMP);";
		$course	= get_term_by("id", $facultet_id, static::get_type());
		$user	= get_user_by("id", $user_id);
		Bio_Mailing::send_mail(
			sprintf( __("You are added to Facultet %s", BIO),  $course->name), 
			sprintf( __("You have gained access to the materials of the Faculty %s", BIO), $course->name) ,
			wp_get_current_user(), 
			[ $user->user_email ]
		);
		
		return $wpdb->query($query);
	}
	static function remove_user_facultet($facultet_id, $user_id )
	{
		if( apply_filters("before_remove_user_facultet",true, $facultet_id, $user_id ) )
		{
			global $wpdb;
			$query = "DELETE FROM `".$wpdb->prefix."facultet_user` WHERE facultet_id='$facultet_id' AND user_id='$user_id';";
			$wpdb->query($query);
			
			$course	= get_term_by("id", $facultet_id, static::get_type());
			$user	= get_user_by("id", $user_id);
			Bio_Mailing::send_mail(
				sprintf( __("You are removed from Facultet %s", BIO),  $course->name), 
				sprintf( __("Engine have remove access to the materials of the Faculty %s", BIO), $course->name) ,
				wp_get_current_user(), 
				[ $user->user_email ]
			);
			do_action("after_remove_user_facultet", $facultet_id, $user_id );
		}
		/**/
	}
	static function get_user_facultet( $user_id = null, $byID=false )
	{
		global $wpdb;
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		//if(!$user)
		//	return [];
		$query = "SELECT * FROM " . $wpdb->prefix . "facultet_user WHERE user_id={$user->ID}";
		if($byID)
		{
			$result = $wpdb->get_results($query);
			$res = [];
			foreach($result as $r)
			{
				$res[] = (int)$r->facultet_id;
			}
			return $res;
		}
		return $wpdb->get_results($query);
	}
	
	static function is_user_subscribe( $facultet_id, $user_id = null )
	{
		global $wpdb;
		$user = is_numeric( $user_id ) ? get_userdata( $user_id ) : wp_get_current_user();
		$query = "SELECT facultet_id FROM " . $wpdb->prefix . "facultet_user WHERE user_id={$user->ID} AND facultet_id = $facultet_id;";
		return $wpdb->get_var($query);
	}
	
	static function get_single_matrix_handler($matrix, $term, $type)
	{
		if( $type == BIO_FACULTET_TYPE )
		{
			$fac = static::get_instance( $term );
			$matrix['order']		= (int)$fac->get_meta("order");
			$matrix['locked']		= $fac->get_meta("locked");
			$matrix['adress']		= $fac->get_meta("adress");
			$matrix['description']	= $fac->get_meta("description");
			$matrix['title_descr']	= $fac->get_meta("title_descr");
		}
		return $matrix;
	}
	static function delete_term( $course_id, $tax_id, $deleted_term, $object_ids )
	{
		
	}
	static function create_taxonomy()
	{
		register_taxonomy(
			static::get_type(), 
			[ ], 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Facultet", BIO),
					'singular_name'     => __("Facultet", BIO),
					'search_items'      => __('search Facultet', BIO),
					'all_items'         => __('all Facultets', BIO),
					'view_item '        => __('view Facultet', BIO),
					'parent_item'       => __('parent Facultet', BIO),
					'parent_item_colon' => __('parent Facultet:', BIO),
					'edit_item'         => __('edit Facultet', BIO),
					'update_item'       => __('update Facultet', BIO),
					'add_new_item'      => __('add new Facultet', BIO),
					'new_item_name'     => __('new Facultet Name', BIO),
					'menu_name'         => __('Facultet', BIO),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_in_nav_menus' 	=> true,
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => false,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
		//wp_nav_menu_taxonomy_meta_boxes() ;
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == static::get_type() )
			$parent_file = 'pe_edu_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'pe_edu_page', 
			__("Facultets", BIO), 
			__("Facultets", BIO), 
			'manage_options', 
			'edit-tags.php?taxonomy=' . static::get_type()
		);
		add_meta_box( "add-{BIO_FACULTET_TYPE}", __("Courses", BIO), 'wp_nav_menu_item_taxonomy_meta_box', 'nav-menus', 'side', 'default', static::get_type() );	
    }
	static function ctg_columns($theme_columns) 
	{
		$new_columns = array
		(
			'cb' 			=> ' ',
			//'id' 			=> 'id',
			'name' 			=> __('Name'),
			'thumbnail'		=> __('Icon'),
			//'color'			=> __("Color", BIO),
			'adress'		=> __("Adress", BIO),
			'post_author'	=> __("Author", BIO),
			'locked'		=> __("Locked", BIO),
			'order'			=> __("Order", BIO),
		);
		return $new_columns;
	}
	static function manage_ctg_columns($out, $column_name, $term_id) 
	{
		switch ($column_name) {
			case 'id':
				$out 		.= $term_id;
				break;
			case 'order': 
				$order = get_term_meta( $term_id, 'order', true ); 
				$out 		.= $order;
				break;	
			case 'adress': 
				$adress = get_term_meta( $term_id, 'adress', true ); 
				$out 		.= $adress;
				break;	
			case 'post_author': 
				$post_author = get_term_meta( $term_id, 'post_author', true ); 
				$out 		.= get_user_by("id", $post_author)->display_name;
				break;	
			case 'color': 
				$color = get_term_meta( $term_id, 'color', true ); 
				$out 		.= "<div class='clr' style='background-color:$color;'></div>";
				break;	 
			case 'locked': 
				$locked = get_term_meta( $term_id, 'locked', true ); 
				$out 		.= $locked ? "<img src='" . BIO_URLPATH . "assets/img/check_checked.png'> <span class='smc-label-782px'>" . $obj[$column_name]['name'] . "</span>" 
				: 
				"<img src='" . BIO_URLPATH . "assets/img/check_unchecked.png'> <span class='smc-label-782px'>" . $obj[$column_name]['name'] . "</span>";
				break;	  
			case "thumbnail":
				$thumbnail = get_term_meta( $term_id, 'thumbnail', true ); 
				$logo = wp_get_attachment_image_src($thumbnail, "full")[0];
				echo "<img src='$logo' style='width:auto; height:60px; margin:10px;' />";
				break;	
			default:
				break;
		}
		return $out;    
	}
	
	static function add_ctg( $term, $tax_name )
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		if($term)
		{
			$term_id = $term->term_id;
			$description = get_term_meta($term_id, "description", true);
			$title_descr = get_term_meta($term_id, "title_descr", true);
			$color = get_term_meta($term_id, "color", true);
			$adress= get_term_meta($term_id, "adress", true);
			$price = get_term_meta($term_id, "price", true);
			$raiting= get_term_meta($term_id, "raiting", true);
			$order = get_term_meta($term_id, "order", true);
			$thumbnail  = get_term_meta($term_id, "thumbnail", true);
			$post_author  = get_term_meta($term_id, "post_author", true);
			$locked  = get_term_meta($term_id, "locked", true);
			$thumbnail  = is_wp_error($thumbnail) ? "" :  $thumbnail;
		}
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="thumbnail">
					<?php echo __("thumbnail", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php
					echo get_input_file_form2( "group_icon", $thumbnail, "group_icon", 0 );
				?>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="title_descr">
					<?php echo __("title_descr", BIO);  ?>
				</label> 
			</th>
			<td>
				<input name="title_descr" value="<?php echo $title_descr; ?>" type="text" />
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="adress">
					<?php echo __("Adress", BIO);  ?>
				</label> 
			</th>
			<td>
				<input name="adress" value="<?php echo $adress; ?>" type="text" />
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="description">
					<?php echo __("Locked", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php  wp_editor( $description, 'description', array(
					'wpautop'       => 1,
					'media_buttons' => 1,
					'textarea_name' => 'description', //нужно указывать!
					'textarea_rows' => 20,
					'tabindex'      => null,
					'editor_css'    => '',
					'editor_class'  => '',
					'teeny'         => 0,
					'dfw'           => 0,
					'tinymce'       => 1,
					'quicktags'     => 1,
					'drag_drop_upload' => false
				) )  ?>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="locked">
					<?php echo __("Locked", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="checkbox" name="locked" value="1"  <?php checked(1, $locked, 1); ?>/>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="order">
					<?php echo __("Order", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="number" name="order" value="<?php echo $order; ?>" />
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="raiting">
					<?php echo __("Raiting", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="number" name="raiting" value="<?php echo $raiting; ?>" />
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="price">
					<?php echo __("price", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="number" name="price" value="<?php echo $price; ?>" />
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="adress">
					<?php echo __("Author", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php
					wp_dropdown_users( array(
						'show_option_none'        => '--',
						'orderby'                 => 'display_name',
						'order'                   => 'ASC',
						'multi'                   => false,
						'show'                    => 'display_name',
						'echo'                    => true,
						'selected'                => $post_author,
						'include_selected'        => false,
						'name'                    => 'post_author',
						'id'                      => 'post_author',
						'class'                   => 'w-100',
						'blog_id'                 => $GLOBALS['blog_id'],
						'who'                     => '',
						'role'                    => 'CourseLeader',
						'role__in'                => array(),
						'role__not_in'            => array(),
					) );
				?>
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		update_term_meta($term_id, "color", 		$_POST['color']);
		update_term_meta($term_id, "title_descr", 	$_POST['title_descr']);
		update_term_meta($term_id, "description", 	$_POST['description']);
		update_term_meta($term_id, "adress",		$_POST['adress']);
		update_term_meta($term_id, "order",			$_POST['order']);
		update_term_meta($term_id, "post_author",	$_POST['post_author']);
		update_term_meta($term_id, "locked",		$_POST['locked']);
		update_term_meta($term_id, "price",			$_POST['price']);
		update_term_meta($term_id, "raiting",		$_POST['raiting']);
		if(!$_POST['group_icon0']) return;
			update_term_meta($term_id, "thumbnail",  $_POST['group_icon0']);
	}
	
	
	
	
	static function wp_dropdown($params=-1)
	{
		if(!is_array($params))
			$params = [];
		if($params['terms'])
		{
			$terms		=  $params['terms'];
		}
		else
		{
			$terms = get_terms( array(
				'taxonomy'      => static::get_type(), 
				'orderby'       => 'name', 
				'order'         => 'ASC',
				'hide_empty'    => false, 
				'fields'        => 'all', 
			) );
		}
		$html		= "<select ";
		if($params['class'])
			$html	.= "class='".$params['class']."' ";
		if($params['style'])
			$html	.= "style='".$params['style']."' ";
		if($params['name'])
			$html	.= "name='".$params['name']."' ";
		if($params['id'])
			$html	.= "id='".$params['id']."' ";
		if($params['special'])
		{
			$pars  	= explode(",", $params['special']);
			$html	.= "$pars[0]='$pars[1]' ";
		}
		$html		.= " >";
		$zero 		= $params['select_none'] ? $params['select_none'] : "---";
			if(!$params['none_zero'])
				$html	.= "<option value='-1' selected>$zero</option>";			
			
		if(count($terms))
		{
			foreach($terms as $term)
			{
				$html	.= "
				<option " . selected($term->term_id, $params['selected'], 0) . " value='".$term->term_id."'>".
					$term->name.
				"</option>";
			}
		}
		$html		.= apply_filters("bio_facultet_last_dropdown", "", $params, $terms) . "
		</select>";
		return $html;
	}
}
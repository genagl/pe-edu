<?php
class Bio_Favorite_Article
{
    static function get_type()
    {
        return "";
    }

    static function init()
    {

    }

    public static function api_action($type, $methods, $code, $pars, $user)
	{

        if(!is_user_logged_in())	throw new ExceptionNotLoggedREST();
        $articles	= [];
        switch($methods) {
            case "update":
				$article_id = (int)$code;
				$is_favor	= $pars['is_favor'];
				$article	= Bio_Article::get_instance($article_id);
				$article->add_to_favorite($is_favor);
				$articles	= Bio_Article::get_article( $article->body, true );
				$id			= $article->id;
				$msg 		= __($is_favor ? "Article added to your favorites" : "Article removed from your favorites", BIO);
                break;
            case "delete":
                break;
            case "create":
                break;
            case "read":
            default:

                $art_ids	= Bio_User::get_favorites($user->ID);
                $articles	= [];
                if(count( $art_ids ))
                    foreach($art_ids as $art_id)
                    {
                        if($art_id->post_id && $art_id->post_id > 0 )
                            $articles[] = Bio_Article::get_article( $art_id->post_id, false);
                    }
                break;
        }


        //$response->pars2		= Bio_Article::$args;

        return [
            "articles" => $articles,
            "is_favor" => $is_favor,
            "id" => $id,
			"pars" => $pars,
			"msg" => $msg
        ];

    }

    static function get_favorites($user_id)
    {
        global $wpdb;
        $query = "SELECT post_id FROM `" . $wpdb->prefix . "user_article` WHERE user_id='$user_id';";
        return $wpdb->get_results($query);
    }

    static function get_requests_count($user_id)
    {
        global $wpdb;
        $query = "SELECT post_id FROM `" . $wpdb->prefix . "user_article` WHERE user_id='$user_id';";
        return $wpdb->get_var($query);
    }

}
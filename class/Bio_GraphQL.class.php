<?php
//TODO :: проверка наличия плагинов wp-qraphql, wp-graphql-cors, wp-graphql-jwt-authentication

require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class Bio_GraphQLNotAdmin extends \Exception implements ClientAware
{
	 public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'Not success capabilities';
    }
}
class Bio_GraphQLNotLogged extends \Exception implements ClientAware
{
	 public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'bad token';
    }
}
class Bio_GraphQLNotAccess extends \Exception implements ClientAware
{
	 public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'Not success capabilities';
    }
}
class Bio_GraphQLError extends \Exception implements ClientAware
{
	public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'Not success capabilities';
    }
}
if ( ! function_exists( 'wp_password_change_notification' ) ) 
{
    function wp_password_change_notification( $user ) 
	{
        return;
    }
}
class Bio_GraphQL
{
	static function init()
	{ 
		/**/
		add_action("pe_graphql_make_schema", [__CLASS__, "exec_graphql"], 8);
		add_action("pe_graphql_make_schema", [__CLASS__, "auth"], 9);
		add_action("pe_graphql_make_schema", [__CLASS__, "graphql_register_input_types"], 10);
		add_action("pe_graphql_make_schema", [__CLASS__, "exec_options"], 10);
		add_action("pe_graphql_make_schema", [__CLASS__, "exec_comments"], 7);
		
		add_filter( 'send_password_change_email', '__return_false' );
		
	}
	static function exec_filtered( $obj, $options, $isForInput = false )
	{
		require_once( BIO_REAL_PATH."class/SMC_Object_type.php" );
		$SMC_Object_type = SMC_Object_Type::get_instance();	
			
		$type = Type::string();
		$fields		= [
			"id"	=> [
				"type" => $type
			]
		] ;
		foreach($obj as $key => $value)
		{
			$resolve = null; 
			switch( $obj[$key]['type'] )
			{
				case "boolean":
					$type = Type::boolean();
					break;
				case "date":
					$type = Type::int();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $SMC_Object_type ) // 
					{ 
						return $options[$key];
					};
					break;
				case "number":
					$type = Type::int();
					break;
				case "post_status":
				case "string":
				case "url":
					$type = Type::string();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $SMC_Object_type ) // 
					{ 
						return $options[$key];
					};
					break;
				case "period":
					$type = Type::listOf(Type::int());
					break;
				case "picto":
					$type = Type::string();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $SMC_Object_type ) // 
					{ 
						//родительский объект
						$media_id 	= $options[$key];
						$url 		= !is_wp_error($media_id) ? wp_get_attachment_url($media_id) : "";
						return $url;
					};
					break;
				case "media":
					$type = Type::string();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $SMC_Object_type ) // 
					{ 
						//родительский объект
						$media_id 	= $options[$key];
						$url 		= !is_wp_error($media_id) ? wp_get_attachment_url($media_id) : "";
						return $url;
					};
					break;
				case "geo":
					$type = Type::listOf(Type::string());
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $key, $SMC_Object_type ) // 
					{ 			
						// $geo = 
						return $key;
					};
					break;
				case "post":
				case "taxonomy":
					require_once( BIO_REAL_PATH."class/SMC_Object_type.php" );
					$SMC_Object_type = SMC_Object_Type::get_instance();		
					$child_type  	= $obj[ $key ][ 'object' ];
					$childs_data 	= $SMC_Object_type->object[ $child_type ];
					if($childs_data)
					{
						$type = $isForInput ? Type::int() : PEGraphql::object_type($childs_data['class']['type']);
					}
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $options, $child_type, $key, $SMC_Object_type, $obj ) // 
					{ 
						//родительский объект
						$instance = $options;
						
						// дочерний элемент, полученный по meta-полю 
						$classs 	= $child_type['smc_post'];
						
						//
						if($obj[$key]['type'] == "post")
						{ 
							return apply_filters(
								"bio-gq-post-meta", 
								$classs::get_single_matrix( $instance[ $key ] ),
								$instance,
								$classs,
								$key
							);
						}
						else if ($obj[$key]['type'] == "taxonomy")
						{
							$element 	= $classs::get_instance( $instance[ $key ] );							
							return  apply_filters(
								"bio-gq-term-meta", 
								$element->get_single_matrix( ),
								$instance,
								$classs,
								$key
							);
						}
					};
					break;
				/*
				case "array":	
					switch($obj[$key]['class'])
					{
						case "string":
							$list_type = Type::string();
							break;
						case "number":
							$list_type = Type::int();
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $child_type, $key, $SMC_Object_type, $obj ) // 
							{
								$instance  	= $class_name::get_instance($root['id']);
								$meta 		= $instance->get_meta( $key );
								if(!is_array($meta))
								{
									$meta = [];
								}
								return $meta;
							};
							break;
						case "special":	
							$list_type = apply_filters(
								"peql_array_special", 
								Type::string(), 
								$obj, 
								$key,
								$isForInput
							);
							//wp_die( $obj[ $key ] );
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $child_type, $key, $SMC_Object_type, $obj ) // 
							{
								return apply_filters(
									"peql_array_spesial_resolve", 
									[],
									$root, 
									$args, 
									$context, 
									$info, 
									$class_name, 
									$child_type, 
									$key, 
									$SMC_Object_type, 
									$obj 
								);
							};
							break;
						default:
							$list_type = Type::string();
							//wp_die( [$class_name, $obj[$key]['class']] );
							require_once( BIO_REAL_PATH."class/SMC_Object_type.php" );
							$SMC_Object_type = SMC_Object_Type::get_instance();
							switch( $obj[$key]['class'] )
							{
								case "taxonomy":
									$child_type = "SMC_Taxonomy";
									break;
								case "post":
								default:
									$child_type = "SMC_Post";
									break;
							}
							foreach( $SMC_Object_type->object as $_key => $_value)
							{
								if( $SMC_Object_type->object[$_key]['class']['type'] == $obj[$key]['class'] )
								{
									$list_type = $isForInput 
										? 
										Type::int() 
										: 
										PEGraphql::object_type($SMC_Object_type->object[$_key]['class']['type']);
										
									$child_type = $SMC_Object_type->object[$_key]['class']['type'];
									break;
								}
							}
							$resolve =  function( 
								$root, 
								$args, 
								$context, 
								$info 
							) use(
								$class_name, 
								$child_type, 
								$obj, 
								$key,
								$SMC_Object_type
							) 
							{
								if (!class_exists($child_type) || !method_exists($child_type, "get_type"))
								{
									return;
								}
								$cl 			= $obj[$key]['class'];	
								$order 			= $obj[$key]['order'];	
								$orderby		= $obj[$key]['orderby'];	
								$element_type 	= $SMC_Object_type->object[ $cl::get_type() ]['t']['type'];
								$parent_type 	= $SMC_Object_type->object[ $class_name::get_type() ]['t']['type'];
								$results = [];
								if( $element_type == "post" )
								{
									$args = [
										"post_type"		=> $child_type::get_type(),
										"count"			=> 1000,
										"author"		=> -1,
										"post_status"	=> "publish"
											
									];
									switch($parent_type)
									{
										case "post":
											$args["relation"] 	= "AND"; 
											$args["fields"] 	= "all"; 
											$args["order"] 		= "DESC"; 
											$args["order_by"] 	= "title"; 
											$args["post_status"]= "publish"; 
											$args["offset"] 	= 0; 
											$args["metas"] 		= [ 
												[
													"key" 			=> $class_name::get_type(), 
													"value_numeric"	=> (int)$root["id"],
													"compare"		=> "="
												] 
											]; 
											break;
										case "taxonomy":
										default:
											$args['tax_query'] 	= [
												[	
													'taxonomy' 	=> $class_name::get_type(),
													'field'    	=> 'id',
													'terms'    	=> $root["id"],
												]
											];
											$args['taxonomies'] = [
												[
													"tax_name"	=> $class_name::get_type(),
													"term_ids"	=> $root["id"]
												]
											];
											
									}
									if( isset($orderby) )
									{
										switch($orderby)
										{
											case "id":
											case "post_title":
											case "post_author":
												break;
											default:
												$args["orderby"] = "meta_value_num";
												$args["meta_key"]= $orderby;
												break;
										}
									}
									if( isset($order) )
									{
										$args['order'] = $order;
									} 
									$results 	= $child_type::get_all_matrixes($args);
									//wp_die( [ $args, $results ] );
								}
								else if($element_type == "taxonomy")
								{
									$args = [
										"number"		=> 10000,
										'taxonomy'      => [ $cl::get_type() ],
										'hide_empty' 	=> false,
										"meta_query"	=> [
											'relation' 	=> 'AND',
											[
												"key" 	=> $class_name::get_type(),
												"value"	=> $root["id"],
												"compare" => "=",
												//'type'    => 'NUMERIC'
											]
										]
									];
									$subobjects = get_terms( $args );
									// wp_die( $args );
									foreach($subobjects as $cc)
									{
										$course 	= Bio_Course::get_instance($cc->term_id);
										$results[] 	= $course->get_single_matrix();
										
									}
									//wp_die( $results );
								}
								
								return $results;
							};
					}
					if(!$isForInput)
					{
						$type = Type::listOf( $list_type );
					}
					break;
				*/
				case "id":
					$type = Type::int();
					break;
				case "user":
					$type = $isForInput ? Type::int() : PEGraphql::object_type("User");
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $SMC_Object_type, $obj ) // 
					{
						//родительский объект
						$instance = $options;	
						// wp_die( $meta );
						$user = Bio_User::get_user( $instance[$key] );
						return $user;
					};
					break;					
				default: 
					$type = Type::string();
			}
			if( $type )
			{
				$fields[ $key ] = [ 
					'type' 			=> $type, 	
					'description' 	=> __( $obj[$key]['name'], BIO )  . " " . __( $obj[$key]['description'], BIO ),
					"resolve" 		=> $resolve,
				];
			}
		}
		
		return $fields;
	}
	static function get_exec_filtered( $isForInput = false )
	{
		require_once( BIO_REAL_PATH."class/SMC_Object_type.php" );
		$SMC_Object_type 	= SMC_Object_Type::get_instance();	
		return static::exec_filtered( $SMC_Object_type->options, BIO::$options, $isForInput ); 	  
		
	}
	static function exec_options()
	{ 
		PEGraphql::add_object_type( 
			[
				"name"			=> "Options",
				"description"	=> __( "main options of portal", BIO ),
				"fields"		=> apply_filters( "bio_gq_options", static::get_exec_filtered() )
			]
		);
		PEGraphql::add_object_type( 
			[
				"name"			=> "PublicOptions",
				"description"	=> __( "main public options of portal", BIO ),
				"fields"		=> apply_filters( "bio_public_gq_options", [ ] )
			]
		);
		
		PEGraphql::add_input_type( 
			[
				"name"		=> 'OptionsInput',
				'description' => __( "main options of portal", BIO ),
				'fields' 		=> apply_filters( "bio_gq_options_input", static::get_exec_filtered( true ) ),
			]
		);
		
		PEGraphql::add_query( 
			'getOptions', 
			[
				'description' => __( 'Get portal settings', BIO ),
				'type' 		=> PEGraphql::object_type("Options"),
				'args'     	=> [ 
					"id"	=> Type::string()	
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{		
					if(!current_user_can("manage_options"))
						throw new \Bio_GraphQLNotAccess( "you not right!" );
					return apply_filters( "bio_get_gq_options", [ ] );
				}
			] 
		);
		
		PEGraphql::add_query( 
			'getPublicOptions', 
			[
				'description' => __( 'Get portal public settings', BIO ),
				'type' 		=> PEGraphql::object_type("PublicOptions"),
				'args'     	=> [
					"id"	=> Type::string()	
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{		
					return apply_filters( "bio_get_public_gq_options", [ ] );
				}
			] 
		);
		
		PEGraphql::add_mutation ( 
			'changeOptions', 
			[
				'description' 	=> __( "Change portal settings for Administrator", BIO ),
				'type' 			=> PEGraphql::object_type("Options"),
				'args'         	=> [
					"id"	=> Type::string(),
					'input' => [
						'type' => PEGraphql::input_type("OptionsInput"),
					]
				],
				'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{	
					if(!current_user_can("manage_options"))
						throw new \Bio_GraphQLNotAccess( "you not right!" );
					
					require_once( BIO_REAL_PATH."class/SMC_Object_type.php" );
					$SMC_Object_type 	= SMC_Object_Type::get_instance();
					$output = []; 
					if( is_array($options) || true )
					{
						foreach( $SMC_Object_type->options as $key => $value )
						{
							if( !isset( $args["input"][ $key ] ) ) continue;
							 
							switch( $value['type'] )
							{
								case "media": 
									if( $args["input"][ $key ] && substr($args["input"][ $key ],0, 4) != "http" )
									{
										$media = Bio_Assistants::insert_media(
											[
												"data" => $args["input"][ $key ],
												"media_name" => $args["input"][ $key . '_name' ] 
													? 
													$args["input"][ $key . '_name' ] 
													: 
													"Options_" . $key . ".jpg"
											]
										); 
										wp_set_object_terms(
											$media['id'], 
											(int)Bio::$options['icon_media_term'],
											BIO_MEDIA_TAXONOMY_TYPE 
										); 
										$output[ $key ] = $media['url'];
										Bio::$options[ $key ] = $media['id'];
									}
									
									break;
								case "string":
								default:
									$output[ $key ] = $args[ "input" ][ $key ];
									Bio::$options[ $key ] = $args[ "input" ][ $key ];
									break;
							}
							
						}
					}
					update_option(BIO, Bio::$options); 
					return $output; 
				}
			] 
		);
	}
	static function exec_comments()
	{
		
		PEGraphql::add_object_type( 
			[
				"name"			=> "Comment",
				"description"	=> __( "single message in hierarhical post's discussion", BIO ),
				"fields"		=> apply_filters( "bio_gq_comment", [ 
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO )
					], 
					"discussion_id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( "Post's disscussion id", BIO )
					], 
					"discussion_type" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( "post or taxonomy", BIO ), 
						'name' => 'discussion_type' 
					], 
					"discussion"  			=> [
						'type' => PEGraphql::object_type("Post"), 	
						'description' 	=> __( "post or taxonomy element", BIO ),
						"resolve"		=> function( $root, $args, $context, $info )
						{
							if( $root["discussion_type"] == "post" )
							{
								$matrix = get_post($root["discussion_id"]);
								return [
									"id"			=> $matrix->ID,
									"post_title"	=> $matrix->post_title,
									"post_name"		=> $matrix->post_name,
									"post_type"		=> $matrix->post_type,
								];
							}
							else if( $root["discussion_type"] == "term" )
							{											
								$matrix = get_term($root["discussion_id"]);
								//wp_die( [$root["discussion_id"], $matrix] );
								return [
									"id"			=> $matrix->term_id,
									"post_title"	=> $matrix->name,
									"post_name"		=> $matrix->slug,
									"post_type"		=> $matrix->type,
								];
							}
							else
							{
								throw new \Bio_GraphQLNotAccess( "unknown discussion type" );
							}
						}						
					],
					"content" 				=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'text of comment', BIO )
					], 
					"parent_id" 		=> [ 
						'type' => Type::string(),		
						'description' 	=> __( "parent comment's id", BIO )
						
					], 
					"parent" 		=> [ 
						'type' => PEGraphql::object_type("Comment"),		
						'description' 	=> __( 'parent', BIO ),
						"resolve"	=> function( $root, $args, $context, $info )
						{
							
							return Bio_Comment::get_single_matrix( $root["parent_id"] );
						}
					], 
					"author"		=> [ 
						'type' => PEGraphql::object_type("User"), 			
						'description' 	=> __( 'authorize author of comment', BIO ),
						"resolve"	=> function( $root, $args, $context, $info )
						{
							// wp_die($root);
							if( isset($root["user_id"]) &&  $root["user_id"]> 0)
							{
								$user = Bio_User::get_user( $root['user_id'] );
							}
							else
							{
								$user = apply_filters(
									"bio_get_user",
									[
										"id"				=> -1,
										"ID"				=> -1,
										"display_name"		=> $root['comment_author'],
										"first_name"		=> $root['comment_author'],
										"last_name"			=> $root['comment_author'],
										"roles"				=> [],
										"caps"				=> [],
										"caps1"				=> [],			
										"user_email"		=> $root['comment_author_email'],
										"account_activated"	=> false,
										"is_blocked"		=> true,
										"__typename" 		=> "User"
									],
									new StdClass
								);
							}
							return $user;
						}
						
					], 
					"date" 		=> [ 
						'type' => Type::int(), 		
						'description' 	=> __( 'UNIXtime int. Date of creation.', BIO )
					], 
					"is_approved"		=> [ 
						'type' => Type::boolean(), 		
						'description' 	=> __( 'Approve', BIO )
					]
				] )
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"		=> 'CommentPaging',
				'description' => __( "single message in hierarhical post's discussion", BIO ),
				'fields' 		=> apply_filters( "bio_gq_commentpaging_input", [ 
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 
						'name' => 'id' 
					],
					"offset" => [
						'type' => Type::int(), 	
						'description' 	=> __( 'Pagination offset', BIO ), 
					],
					"count" => [
						'type' => Type::int(), 	
						'description' 	=> __( "Pagination page's count of elements", BIO ),
						"default"		=> 10						
					],
					"parent" => [
						'type' => Type::string(), 	
						'description' 	=> __( 'Parent Comment by this.', BIO ), 
					],
					"discussion_id" => [
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 
					],
					"discussion_type" => [
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 
					],
					'is_admin'	=> [
						'type' 	=> Type::boolean(),
						'description' => __( 'For admin panel', BIO ),
						"defaultValue" => false
					]
				])
			]
		);
		PEGraphql::add_input_type( 
			[
				"name"		=> 'CommentInput',
				'description' => __( "single message in hierarhical post's discussion", BIO ),
				'fields' 		=> apply_filters( "bio_gq_comment_input", [ 
					"id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 
						'name' => 'id' 
					], 
					"author" 		=> [ 
						'type' => Type::int(), 	
						'description' 	=> __( "post or taxonomy", BIO ), 
						'name' => 'author' 
					],
					"discussion_id" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( "Post's disscussion id", BIO ), 
						'name' => 'discussion_id' 
					], 
					"discussion"  			=> [
						'type' => Type::int(), 	
						'description' 	=> __( "post or taxonomy element id", BIO )
					],
					"discussion_type" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( "post or taxonomy", BIO ), 
						'name' => 'discussion_type' 
					], 
					"content" 		=> [ 
						'type' => Type::string(), 	
						'description' 	=> __( 'text of comment', BIO )
						
					], 
					"parent" 		=> [ 
						'type' => Type::int(),		
						'description' 	=> __( 'parent', BIO )
						
					], 
					"author" 		=> [ 
						'type' => Type::int(), 			
						'description' 	=> __( 'authorize author of comment', BIO )
						
					], 
					"date"		=> [ 
						'type' => Type::int(), 		
						'description' 	=> __( 'UNIXtime int. Date of creation.', BIO ) 
						
					], 
					"is_approved"		=> [ 
						'type' => Type::boolean(), 		
						'description' 	=> __( 'Approve', BIO ) 
						
					], 
					"is_admin"		=> [ 
						'type' => Type::boolean(), 		
						'description' 	=> __( 'For admin panel', BIO ) 
						
					]
				]),
			]
		);
		
		PEGraphql::add_query( 
			'getComments', 
			[
				'description' 		=> __( 'Get comments array', BIO ),
				'type' 				=> Type::listOf( PEGraphql::object_type("Comment") ),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("CommentPaging") 
					]
				],
				'resolve' 			=> function( $root, $args, $context, $info )
				{	
					//wp_die($args['paging']);
					return Bio_Comment::get_matrixes($args['paging']);
				}
			] 
		);
		PEGraphql::add_query( 
			'getCommentCount', 
			[
				'description' 		=> __( 'Get comments count', BIO ),
				'type' 				=> Type::int(),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("CommentPaging") 
					]
				],
				'resolve' 			=> function( $root, $args, $context, $info )
				{	
					return count(Bio_Comment::get_matrixes($args['paging']));
				}
			] 
		);
		PEGraphql::add_query( 
			'getComment', 
			[
				'description' => __( 'Get single comment', BIO ),
				'type' 		=> PEGraphql::object_type("Comment"),
				'args'     	=> [ 
					"id"	=> [
						"type" => Type::string()
					]
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{		
					return Bio_Comment::get_single_matrix( $args["id"] );
				}
			] 
		);
		
		PEGraphql::add_mutation ( 
			'changeComment', 
			[
				'description' 	=> __( "Change single comment", BIO ),
				'type' 			=> Type::listOf( PEGraphql::object_type("Comment")),
				'args'         	=> [
					"id"	=> [
						"type" => Type::string(),
					],
					'input' => [
						'type' => PEGraphql::input_type("CommentInput"),
					]
				],
				'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{		
					// wp_die( $args );
					if(!isset($args['id']))
					{
						$user = current_user_can("manage_option") 
							? 
							get_user_by("id", $args['input']['author'])
							:
							wp_get_current_user(); 
						
						if( !isset( $args['input']['discussion_id'] ) )
						{
							if( $args['input']['parent'] > 1)
							{
								$comment = Bio_Comment::get_single_matrix($args['input']['parent']);
								$args['input']['discussion_id'] 	= $comment['discussion_id'];
								$args['input']['discussion_type'] 	= $comment['discussion_type'];
							}
							else
							{
								return;
							}
						}						
						
						$new_comment = wp_insert_comment( 
							apply_filters(
								"bio_gq_change_comment", [
									'comment_post_ID'	=> $args['input']['discussion_id'],
									'discussion_type'	=> $args['input']['discussion_type'],
									"comment_approved"	=> true,
									"user_id"			=> $user->ID,
									"comment_author"	=> $user->display_name,
									"comment_author_email" => $user->user_email,
									"comment_content"	=>  $args['input']['content'],
									"comment_parent"	=>  $args['input']['parent'],
									"comment_date"	 	=>  null
								],
								null
							)
						);
						if( $new_comment && $args['input']['discussion_type'] == "term" )
						{
							global $wpdb;
							$wpdb->query("UPDATE " . $wpdb->prefix . "comments SET discussion_type='term' ");
							//wp_die( [ $new_comment, $boo, $args['input']['discussion_type'] ] );
							
							$_comment = get_comment( $new_comment );
							$_comment->discussion_type = 'term';
							wp_cache_set( $_comment->comment_ID, $_comment, 'comment' );
						}
						//return Bio_Comment::get_single_matrix($new_comment);
						return Bio_Comment::get_matrixes([
							"post_id" => $args['input']['discussion_id'] ,
							"status" => "approve" 
						]);
					}
					else
					{
						$user = current_user_can("manage_option") 
							? 
							get_user_by("id", $args['input']['author'])
							:
							get_current_user_id(); 
						$new_comment = wp_update_comment( 
							apply_filters(
								"bio_gq_change_comment", [
									'comment_post_ID'	=> $args['input']['discussion_id'],
									"comment_approved"	=> true,
									"user_id"			=> $user->ID,
									"comment_author"	=> $user->display_name,
									"comment_author_email" => $user->user_email,
									"comment_content"	=>  $args['input']['content'],
									"comment_parent"	=>  $args['input']['parent'],
									"comment_date"	 	=>  null, //$args['input']['date'], 
								],
								$args['id']
							)
						);
						return  Bio_Comment::get_single_matrix($new_comment);
					}
					return false;
				}
			] 
		);
		
		
			PEGraphql::add_mutation( 
				'deleteComment', 
				[
					'description' 	=> __( "Delete single comment", BIO ),
					'type' 			=> Type::boolean(),
					'args'         	=> [
						"id"	=> [
							'type' => Type::string(),
							'description' => __( 'Unique identificator', BIO ),
						]
					],
					'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						return wp_delete_comment( $args['id'], true );
					}
				] 
			);
	}

	static function exec_graphql()
	{
		try
		{
			static::register_smc_posts( );
		}
		catch(Bio_GraphQLNotAccess $ew)
		{
			
		}
		catch(Bio_GraphQLNotLogged $ew)
		{
			
		}
		catch(Bio_GraphQLNotAdmin $ew)
		{
			
		}
		catch(Bio_GraphQLError $ew)
		{
			
		}
	}
	
	static function getFields( $obj, $post_type, $val = "", $isForInput = false )
	{
		$class_name				= $obj['class']['type'];
		$class_type 			= $obj['t']['type'];
		$arr[] 					= $class_name;
		$fields 				= [] ;	
		if(!$isForInput)
		{
			$fields["id"] 		= [ 
				'type' => Type::string(), 	
				'description' 	=> __( 'Uniq identificator', BIO ), 'name' => 'id' 
			];
		}
		$fields["post_title"] 	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'title', BIO ) 
		];		
		$fields["post_name"] 	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'slug', BIO ) 
		];			
		$fields["post_content"]	= [ 
			'type' => Type::string(), 	
			'description' 	=> __( 'content. May be empty.', BIO )
		];
		$fields["post_status"]	= [ 
			'type' => Type::string(), 	
			'description' 	=> __( '"publish" or "draft"', BIO ) 
		];
		$fields["post_date"] 	= [ 
			'type' => Type::int(), 		
			'description' 	=> __( 'UNIXtime int. Date of creation.', BIO ) 
		];
		$fields["thumbnail"] 	= [ 
			'type' => Type::string(), 	
			'description' 	=> __( 'URL of media', BIO ) 
		];
		$fields["thumbnail_name"] = [ 
			'type' => Type::string(), 		
			'description' 	=> __( 'file name of thumbnail. Need for uploading new thumbnail', BIO ) 
			
		];
		$fields["count"] = [ 
			'type' => Type::int(), 		
			'description' 	=> __( 'Only for group selection. Count of elements in group', BIO ) 
			
		];
		$fields["offset"] = [ 
			'type' => Type::int(), 		
			'description' 	=> __( 'Only for group selection. Offset in group by zero.', BIO ) 
			
		];
		$fields["parent"]		 = [
			'type' => $isForInput ? Type::int() : PEGraphql::object_type($class_name),		
			'description' 	=> __( 'Parent element', BIO )  ,
			'resolve' 		=> function( $root, $args, $context, $info )use( $class_name, $class_type, $post_type )
			{
				if($class_type == "post")
				{
					return null;
				}
				else if($class_type == "taxonomy")
				{
					$term = get_term($root['id'], $post_type);
					if( $root['parent'] )
					{
						$term = $class_name::get_instance( $root['parent'] );
						return $term->get_single_matrix();
					}
					else
					{
						return null;
					}
				}
				else
				{
					return null;
				}
			}
		];
		$fields["main_parent"]		 = [
			'type' => $isForInput ? Type::int() : PEGraphql::object_type($class_name),		
			'description' 	=> __( 'Top-level parent element', BIO )  ,
			'resolve' 		=> function( $root, $args, $context, $info )use( $class_name, $class_type, $post_type )
			{
				if($class_type == "post")
				{
					return null;
				}
				else if($class_type == "taxonomy")
				{
					if( $root['parent'] )
					{
						$parent = get_term_top_most_parent($root['id'], $post_type);
						$term = $class_name::get_instance( $parent );
						return $term->get_single_matrix();
					}
					else
					{
						return null;
					}
				}
				else
				{
					return null;
				}
			}
		];
		$fields["post_author"] 	 = [ 
			'type' => $isForInput ? Type::int() : PEGraphql::object_type("User"), 	
			'description' 	=> __( 'Author of this element', BIO ) ,
			'resolve' 		=> function( $root, $args, $context, $info )
			{
				$user = Bio_User::get_user( $root['post_author'] );
				//wp_die($user);
				return $user['id'] ? $user : null;
			}
		];
		$fields["children"] = [ 
			'type' => Type::listOf($isForInput ? Type::int() : PEGraphql::object_type($class_name)), 	
			'description' 	=> __( '', BIO ),
			'resolve' 		=> function( $root, $args, $context, $info )use( $class_name, $class_type, $post_type )
			{
				$children = [];
				if($class_type == "post")
				{
					return $children;
				}
				else if($class_type == "taxonomy")
				{
					$childs = $class_name::get_all( [ "parent" => $root[ "id" ], "order_by_meta" => "order" ] );
					foreach($childs as $child)
					{
						$ch = $class_name::get_instance( $child );
						$children[] = $ch->get_single_matrix( [ 'children' => $ch->id ] );
					}
					return $children;
				}
				else
				{
					return null;
				}
			}
		];
		
		$type = Type::string();
		
		foreach($obj as $key => $value)
		{
			/*
			$obj
			$key
			$value
			$class_name
			$child_type
			$SMC_Object_type
			
			
			*/
			
			
			$resolve = null;
			if($key == 't' ||$key == 'class' ) continue;
			switch( $obj[$key]['type'] )
			{
				case "boolean":
					$type = Type::boolean();
					break;
				case "date":
					$type = Type::int();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $SMC_Object_type ) // 
					{
						//wp_die( [ $key ] );						
						$inst = $class_name::get_instance($root['id']);
						return $inst->get_meta($key);
					};
					break;
				case "number":
					$type = Type::int();
					break;
				case "post_status":
				case "string":
					$type = Type::string();
					break;
				case "period":
					$type = Type::listOf(Type::int());
					break;
				case "picto":
				case "media":
					$type = Type::string();
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $SMC_Object_type ) // 
					{
						//родительский объект
						$instance 	= $class_name::get_instance( $root['id'] );
						$media_id 	= $instance->get_meta( $key );
						$url 		= !is_wp_error($media_id) ? wp_get_attachment_url($media_id) : "";
						return $url;
					};
					break;
				case "post":
				case "taxonomy":
					require_once( BIO_REAL_PATH."class/SMC_Object_type.php" );
					$SMC_Object_type = SMC_Object_Type::get_instance();		
					$child_type  	= $obj[ $key ][ 'object' ];
					$childs_data 	= $SMC_Object_type->object[ $child_type ];
					if($childs_data)
					{
						$type = $isForInput ? Type::int() : PEGraphql::object_type($childs_data['class']['type']);
					}
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $SMC_Object_type, $obj ) // 
						{
							if (!class_exists($child_type) || !method_exists($child_type, "get_type"))
							{
								return null;
							}
							// wp_die( $obj );
							//родительский объект
							$instance = $class_name::get_instance( $root['id'] );
							
							// дочерний элемент, полученный по meta-полю
							$classs 	= $SMC_Object_type->get_class_by_name( $child_type );
							
							//
							if($obj[$key]['type'] == "post")
							{ 
								return apply_filters(
									"bio-gq-post-meta", 
									$classs::get_single_matrix( $instance->get_meta( $key )),
									$instance,
									$classs,
									$key
								);
							}
							else if ($obj[$key]['type'] == "taxonomy")
							{
								$element 	= $classs::get_instance( $instance->get_meta( $key ) );							
								return  apply_filters(
									"bio-gq-term-meta", 
									$element->get_single_matrix( ),
									$instance,
									$classs,
									$key
								);
							}
						};
					break;
				case "array":	
					switch($obj[$key]['class'])
					{
						case "string":
							$list_type = Type::string();
							break;
						case "number":
							$list_type = Type::int();
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $child_type, $key, $SMC_Object_type, $obj ) // 
							{
								$instance  	= $class_name::get_instance($root['id']);
								$meta 		= $instance->get_meta( $key );
								if(!is_array($meta))
								{
									$meta = [];
								}
								return $meta;
							};
							break;
						case "special":	
							$list_type = apply_filters(
								"peql_array_special", 
								Type::string(), 
								$obj, 
								$key,
								$isForInput
							);
							//wp_die( $obj[ $key ] );
							$resolve =  function(
								$root, 
								$args, 
								$context, 
								$info 
							) use( $class_name, $child_type, $key, $SMC_Object_type, $obj ) // 
							{
								return apply_filters(
									"peql_array_spesial_resolve", 
									[],
									$root, 
									$args, 
									$context, 
									$info, 
									$class_name, 
									$child_type, 
									$key, 
									$SMC_Object_type, 
									$obj 
								);
							};
							break;
						default:
							$list_type = Type::string();
							//wp_die( [$class_name, $obj[$key]['class']] );
							require_once( BIO_REAL_PATH."class/SMC_Object_type.php" );
							$SMC_Object_type = SMC_Object_Type::get_instance();
							switch( $obj[$key]['class'] )
							{
								case "taxonomy":
									$child_type = "SMC_Taxonomy";
									break;
								case "post":
								default:
									$child_type = "SMC_Post";
									break;
							}
							foreach( $SMC_Object_type->object as $_key => $_value)
							{
								if( $SMC_Object_type->object[$_key]['class']['type'] == $obj[$key]['class'] )
								{
									$list_type = $isForInput 
										? 
										Type::int() 
										: 
										PEGraphql::object_type($SMC_Object_type->object[$_key]['class']['type']);
										
									$child_type = $SMC_Object_type->object[$_key]['class']['type'];
									break;
								}
							}
							$resolve =  function( 
								$root, 
								$args, 
								$context, 
								$info 
							) use(
								$class_name, 
								$child_type, 
								$obj, 
								$key,
								$SMC_Object_type
							) 
							{
								if (!class_exists($child_type) || !method_exists($child_type, "get_type"))
								{
									return;
								}
								$cl 			= $obj[$key]['class'];	
								$order 			= $obj[$key]['order'];	
								$orderby		= $obj[$key]['orderby'];	
								$element_type 	= $SMC_Object_type->object[ $cl::get_type() ]['t']['type'];
								$parent_type 	= $SMC_Object_type->object[ $class_name::get_type() ]['t']['type'];
								$results = [];
								if( $element_type == "post" )
								{
									$args = [
										"post_type"		=> $child_type::get_type(),
										"count"			=> 1000,
										"author"		=> -1,
										"post_status"	=> "publish"
											
									];
									switch($parent_type)
									{
										case "post":
											$args["relation"] 	= "AND"; 
											$args["fields"] 	= "all"; 
											$args["order"] 		= "DESC"; 
											$args["order_by"] 	= "title"; 
											$args["post_status"]= "publish"; 
											$args["offset"] 	= 0; 
											$args["metas"] 		= [ 
												[
													"key" 			=> $class_name::get_type(), 
													"value_numeric"	=> (int)$root["id"],
													"compare"		=> "="
												] 
											]; 
											break;
										case "taxonomy":
										default:
											$args['tax_query'] 	= [
												[	
													'taxonomy' 	=> $class_name::get_type(),
													'field'    	=> 'id',
													'terms'    	=> $root["id"],
												]
											];
											$args['taxonomies'] = [
												[
													"tax_name"	=> $class_name::get_type(),
													"term_ids"	=> $root["id"]
												]
											];
											
									}
									if( isset($orderby) )
									{
										switch($orderby)
										{
											case "id":
											case "post_title":
											case "post_author":
												break;
											default:
												$args["orderby"] = "meta_value_num";
												$args["meta_key"]= $orderby;
												break;
										}
									}
									if( isset($order) )
									{
										$args['order'] = $order;
									}
									
									/*
									$subobjects = get_posts( $args );
									//wp_die($args);
									
									foreach($subobjects as $subobject)
									{
										$results[] = $child_type::get_single_matrix( $subobject );
									}
									*///
									$results 	= $child_type::get_all_matrixes($args);
									//wp_die( [ $args, $results ] );
								}
								else if($element_type == "taxonomy")
								{
									$args = [
										"number"		=> 10000,
										'taxonomy'      => [ $cl::get_type() ],
										'hide_empty' 	=> false,
										"meta_query"	=> [
											'relation' 	=> 'AND',
											[
												"key" 	=> $class_name::get_type(),
												"value"	=> $root["id"],
												"compare" => "=",
												//'type'    => 'NUMERIC'
											]
										]
									];
									$subobjects = get_terms( $args );
									// wp_die( $args );
									foreach($subobjects as $cc)
									{
										$course 	= Bio_Course::get_instance($cc->term_id);
										$results[] 	= $course->get_single_matrix();
										
									}
									//wp_die( $results );
								}
								
								return $results;
							};
					}
					if(!$isForInput)
					{
						$type = Type::listOf( $list_type );
					}
					break;
				case "id":
					$type = Type::int();
					break;
				case "user":
					$type = $isForInput ? Type::int() : PEGraphql::object_type("User");
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $SMC_Object_type, $obj ) // 
					{
						
						//родительский объект
						$instance = $class_name::get_instance( $root['id'] );
						
						if($obj[$key]['type'] == "post")
						{ 
							$meta = $instance->get_meta( $key );
						}
						else
						{
							$meta = $instance->get_meta( $key );
						}
						// wp_die( $meta );
						$user = Bio_User::get_user( $meta );
						return $user;
					};
					break;
				case "geo":
					$type = Type::listOf(Type::float());
					$resolve =  function(
						$root, 
						$args, 
						$context, 
						$info 
					) use( $class_name, $child_type, $key, $SMC_Object_type, $obj ) // 
					{
						//wp_die( $key );
						//wp_die($obj[$key]['type']);
						//wp_die($root[$key]);
						$geo = [];
						foreach($root[$key] as $value)
						{
							$geo[] = (float)$value;
						}
						return $geo;
					};
					break;
				default:
					//wp_die(  $obj[$key]['type'] );
					$type = Type::string();
			}
			
			if($type)
			{
				$fields[ $key ] = [ 
					'type' 			=> $type, 	
					'description' 	=> __( $obj[$key]['name'], BIO )  . " " . __( $obj[$key]['description'], BIO ),
					"resolve" 		=> $resolve,
				];
			}
		}
		$fields['comments'] =  [
			'type' 			=> Type::listOf($isForInput ? Type::int() : PEGraphql::object_type("Comment")), 	
			'description' 	=> __( "Comments list", BIO ),
			"resolve" =>  function( $root, $args, $context, $info ) use($class_type)
			{
				//wp_die( $class_type );
				return Bio_Comment::get_matrixes([ "discussion_id" => $root['id'], "discussion_type" => $class_type ]);
			}		
		];
		
		// add post's taxonomy terms
		if($obj['t']['type'] == "post")
		{
			$taxonomy_names = get_object_taxonomies( $post_type );			
			foreach($taxonomy_names as $tax)
			{
				require_once( BIO_REAL_PATH."class/SMC_Object_type.php" );
				$SMC_Object_type = SMC_Object_Type::get_instance();		
				$childs_data 	= $SMC_Object_type->object[ $tax ];
				if($childs_data)
				{
					$class_named = $childs_data['class']['type'];
					$fields[ $tax ] = [ 
						'type' 			=>  Type::listOf($isForInput ? Type::int() : PEGraphql::object_type($class_named)),
						'description' 	=> "taxonomy",
						"resolve" =>  function( $root, $args, $context, $info ) use($class_name, $class_named, $post_type) // 
						{
							
							if (!class_exists($class_named) || !method_exists($class_named, "get_type"))
							{
								return;
							}
							
							$results = [];
							$terms	= get_the_terms($root["id"], $class_named::get_type());
							if($terms)
							{
								foreach($terms as $term)
								{
									$__term 	= $class_named::get_instance($term );									
									$results[] = $__term->get_single_matrix();
								}
							}
							
							return $results;
						}
					];
				}
			}
		}
		
		return [
			"class_name" => $class_name, 
			"fields" => apply_filters("bio_gq_get_fields", $fields, $class_name, $post_type, $isForInput ), 
			"type" => $type 
		];
	}
	static function register_smc_posts( )
	{
		
		PEGraphql::add_object_type( 
			[
				"name"			=> "Label",
				"description"	=> __( "Unediteble text", BIO ),
				"fields"		=> [
					"post_content"	=> [
						"type"	=> Type::string()
					]
				]
			]
		);
		
		
		require_once( BIO_REAL_PATH."class/SMC_Object_type.php" );
		$SMC_Object_type	= SMC_Object_Type::get_instance();		
		$arr = [];
		foreach( $SMC_Object_type->object as $post_type => $val )
		{
			$obj					= $SMC_Object_type->object[ $post_type ];
			
			extract(static::getFields( $obj, $post_type, $val ));
			
			
			/* class_name fields type */
			
			PEGraphql::add_object_type( 
				[
					"name"			=> $class_name,
					"description"	=> "modified post type",
					"fields"		=> $fields
				]
			);

			PEGraphql::add_query( 
				'get' . $class_name, 
				[
					'description' => __( 'Get single post', BIO ),
					'type' 		=> PEGraphql::object_type($class_name),
					'args'     	=> [ 
						"id" => [ "type" => Type::string() ],
						
					],
					'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
					{						
						if (in_array($class_name, ['Post', "Page"]))
						{
							$class_name = "Bio_" . $class_name;
						}
						if($val['t']['type']=="post")
						{
							$post = $class_name::get_single_matrix($args['id']);
							$post->id = $post->ID;
							return $post;
						}
						else if($val['t']['type']=="taxonomy")
						{
							$term = $class_name::get_instance($args['id']);
							return $term->get_single_matrix();
						}
					}
				] 
			);
			
			PEGraphql::add_query( 
				'get'. $class_name . "s", 
				[
					'description' 	=> __( 'Get post type list', BIO ),
					'type' 			=> Type::listOf(PEGraphql::object_type($class_name)),
					'args'     		=> [ 
						"paging" 	=> [ "type" => PEGraphql::input_type("Paging") ],
						
					],
					'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						//var_dump($args["paging"]);
						
						if($val['t']['type']=="post")
						{
							if (in_array($class_name, ['Post', "Page"]))
							{
								$class_name = "Bio_" . $class_name;
							}
							$taxonomies = [];
							if (isset($args["paging"]['taxonomies']))
							{								
								foreach($args["paging"]['taxonomies'] as $tax => $val )
								{
									//tax_name, term_ids
									if(!isset( $taxonomies[$val['tax_name']]) )
									{
										$taxonomies[$val['tax_name']] = [];										
									}
									$taxonomies[$val['tax_name']] =  array_merge($taxonomies[$val['tax_name']], $val['term_ids']);
								}
							}
							//wp_die($args["paging"]);
							$articles = $class_name::get_all_matrixes( $args["paging"] );
							//wp_die(count($articles));
							//wp_die($class_name);
							return $articles;
						}
						else if($val['t']['type']=="taxonomy")
						{
							return $class_name::get_all_matrixes($args["paging"]);
						}
					}
				] 
			);
			PEGraphql::add_query( 
				'get'. $class_name . "Count", 
				[
					'description' 	=> __( 'Get post type count', BIO ),
					'type' 			=> Type::int(),
					'args'     		=> [ 
						"paging" 	=> [ "type" => PEGraphql::input_type("Paging") ],
						
					],
					'resolve' 	=> function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						//var_dump($args["paging"]);
						unset ($args["paging"]["count"]);
						unset ($args["paging"]["offset"]);
						if($val['t']['type']=="post")
						{
							if (in_array($class_name, ['Post', "Page"]))
							{
								$class_name = "Bio_" . $class_name;
							}
							$taxonomies = [];
							if (isset($args["paging"]['taxonomies']))
							{								
								foreach($args["paging"]['taxonomies'] as $tax => $val )
								{
									//tax_name, term_ids
									if(!isset( $taxonomies[$val['tax_name']]) )
									{
										$taxonomies[$val['tax_name']] = [];										
									}
									$taxonomies[$val['tax_name']] =  array_merge($taxonomies[$val['tax_name']], $val['term_ids']);
								}
							} 
							$articles = $class_name::get_all_matrixes( $args["paging"] ); 
							return count($articles);
						}
						else if($val['t']['type']=="taxonomy")
						{
							unset ($args["paging"]["meta_relation"]);
							unset ($args["paging"]["tax_relation"]);
							unset ($args["paging"]["taxonomies"]);
							unset ($args["paging"]["metas"]);
							unset ($args["paging"]["post_status"]);
							unset ($args["paging"]["is_admin"]);
							//wp_die($args["paging"]);
							return count($class_name::get_all_matrixes($args["paging"]));
						}
					}
				] 
			);
			
			PEGraphql::add_mutation( 
				'delete'.$class_name, 
				[
					'description' 	=> __( "Delete single single", BIO ),
					'type' 			=> Type::boolean(),
					'args'         	=> [
						"id"	=> [
							'type' => Type::string(),
							'description' => __( 'Unique identificator', BIO ),
						]
					],
					'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						if( $val['t']['type'] == "post" )
						{
							if(in_array($class_name, ['Post', "Page"]))
							{
								if(!current_user_can("edit_pages"))
								{
									throw new \Bio_GraphQLNotAccess( "you not right!" );
								}
								
							}
							else
							{
								$const = constant( strtoupper( $post_type ) . "_DELETE" );
								Bio_User::access_caps_gq( $const, "you not right!" );
							}							
							$del = wp_delete_post( $args[ "id" ] );	
							if( $del === false || $del === null )
							{
								return false;
							}
							else
							{
								return true;
							}
						}
						else if( $val['t']['type'] == "taxonomy" )
						{
							$del	= $class_name::delete($args["id"]);
							if($del	=== true)
								return true;
							else
								return false;
						}
					}
				] 
			);
			
			extract(static::getFields( $obj, $post_type, $val, true ));
			PEGraphql::add_input_type( 
				[
					"name"		=> $class_name.'Input',
					'description' => __( "post type", BIO ),
					'fields' 		=> $fields,
				]
			);
			
			
			PEGraphql::add_mutation(
				"change$class_name", 
				[
					'description' 	=> __( "Change parameter of single post type", BIO ),
					'type' 			=> PEGraphql::object_type($class_name),
					'args'         	=> [
						"id"	=> [
							'type' 	=> Type::string(),
							'description' => __( 'Unique identificator', BIO ),
						],
						'input' => [
							'type' => PEGraphql::input_type($class_name.'Input'),
							'description' => __( "Post types's new params", BIO ),
						]
					],
					'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						if (!isset($args["input"]["post_content"]))
						{
							$args["input"]["post_content"] = "";
						}
						$return = apply_filters(
							"before_change_gq_post", 
							null,
							$root, $args, $context, $info, $class_name, $post_type, $val
						);
						if(isset($return))
							return $return;
						
						if($val['t']['type'] == "post")
						{							
							if(in_array($class_name, ['Post', "Page"]))
							{
								if(!current_user_can("edit_pages"))
								{
									throw new \Bio_GraphQLNotAccess( "you not right!" );
								}
								$post_args = $args['input'];
								if( $args['id'] && $args['id'] > 0 )
								{
									$post_args["ID"] = $args['id'];
									wp_update_post([
										"ID" => $args['id'],
										"post_title" => $args['input']['post_title'],
										"post_content" =>$args['input']['post_content'],
									]);
									return SMC_Post::get_single_matrix($args['id']);
								}
								else
								{
									$post_args["post_author"] = get_current_user_id();
									$post_args["post_type"] = strtolower($class_name);
									$post_args['post_status']   = $args['input']['post_status']
											? 
											$args['input']['post_status']
											: 
											"publish";
									//wp_die($args['input']['post_title']);
									$id = wp_insert_post($post_args);
									if(!is_wp_error($id))
									{
										return SMC_Post::get_single_matrix($id);
									}
									else
									{
										wp_die($id);
									}
								}
							}
							
							
							// TODO - проверить права на изменения
							if($post_type == BIO_ARTICLE_TYPE)
								$const = constant( "BIO_ARTICLE_EDIT" );
							else
								$const = constant( strtoupper( $post_type ) . "_EDIT" );
							
							Bio_User::access_caps_gq( $const, "you not right!" );
							
							if( $args['id'] && $args['id'] > 0 )
							{
								$class_name::update( $args['input'], $args['id'] );
								// TODO - вставить все поля (только $args['input')
								$class_name::clear_instance($args['id']);
								return $class_name::get_single_matrix($args['id']);
							}
							else
							{
								$art = $class_name::insert($args['input']);
								return apply_filters(
									"bio_gq_change_" . $post_type, 
									$class_name::get_single_matrix($art->id),
									$art,
									$args
								);
							}
						}
						else if($val['t']['type'] == "taxonomy")
						{
							$const = constant( strtoupper( $post_type ) . "_EDIT" );
							Bio_User::access_caps_gq( $const, "you not right!" );
							
							if( $args['id'] && $args['id'] > 0 )
							{					 
								$res = $class_name::update( $args['input'], $args['id'] );
								if(is_wp_error($res))
								{
									throw new Bio_GraphQLError($res->get_error_message());
								}
								else
								{
									$examle = $class_name::get_instance( $res );
									return apply_filters(
										"bio_gq_change_" . $post_type,
										$examle->get_single_matrix(),
										$examle
									);
								}
							}
							else
							{
								$res = $class_name::insert( $args['input'] );
								if(is_wp_error($res))
								{
									throw new Bio_GraphQLError($res->get_error_message());
								}
								else
								{
									$examle = $class_name::get_instance( $res["term_id"] );
									return $examle->get_single_matrix();
								}
							}
						}
					}
				] 
			);
			
			
			PEGraphql::add_mutation( 
				'addCourseUser', 
				[
					'description' 	=> __( "add Course to current User", BIO ),
					'type' 			=> Type::boolean(),
					'args'         	=> [
						"course_id"	=> [
							'type' => Type::string(),
							'description' => __( 'Unique identificator of Course', BIO ),
						]
					],
					'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
					{
						return true;
					}
				] 
			);
			
		}
		return $arr;
		
	}
	static function graphql_register_input_types()
	{
		/**/
		PEGraphql::add_input_type(
			[
				"name"		=> 'TaxFilter',
				'description' => __( "Filter by tax Field", BIO ),
				"fields"	=> [
					'tax_name'	=> [
							'type' 	=> Type::string(),
							'description' => __( 'name of filter term', BIO )
						],
					'term_ids'	=> [
							'type' 	=> Type::listOf(Type::int()),
							'description' => __( 'array of ID', BIO )
						],
				]
			]
		);		
		
		PEGraphql::add_input_type( 
			[
				"name"		=> 'Paging',
				'description' => __( "Pagination of all Types", BIO ),
				'fields' 		=> [
					'count' 	=> [
						'type' 	=> Type::int(),
						'description' => __( 'Count of collection', BIO ),
						"defaultValue" => 10000
					],
					'offset' 	=> [
						'type' 	=> Type::int(),
						'description' => __( 'offset by start', BIO ),
						"defaultValue" => 0
					],
					"meta_relation" => [
						'type' 	=> Type::string(),
						'description' => __( 'Compare of different meta filters. Values "OR" or "AND". Default - "OR".', BIO ),
						"defaultValue" => "OR"
					],
					"tax_relation" => [
						'type' 	=> Type::string(),
						'description' => __( 'Compare of different taxonomies filters. Values "OR" or "AND". Default - "OR".', BIO ),
						"defaultValue" => "OR"
					],
					"order" => [
						'type' 	=> Type::string(),
						'description' => __( 'Values "DESC" or "ASC". Default "DESC".', BIO ),
						"defaultValue" => 'date'
					],
					
					'parent'			=> [
						'type' 	=> Type::string(),
						'description' => __( 'Parent of elements', BIO )
					],
					'order_by_meta'	=> [
						'type' 	=> Type::string(),
						'description' => __( '', BIO )
					],
					"relation"		=> [
						"type" 	=> Type::string(),
						"description" => __( '', BIO ),
					],
					'taxonomies'	=> [
						'type' 	=> Type::listOf(PEGraphql::input_type("TaxFilter")),//,
						'description' => __( 'Current page. Default is 1', BIO ),
						"defaultValue" => []
					],
					'metas'	=> [
						'type' 	=> Type::listOf(PEGraphql::input_type("MetaFilter")),//,
						'description' => __( ' ', BIO ),
						"defaultValue" => []
					],
					'post_status'	=> [
						'type' 	=> Type::string(),
						'description' => __( 'Current page. Default is 1', BIO ),
						"defaultValue" => 'publish'
					],
					'post_author'	=> [
						'type' 	=> Type::string(),
						'description' => __( 'By author ID', BIO )
					],
					'is_admin'	=> [
						'type' 	=> Type::boolean(),
						'description' => __( 'For admin panel', BIO ),
						"defaultValue" => false
					],
					"search" => [
						'type' 	=> Type::string(),
						'description' => __( 'search in titles by substring".', BIO )
					],
				],
			]
		);
	
	}

	static function auth()
	{
		//register
		PEGraphql::add_mutation( 
			'registerUser', 
			[
				'description' 	=> __( "Register new User's account", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'input' => [
						'type' => PEGraphql::input_type('UserInput'),
						'description' => __( "User's new params", BIO ),
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					do_action("bio_graphql_register_user", $args['input']);
					if(!$args['input']['password'])
					{
						throw new Bio_GraphQLError( __("Password not be empty!", BIO) );
					}
					$answ = Bio_User::insert($args['input']);
					if(is_wp_error($answ))
					{
						throw new Bio_GraphQLError( $answ->get_error_message() );
					}
					else
					{
						$login		= $args['input']['user_email'];
						$passw		= $args['input']['psw'];
						do_action("graphql_after_register_user", $answ, $args['input']);
						$user = get_user_by('id', $answ);
						
						if( $user )
						{
							$user_id	= $user->ID;
							
							$code 		= md5(time());
							update_user_meta($user_id, 'activation_code', $code);
							update_user_meta($user_id, 'account_activated', 0);
							
							Bio_Mailing::send_mail(								
								sprintf( __("You are create account on %s", BIO),  get_bloginfo("title") ),
								sprintf( 
									__("To ensure full functionality, please confirm your email address. To do this, follow <a href='%s'>this link</a>", BIO),  
									Bio::$options['web_client_url'] . "/verify/$user_id/$code" 
								),
								1,
								[ $args['input']['user_email'] ]
							);
						}
						else
						{
							throw new Bio_GraphQLError(__( "User insert success. Logg in please.", BIO) );
						}
						return true;
					}
				}
			] 
		);
		
		//register
		PEGraphql::add_mutation( 
			'verifyUser', 
			[
				'description' 	=> __( "Verified User after create account", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'id' 	=> [
						'type' 	=> Type::string(),
						'description' => __( 'User unique identificator', BIO )
					],
					'code' => [
						'type' => Type::string(),
						'description' => __( "Unique cache that user have in registration e-mail.", BIO ),
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					$code				= $args['code'];
					$activation_code	= get_user_meta((int)$args['id'], 'activation_code', $true);
					if($activation_code[0] == $code)
					{
						update_user_meta((int)$args['id'], 'account_activated', 1);
						return true;
					}
					else
					{
						return false;
					}
					
				}
			]
		);
		
		//register
		PEGraphql::add_mutation( 
			'restorePass', 
			[
				'description' 	=> __( "Restore password by unlogged User with send e-mail.", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'email' 	=> [
						'type' 	=> Type::string(),
						'description' => __( 'User\'s e-mail box', BIO )
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					$user = get_user_by( "email",  $args['email'] );
					//wp_die( [ $args, $user ] );
					if( $user )
					{
						$user_id	= $user->ID;								
						$code 		= md5(time());
						update_user_meta($user_id, 'restored_code', $code);
						
						Bio_Mailing::send_mail(								
							sprintf( __("You are called resore your password in %s", BIO),  get_bloginfo("title") ),
							sprintf( 
								__("Restore may your password. To do this, follow <a href='%s'>this link</a>", BIO),  
								Bio::$options['web_client_url'] . "/restore/$user_id/$code" 
							),
							1,
							[ $args['email'] ]
						);
						/*
						return wp_mail(
						return wp_mail(
							"genglaz@gmail.com",
							"test theme",
							"test message"
						);*/
						//wp_die([$user_id, $code ]);
						return true;
					}
					return false;
				}
			]
		);
		//restored password
		PEGraphql::add_mutation( 
				'compareRestore', 
				[
					'description' 	=> __( "Final restored User password", BIO ),
					'type' 			=> Type::boolean(),
					'args'         	=> [
						'id' 	=> [
							'type' 	=> Type::string(),
							'description' => __( 'User unique identificator', BIO )
						],
						'code' => [
							'type' => Type::string(),
							'description' => __( "Unique cache that user have in e-mail.", BIO ),
						]
					],
					'resolve' => function( $root, $args, $context, $info )
					{
						$code				= $args['code'];
						$restored_code	= get_user_meta((int)$args['id'], 'restored_code', $true);
						if($restored_code[0] == $code)
						{
							//update_user_meta((int)$args['id'], 'restored_code', 1);
							return true;
						}
						else
						{
							return false;
						}
					}
				]
		);
		//restored password
		PEGraphql::add_mutation( 
			'saveNewPassword', 
			[
				'description' 	=> __( "Change User's password", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'id' 	=> [
						'type' 	=> Type::string(),
						'description' => __( 'User unique identificator', BIO )
					],
					'password' => [
						'type' => Type::string(),
						'description' => __( "New password", BIO ),
					],
					'code' => [
						'type' => Type::string(),
						'description' => __( "Unique cache that user have in e-mail.", BIO ),
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					$code				= $args['code'];
						$restored_code	= get_user_meta((int)$args['id'], 'restored_code', $true);
						if(isset($restored_code) && $restored_code[0] == $code )
						{
							delete_user_meta((int)$args['id'], 'restored_code' );
							$userdata = [
								'ID'              => (int)$args['id'],
								'user_pass'       => $args['password']
							];
							$user = wp_update_user(  $userdata );
							if(is_wp_error($user))
							{
								throw new Bio_GraphQLError( __("Unknown error. Repeat please!", BIO) );
							}
							else
								return true;
						}
						else
						{
							return false;
						}
					
				}
			]
		);
	}
	
}

function get_term_top_most_parent( $term, $taxonomy ) 
{
    // Start from the current term
    $parent  = get_term( $term, $taxonomy );
    // Climb up the hierarchy until we reach a term with parent = '0'
    while ( $parent->parent != '0' ) 
	{
        $term_id = $parent->parent;
        $parent  = get_term( $term_id, $taxonomy);
    }
    return $parent;
}

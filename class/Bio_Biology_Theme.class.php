<?php
class Bio_Biology_Theme extends SMC_Taxonomy
{
	
	static function get_type()
	{
		return BIO_BIOLOGY_THEME_TYPE;
	}
	static function init()
	{
		add_action( 'init', 				array( __CLASS__, 'create_taxonomy'), 19);
		add_action( 'parent_file',			array( __CLASS__, 'tax_menu_correction'), 1);	
		add_action( 'admin_menu', 			array( __CLASS__, 'tax_add_admin_menus'), 21);
		add_filter("manage_edit-".BIO_BIOLOGY_THEME_TYPE."_columns", 	array( __CLASS__,'ctg_columns')); 
		add_filter("manage_".BIO_BIOLOGY_THEME_TYPE."_custom_column",	array( __CLASS__,'manage_ctg_columns'), 11.234, 3);
		add_action( BIO_BIOLOGY_THEME_TYPE.'_edit_form_fields', 		array( __CLASS__, 'add_ctg'), 2, 2 );
		add_action( 'edit_'.BIO_BIOLOGY_THEME_TYPE, 					array( __CLASS__, 'save_ctg'), 10);  
		add_action( 'create_'.BIO_BIOLOGY_THEME_TYPE, 					array( __CLASS__, 'save_ctg'), 10);	
	}
	static function create_taxonomy()
	{
		register_taxonomy(
			static::get_type(), 
			array( "post", BIO_ARTICLE_TYPE, BIO_EVENT_TYPE, BIO_TEST_TYPE ), 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Biology Theme", BIO),
					'singular_name'     => __("Biology Theme", BIO),
					'search_items'      => __('search Biology Theme', BIO),
					'all_items'         => __('all Biology Themes', BIO),
					'view_item '        => __('view Biology Theme', BIO),
					'parent_item'       => __('parent Biology Theme', BIO),
					'parent_item_colon' => __('parent Biology Theme:', BIO),
					'edit_item'         => __('edit Biology Theme', BIO),
					'update_item'       => __('update Biology Theme', BIO),
					'add_new_item'      => __('add Biology Theme', BIO),
					'new_item_name'     => __('new Biology Theme Name', BIO),
					'menu_name'         => __('Biology Theme', BIO),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_in_nav_menus' 	=> true,
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => true,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
		//wp_nav_menu_taxonomy_meta_boxes() ;
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == static::get_type() )
			$parent_file = 'pe_edu_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'pe_edu_page', 
			__("Biology Themes", BIO), 
			__("Biology Themes", BIO), 
			'manage_options', 
			'edit-tags.php?taxonomy=' . static::get_type()
		);
		add_meta_box( "add-".BIO_BIOLOGY_THEME_TYPE."", __("Biology Themes", BIO), 'wp_nav_menu_item_taxonomy_meta_box', 'nav-menus', 'side', 'default',  static::get_type() );	
    }
	static function ctg_columns($theme_columns) 
	{
		$new_columns = array
		(
			'cb' 			=> ' ',
			//'id' 			=> 'id',
			'name' 			=> __('Name'),
			'icon' 			=> __('Icon', BIO),
			'order' 		=> __('Order', BIO),
		);
		return $new_columns;
	}
	static function manage_ctg_columns($out, $column_name, $term_id) 
	{
		switch ($column_name) {
			case 'id':
				$out 		.= $term_id;
				break;
			case 'color': 
				$color = get_term_meta( $term_id, 'color', true ); 
				$out 		.= "<div class='clr' style='background-color:$color;'></div>";
				break;	 
			case 'order': 
				$order = get_term_meta( $term_id, 'order', true ); 
				$out 		.= $order;
				break;	 
			case "icon":
				$icon = get_term_meta( $term_id, 'icon', true ); 
				$logo = wp_get_attachment_image_src($icon, "full")[0];
				echo "<img src='$logo' style='width:auto; height:60px; margin:10px;' />";
				break;	
			default:
				break;
		}
		return $out;    
	}
	
	static function add_ctg( $term, $tax_name )
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		if($term)
		{
			$term_id = $term->term_id;
			$color = get_term_meta($term_id, "color", true);
			$icon  = get_term_meta($term_id, "icon", true);
			$icon  = is_wp_error($icon) ? "" :  $icon;
			$order  = get_term_meta($term_id, "order", true);
		}
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="icon">
					<?php echo __("Icon", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php
					echo get_input_file_form2( "group_icon", $icon, "group_icon", 0 );
				?>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="order">
					<?php echo __("Order", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="number" value="<?php echo $order; ?>" name="order" id="order"/>
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		update_term_meta($term_id, "color", $_POST['color']);
		update_term_meta($term_id, "order", $_POST['order']);
		update_term_meta($term_id, "icon",  $_POST['group_icon0']);
	}

    static function delete( $post_id )
    {
        $post_id = (int)$post_id;

        wp_delete_term( $post_id, static::get_type() );
//        update_term_meta($post_id, "icon", $data["icon"]);
        return $post_id;
    }
	
	static function update( $data, $post_id )
	{
		$post_id = (int)$post_id;
        $data['name'] = $data['post_title'];

		$instance = static::get_instance($post_id);
		$instance->update_thumbnail($data);
		
        wp_update_term( $post_id, static::get_type(), array(
            'name' 			=> $data["name"],
            'description' 	=> $data["description"],
        ));

        $data["order"] > -1 ? update_term_meta($post_id, "order", $data["order"]) : delete_term_meta($post_id, "order", true);
		return $post_id;
	}
	static function insert( $data )
	{
        $data['name'] = $data['post_title'];

        if( $data['icon_id'] < 1 )
        {
            $media = Bio_Assistants::insert_media([ "data" => $data['icon'], "media_name"=> $data['media_name']]);wp_set_object_terms( $media['id'], (int)Bio::$options['icon_media_term'], BIO_MEDIA_TAXONOMY_TYPE );
            $data['icon_id']	= $media['id'];
            $data['icon']		= $media['url'];
        }

		$post_id = wp_insert_term( $data["name"], static::get_type(), array(
			'description' => $data["description"]
		) );

        update_term_meta($post_id['term_id'], "icon", $data["icon_id"]);
        update_term_meta($post_id['term_id'], "order", $data["order"]);

		return $post_id;
	}
	
	

    static function get_bio_themes($p)
    {
        if(is_numeric($p))
        {
            $course = get_term($p, BIO_BIOLOGY_THEME_TYPE);
        }
        else
        {
            $course = $p;
        }
        $c = [];
        if(is_wp_error($course) || !$course)
            return $c;
        $c['id']			= $course->term_id;
        $c['ID']			= $course->term_id;
        $c['post_title']	= $course->name;
        $c['post_content']	= $course->description;
        $c['articles_count']= Bio_Course::get_article_count($course->term_id);
        //$c['tests_count']	= Bio_Course::get_test_count($course->term_id);
        $c['count']			= Bio_Course::get_article_count($course->term_id);
        $c['order']			= get_term_meta( $course->term_id, "order", true);
        $c['icon_id']		= get_term_meta( $course->term_id, "icon", true);
        $c['icon']			= wp_get_attachment_image_src($c['icon_id'], "full")[0];
        $c['thumbnail_id']	= $c['icon_id'];
        $c['thumbnail']		= $c['icon'];
        return $c;
    }
	
	

    static function get_classes($p)
    {
        return static::get_bio_themes($p);
    }

    static function get_class($class)
    {
        if(is_numeric($class))
        {
            $class = get_term($class, BIO_BIOLOGY_THEME_TYPE);
        }
        $c = [];
        $c['ID']			= $class->term_id;
        $c['id']			= $class->term_id;
        $c['post_title']	= $class->name;
        $c['icon_id']		= get_term_meta( $class->term_id, "icon", true);
        $c['icon']			= wp_get_attachment_image_src($c['icon_id'], "full")[0];
        $c['thumbnail_id']	= $c['icon_id'];
        $c['thumbnail']		= $c['icon'];
        $c['articles_count']= Bio_Course::get_article_count($class->term_id);
        //$c['tests_count']	= Bio_Course::get_test_count($class->term_id);
        $c['count']			= Bio_Course::get_article_count($class->term_id);
        $c['order']			= get_term_meta( $class->term_id, "order", true);
        return $c;

    }
	
	
    public static function api_action($type, $methods, $code, $pars, $user)
    {
        $courses	= [];
        switch($methods) {
            case "update":
                if(is_numeric($code)) {
//                            $p = static::get_article($code, true);
//                            $pst = Bio_Article::get_instance($code);
//                            if (!$pst->is_user_author(true)) throw new ExceptionNotLoggedREST();
                    Bio_Biology_Theme::update($pars, $code);
                    $courses[]	= static::get_bio_themes( $code );
                    $update = 'success';
                }else{
                    $update = 'error';
                }
                break;
            case "delete":
                if(is_numeric($code)) {
                    Bio_Biology_Theme::delete($code);
                    $update = 'success';
                }else{
                    $update = 'error';
                }
                break;
            case "create":
                if(is_numeric($code)) {
                    Bio_Biology_Theme::update($pars, $code);
                    $cat = static::get_bio_themes( $code );
					$msg = sprintf( __("Biology Theme «%s» updated succesfully", BIO), $cat['post_title'] ); 
					$courses[]	= $cat;					
                }else{
                    $class = Bio_Biology_Theme::insert($pars);
                    $courses[]			= static::get_bio_themes($class);
                    $msg = __("Biology Theme inserted succesfully", BIO);
                }
                break;
            case "read":
            default:
                //$code = (int)$code;
                if(is_numeric($code)){
                    $c							= static::get_bio_themes( $code );
                    $courses[] 		= $c;
                    $articles = [];
                    $all 	= Bio_Article::get_all(
                        isset($pars['metas']) 			? $pars['metas'] 		: [], 		// []
                        isset($pars['numberposts'])		? $pars['numberposts'] 	: -1,  		// -1
                        isset($pars['offset'])			? $pars['offset']		: 0,  		// 0
                        isset($pars['order_by'])		? $pars['order_by']		: "title", 	// 'title'
                        isset($pars['order'])			? $pars['order']		: 'DESC', 	// 'DESC'
                        isset($pars['order_by_meta'])	? $pars['order_by_meta']: "", 		// ""
                        "all", 																// $pars['fields'],
                        isset($pars['relation'])		? $pars['relation']		: "AND",	// "AND",
                        isset($pars['author'])			? $pars['author']		: -1,		// "",
                        [ "bio_biology_theme" => $code ]

                    );
                    foreach($all as $p)
                    {
                        $a 					= Bio_Article::get_article( $p, false );
                        $articles[]			= $a;
                    }
                    break;

                }else{
                    $terms = get_terms( array(
                        'taxonomy'      => static::get_type(),
                        'orderby'       => 'meta_value_num',
                        'order'         => 'ASC',
						'hide_empty'   	=> false,
                        'fields'        => 'all',
						"meta_key"	   	=> "order",
                    ) );
                    foreach($terms as $c)
                    {
                        $courses[]	= static::get_bio_themes( $c );
                    }
                }
                break;
        }
        return Array (
            "bio_biology_theme" => $courses,
            "articles" => $articles,
            "id" => $code,
            "msg" => $msg,
            "update"=> $update
        );
    }

	static function wp_dropdown($params=-1)
	{
		if(!is_array($params))
			$params = [];
		if($params['terms'])
		{
			$terms		=  $params['terms'];
		}
		else
		{
			$terms = get_terms( array(
				'taxonomy'      => static::get_type(), 
				'orderby'       => 'name', 
				'order'         => 'ASC',
				'hide_empty'    => false, 
				'fields'        => 'all', 
			) );
		}
		$html		= "<select ";
		if($params['class'])
			$html	.= "class='".$params['class']."' ";
		if($params['style'])
			$html	.= "style='".$params['style']."' ";
		if($params['name'])
			$html	.= "name='".$params['name']."' ";
		if($params['id'])
			$html	.= "id='".$params['id']."' ";
		if($params['special'])
		{
			$pars  	= explode(",", $params['special']);
			$html	.= "$pars[0]='$pars[1]' ";
		}
		$html		.= " >";
		$zero 		= $params['select_none'] ? $params['select_none'] : "---";
			if(!$params['none_zero'])
				$html	.= "<option value='-1' selected>$zero</option>";			
			
		if(count($terms))
		{
			foreach($terms as $term)
			{
				$html	.= "
				<option " . selected($term->term_id, $params['selected'], 0) . " value='".$term->term_id."'>".
					$term->name.
				"</option>";
			}
		}
		$html		.= apply_filters("bio_biology_theme_last_dropdown", "", $params, $terms) . "
		</select>";
		return $html;
	}
    static function get_category($p)
    {
        if(is_numeric($p))
        {
            $course = get_term($p, static::get_type());
        }
        else
        {
            $course = $p;
        }
        $c = [];
        if(is_wp_error($course) || !$course)
            return $c;
        $c['id']			= $course->term_id;
        $c['post_title']	= $course->name;
        $c['icon_id']		= get_term_meta( $course->term_id, "icon", true);
        $c['icon_id']		= get_term_meta( $course->term_id, "icon", true);
        $c['icon']			= wp_get_attachment_image_src($c['icon_id'], "full")[0];
        return $c;
    }
}
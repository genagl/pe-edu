<?php
class Bio_Media_Taxonomy extends SMC_Taxonomy
{
	static function get_type()
	{
		return BIO_MEDIA_TAXONOMY_TYPE;
	}
	static function init()
	{
		add_action( 'init', 				[ __CLASS__, 'create_taxonomy'], 19);
		add_filter( 'manage_media_columns', [__CLASS__, 'manage_media_columns'], 10, 2 );
		add_action( 'manage_media_custom_column', [__CLASS__, 'fill_column_in_media_table'], 10, 2 );
		//add_action( 'parent_file',			array( __CLASS__, 'tax_menu_correction'), 1);	
		//add_action( 'admin_menu', 			array( __CLASS__, 'tax_add_admin_menus'), 19);		
	}
	
	static function manage_media_columns( $posts_columns, $detached )
	{
		// Изменяем...
		unset( $posts_columns["date"] );
		return [ 'id-image' => 'ID' ] + $posts_columns;
		return $posts_columns;
	}
	static function fill_column_in_media_table( $colname, $post_id ) 
	{
		if ( $colname === 'id-image' ) {
			echo (int) $post_id;
		}
	}
	static function create_taxonomy()
	{
		register_taxonomy(
			static::get_type(), 
			array( "attachment" ), 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Media Tax", BIO),
					'singular_name'     => __("Media Tax", BIO),
					'search_items'      => __('search Media Tax', BIO),
					'all_items'         => __('all Media Taxs', BIO),
					'view_item '        => __('view Media Tax', BIO),
					'parent_item'       => __('parent Media Tax', BIO),
					'parent_item_colon' => __('parent Media Tax:', BIO),
					'edit_item'         => __('edit Media Tax', BIO),
					'update_item'       => __('update Media Tax', BIO),
					'add_new_item'      => __('add Media Tax', BIO),
					'new_item_name'     => __('new Media Tax Name', BIO),
					'menu_name'         => __('Media Tax', BIO),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_in_nav_menus' 	=> true,
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => true,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
		//wp_nav_menu_taxonomy_meta_boxes() ;
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == static::get_type() )
			$parent_file = 'pe_edu_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'pe_edu_page', 
			__("Media Taxs", BIO), 
			__("Media Taxs", BIO), 
			'manage_options', 
			'edit-tags.php?taxonomy=' . static::get_type()
		);
    }
	

    static function update( $data, $post_id )
    {
        $post_id = (int)$post_id;
        $data["name"] = $data["post_title"];// ? $data["post_title"] : $data["name"];
        wp_update_term( $post_id, static::get_type(), array(
            'name' 			=> $data["name"],
            'description' 	=> $data["description"],
        ));
        update_term_meta($post_id, "icon", $data["icon"]);
        update_term_meta($post_id, "order", (int)$data["name"]);
        return $post_id;
    }
    static function insert( $data )
    {
        $data['name'] = $data['post_title'];
        $post_id = wp_insert_term(
            $data["name"], static::get_type(),
            array(
				'description' => $data["description"]
        ) );
        update_term_meta($post_id['term_id'], "icon", $data["icon"]);
        update_term_meta($post_id['term_id'], "order", (int)$data["name"]);
        return $post_id;
    }



    static function get_classes($p)
    {
        if(is_numeric($p))
        {
            $course = get_term($p, BIO_MEDIA_TAXONOMY_TYPE);
        }
        else
        {
            $course = $p;
        }
        $c = [];
        if(is_wp_error($course) || !$course)
            return $c;

        $c['id']			= $course->term_id;
        $c['post_title']	= $course->name;

        return $c;
    }

    static function get_class($class)
    {
        if(is_numeric($class))
        {
            $class = get_term($class, BIO_MEDIA_TAXONOMY_TYPE);

        }
        $c = [];
        $c['id']			= $class->term_id;
        $c['post_title']	= $class->name;
        return $c;

    }

}
	
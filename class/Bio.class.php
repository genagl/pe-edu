<?php
/*
	rest api reciver
*/
function bio_rest_api_create( WP_REST_Request $request )
{
    require_once BIO_REAL_PATH . "class/Bio_REST.class.php";
    $methods = "create";
    return Bio_REST::api_routing_right( $request, $methods );
}

function bio_rest_api_read( WP_REST_Request $request )
{
    require_once BIO_REAL_PATH . "class/Bio_REST.class.php";
    $methods = "read";
    return Bio_REST::api_routing_right( $request, $methods );
}

function bio_rest_api_update( WP_REST_Request $request )
{
    require_once BIO_REAL_PATH . "class/Bio_REST.class.php";
    $methods = "update";
    return Bio_REST::api_routing_right( $request, $methods );
}

function bio_rest_api_delete( WP_REST_Request $request )
{
    require_once BIO_REAL_PATH . "class/Bio_REST.class.php";
    $methods = "delete";
    return Bio_REST::api_routing_right( $request, $methods );
}

//TODO School REST API

function school_rest_api_create( WP_REST_Request $request )
{
    require_once BIO_REAL_PATH . "class/School_REST.class.php";
    $methods = "create";
    return School_REST::api_routing_right( $request, $methods );
}

function school_rest_api_read( WP_REST_Request $request )
{
    require_once BIO_REAL_PATH . "class/School_REST.class.php";
    $methods = "read";
    return School_REST::api_routing_right( $request, $methods );
}

function school_rest_api_update( WP_REST_Request $request )
{
    require_once BIO_REAL_PATH . "class/School_REST.class.php";
    $methods = "update";
    return School_REST::api_routing_right( $request, $methods );
}

function school_rest_api_delete( WP_REST_Request $request )
{
    require_once BIO_REAL_PATH . "class/School_REST.class.php";
    $methods = "delete";
    return School_REST::api_routing_right( $request, $methods );
}


add_filter( 'cron_schedules', 'cron_add_two_min' );
function cron_add_two_min( $schedules ) {
	$schedules['two_min'] = [
		'interval' => 60 * 2,
		'display' => 'Раз в 2 минут'
	];
	return $schedules;
}

/*
	main plugin class
*/
class Bio
{	
	static function rest_send_nocache_headers()
	{
		header("Access-Control-Allow-Origin: *");
		header('Content-Type: application/json');
		header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS, CONNECT, TRACE, PATCH, HEAD");
		header("Allow: GET, POST, PUT, DELETE, OPTIONS, CONNECT, TRACE, PATCH, HEAD");
		header("Access-Control-Allow-Credentials: true");
		header("Access-Control-Max-Age: 86400");
		header("Access-Control-Allow-Headers: application/json");//
		header("Access-Control-Allow-Headers: Accept, Accept-CH, Accept-Charset, Accept-Datetime, Accept-Encoding, Accept-Ext, Accept-Features, Accept-Language, Accept-Params, Accept-Ranges, Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Access-Control-Allow-Methods, Access-Control-Allow-Origin, Access-Control-Expose-Headers, Access-Control-Max-Age, Access-Control-Request-Headers, Access-Control-Request-Method, Age, Allow, Alternates, Authentication-Info, Authorization, C-Ext, C-Man, C-Opt, C-PEP, C-PEP-Info, CONNECT, Cache-Control, Compliance, Connection, Content-Base, Content-Disposition, Content-Encoding, Content-ID, Content-Language, Content-Length, Content-Location, Content-MD5, Content-Range, Content-Script-Type, Content-Security-Policy, Content-Style-Type, Content-Transfer-Encoding, Content-Type, Content-Version, Cookie, Cost, DAV, DELETE, DNT, DPR, Date, Default-Style, Delta-Base, Depth, Derived-From, Destination, Differential-ID, Digest, ETag, Expect, Expires, Ext, From, GET, GetProfile, HEAD, HTTP-date, Host, IM, If, If-Match, If-Modified-Since, If-None-Match, If-Range, If-Unmodified-Since, Keep-Alive, Label, Last-Event-ID, Last-Modified, Link, Location, Lock-Token, MIME-Version, Man, Max-Forwards, Media-Range, Message-ID, Meter, Negotiate, Non-Compliance, OPTION, OPTIONS, OWS, Opt, Optional, Ordering-Type, Origin, Overwrite, P3P, PEP, PICS-Label, POST, PUT, Pep-Info, Permanent, Position, Pragma, ProfileObject, Protocol, Protocol-Query, Protocol-Request, Proxy-Authenticate, Proxy-Authentication-Info, Proxy-Authorization, Proxy-Features, Proxy-Instruction, Public, RWS, Range, Referer, Refresh, Resolution-Hint, Resolver-Location, Retry-After, Safe, Sec-Websocket-Extensions, Sec-Websocket-Key, Sec-Websocket-Origin, Sec-Websocket-Protocol, Sec-Websocket-Version, Security-Scheme, Server, Set-Cookie, Set-Cookie2, SetProfile, SoapAction, Status, Status-URI, Strict-Transport-Security, SubOK, Subst, Surrogate-Capability, Surrogate-Control, TCN, TE, TRACE, Timeout, Title, Trailer, Transfer-Encoding, UA-Color, UA-Media, UA-Pixels, UA-Resolution, UA-Windowpixels, URI, Upgrade, User-Agent, Variant-Vary, Vary, Version, Via, Viewport-Width, WWW-Authenticate, Want-Digest, Warning, Width, X-Content-Duration, X-Content-Security-Policy, X-Content-Type-Options, X-CustomHeader, X-DNSPrefetch-Control, X-Forwarded-For, X-Forwarded-Port, X-Forwarded-Proto, X-Frame-Options, X-Modified, X-OTHER, X-PING, X-PINGOTHER, X-Powered-By, X-Requested-With, access_token, token, authorization,  application/json");
		
		return true;
	}


    //https://v2.wp-api.org/extending/adding/

	static function rest_api_init()
	{
		register_rest_route( 'pe_edu', '/(?P<type>\w+)', array(
			'methods'  => "GET",
			'callback' => 'bio_rest_api_read',
		) );
		register_rest_route( 'pe_edu', '/(?P<type>\w+)/(?P<code>\w+)', array(
			'methods'  => "GET",
			'callback' => 'bio_rest_api_read',
		) );
		register_rest_route( 'pe_edu', '/(?P<type>\w+)/(?P<code>\w+)/(?P<pars>\w+)', array(
			'methods'  => "GET",
			'callback' => 'bio_rest_api_read',
		) );

		register_rest_route( 'pe_edu', '/(?P<type>\w+)', array(
			'methods'  => "POST",
			'callback' => 'bio_rest_api_create',
		) );		
		register_rest_route( 'pe_edu', '/(?P<type>\w+)/(?P<code>\w+)', array(
			'methods'  => "POST",
			'callback' => 'bio_rest_api_create',
		) );
		register_rest_route( 'pe_edu', '/(?P<type>\w+)/(?P<code>\w+)/(?P<pars>\w+)', array(
			'methods'  => "POST",
			'callback' => 'bio_rest_api_create',
		) );

        register_rest_route( 'pe_edu', '/(?P<type>\w+)', array(
            'methods'  => "PUT",
            'callback' => 'bio_rest_api_update',
        ) );
        register_rest_route( 'pe_edu', '/(?P<type>\w+)/(?P<code>\w+)', array(
            'methods'  => "PUT",
            'callback' => 'bio_rest_api_update',
        ) );
        register_rest_route( 'pe_edu', '/(?P<type>\w+)/(?P<code>\w+)/(?P<pars>\w+)', array(
            'methods'  => "PUT",
            'callback' => 'bio_rest_api_update',
        ) );

        register_rest_route( 'pe_edu', '/(?P<type>\w+)', array(
            'methods'  => "DELETE",
            'callback' => 'bio_rest_api_delete',
        ) );
        register_rest_route( 'pe_edu', '/(?P<type>\w+)/(?P<code>\w+)', array(
            'methods'  => "DELETE",
            'callback' => 'bio_rest_api_delete',
        ) );
        register_rest_route( 'pe_edu', '/(?P<type>\w+)/(?P<code>\w+)/(?P<pars>\w+)', array(
            'methods'  => "DELETE",
            'callback' => 'bio_rest_api_delete',
        ) );

        //TODO school API

        register_rest_route( 'school', '/(?P<type>\w+)', array(
            'methods'  => "GET",
            'callback' => 'school_rest_api_read',
        ) );
        register_rest_route( 'school', '/(?P<type>\w+)/(?P<code>\w+)', array(
            'methods'  => "GET",
            'callback' => 'school_rest_api_read',
        ) );
        register_rest_route( 'school', '/(?P<type>\w+)/(?P<code>\w+)/(?P<pars>\w+)', array(
            'methods'  => "GET",
            'callback' => 'school_rest_api_read',
        ) );

        register_rest_route( 'school', '/(?P<type>\w+)', array(
            'methods'  => "POST",
            'callback' => 'school_rest_api_create',
        ) );
        register_rest_route( 'pe_edu', '/(?P<type>\w+)/(?P<code>\w+)', array(
            'methods'  => "POST",
            'callback' => 'school_rest_api_create',
        ) );
        register_rest_route( 'school', '/(?P<type>\w+)/(?P<code>\w+)/(?P<pars>\w+)', array(
            'methods'  => "POST",
            'callback' => 'school_rest_api_create',
        ) );

        register_rest_route( 'school', '/(?P<type>\w+)', array(
            'methods'  => "PUT",
            'callback' => 'school_rest_api_update',
        ) );
        register_rest_route( 'school', '/(?P<type>\w+)/(?P<code>\w+)', array(
            'methods'  => "PUT",
            'callback' => 'school_rest_api_update',
        ) );
        register_rest_route( 'school', '/(?P<type>\w+)/(?P<code>\w+)/(?P<pars>\w+)', array(
            'methods'  => "PUT",
            'callback' => 'school_rest_api_update',
        ) );

        register_rest_route( 'school', '/(?P<type>\w+)', array(
            'methods'  => "DELETE",
            'callback' => 'school_rest_api_delete',
        ) );
        register_rest_route( 'school', '/(?P<type>\w+)/(?P<code>\w+)', array(
            'methods'  => "DELETE",
            'callback' => 'school_rest_api_delete',
        ) );
        register_rest_route( 'school', '/(?P<type>\w+)/(?P<code>\w+)/(?P<pars>\w+)', array(
            'methods'  => "DELETE",
            'callback' => 'school_rest_api_delete',
        ) );

	}
	static $options;
	static $instance;
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	static function activate()
	{		
		$options = get_option(BIO);
		global $wpdb;			
		$query = "ALTER TABLE `".$wpdb->prefix."comments` ADD `discussion_type` VARCHAR( 10 ) NOT NULL DEFAULT 'post' AFTER `comment_post_ID` ;";
		$wpdb->query($query);
		
		$query = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."course_user` (
	`ID` int(255) NOT NULL AUTO_INCREMENT,
	`course_id` int(255) NOT NULL,
	`user_id` int(255) NOT NULL,
	`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`ID`),
	UNIQUE KEY `course_user` (`course_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;";
		$wpdb->query($query);		
		
		$query = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."article_cat_user_requests` (
	`ID` int(255) unsigned NOT NULL AUTO_INCREMENT,
	`article_id` int(255) unsigned NOT NULL,
	`cat_id` int(255) unsigned NOT NULL,
	`user_id` int(255) unsigned NOT NULL,
	`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`accessed` tinyint(1) unsigned NOT NULL DEFAULT '0',
	PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;";
		$wpdb->query($query);
		
		$query = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."category_user_requests` (
	`ID` int(255) unsigned NOT NULL AUTO_INCREMENT,
	`category_id` int(255) unsigned NOT NULL,
	`user_id` int(255) unsigned NOT NULL,
	`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`accessed` tinyint(1) unsigned NOT NULL DEFAULT '0',
	PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;";
		$wpdb->query($query);
		
		$query = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."user_bio_test_question_result` (
  `ID` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(255) UNSIGNED NOT NULL,
  `post_id` int(255) UNSIGNED NOT NULL,
  `question_id` int(255) UNSIGNED NOT NULL,
  `answer_id` int(255) UNSIGNED NOT NULL,
  `is_right` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;";
		$wpdb->query($query);
		//
		
		$query = "CREATE TABLE `".$wpdb->prefix."error_test` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251  AUTO_INCREMENT=1;";
		$wpdb->query($query);
		//
		
		$query = "CREATE TABLE  IF NOT EXISTS `".$wpdb->prefix."facultet_user` (
	`ID` int(255) NOT NULL AUTO_INCREMENT,
	`facultet_id` int(255) NOT NULL,
	`user_id` int(255) NOT NULL,
	`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1;";
		$wpdb->query($query);
		//
		
		add_role( 
			"HiddenUser", __("Hidden User", BIO ) , 
			array(
				'edit_published_posts'		=> 0,
				'upload_files'				=> 0,
				'publish_posts'				=> 1,
				'delete_published_posts'	=> 0,
				'edit_posts'				=> 0,
				'delete_posts'				=> 0,
				'read'						=> 1,
			)
		);		
		add_role( 
			"CourseLeader", __("Course Leader", BIO ) , 
			array(
				'edit_published_posts'		=> 1,
				'upload_files'				=> 1,
				'publish_posts'				=> 1,
				'delete_published_posts'	=> 1,
				'edit_posts'				=> 1,
				'delete_posts'				=> 1,
				'read'						=> 1,
			)
		);
		add_role( 
			"TestEditor", __("Test Editor", BIO ) , 
			array(
				'edit_published_posts'		=> 1,
				'upload_files'				=> 1,
				'publish_posts'				=> 1,
				'delete_published_posts'	=> 1,
				'edit_posts'				=> 1,
				'delete_posts'				=> 1,
				'read'						=> 1,
			)
		);
		add_role( 
			"Teacher", __("Teacher", BIO ) , 
			array(
				'edit_published_posts'		=> 1,
				'upload_files'				=> 1,
				'publish_posts'				=> 1,
				'delete_published_posts'	=> 0,
				'edit_posts'				=> 0,
				'delete_posts'				=> 0,
				'read'						=> 1
			)
		);
		add_role( 
			"Pupil", __("Pupil", BIO ) , 
			array(
				'edit_published_posts'		=> 1,
				'upload_files'				=> 1,
				'publish_posts'				=> 1,
				'delete_published_posts'	=> 0,
				'edit_posts'				=> 0,
				'delete_posts'				=> 0,
				'read'						=> 1,
			)
		);
		
		
		if((int)$options['icon_media_term'] < 1)
		{
			$icon_term = wp_insert_term( "icons", BIO_MEDIA_TAXONOMY_TYPE, [
				'description' => '',
				'parent'      => 0,
				'slug'        => '',
			] );
			$options['icon_media_term'] = 
				$icon_term->term_id;
		}
		if((int)$options['test_media_term'] < 1)
		{
			$test_term = wp_insert_term( "tests", BIO_MEDIA_TAXONOMY_TYPE, [
				'description' => '',
				'parent'      => 0,
				'slug'        => '',
			] );
			$options['test_media_term'] = 
				$test_term->term_id;
		}
		if((int)$options['test_media_free'] < 1)
		{
			$free_term = wp_insert_term( "User upload", BIO_MEDIA_TAXONOMY_TYPE, [
				'description' => '',
				'parent'      => 0,
				'slug'        => '',
			] );
			$options['test_media_free'] = 
				$free_term->term_id;
		}
		
		$options['caps'] = 
		[
			"administrator"		=> bio_cap_mask([ 					
				
			], "administrator"),
			"editor" => bio_cap_mask([ 	
								
				BIO_ARTICLE_CREATE,
				BIO_ARTICLE_EDIT,
				BIO_ARTICLE_DELETE,
				BIO_ARTICLE_OTHERS_EDIT,
				BIO_ARTICLE_OTHERS_DELETE,
				BIO_ARTICLE_ADD_COURSE,
				BIO_ARTICLE_ADD_CLASS,
				BIO_ARTICLE_ADD_BIOLOGY_THEME,
				BIO_ARTICLE_ADD_OLIMPIAD_TYPE,
				BIO_ARTICLE_ADD_ROLE_TAXONOMY_TYPE,
				BIO_ARTICLE_OTHERS_ADD_COURSE,
				BIO_ARTICLE_OTHERS_ADD_CLASS,
				BIO_ARTICLE_OTHERS_ADD_BIOLOGY_THEME,
				BIO_ARTICLE_OTHERS_ADD_OLIMPIAD_TYPE,
				BIO_ARTICLE_OTHERS_ADD_ROLE_TAXONOMY_TYPE,
				BIO_EVENT_CREATE,
				BIO_EVENT_EDIT,
				BIO_EVENT_DELETE,
				BIO_EVENT_OTHERS_EDIT,
				BIO_EVENT_OTHERS_DELETE,
				BIO_TEST_CREATE,
				BIO_TEST_EDIT,
				BIO_TEST_DELETE,
				BIO_TEST_OTHERS_EDIT,
				BIO_TEST_OTHERS_DELETE,
				BIO_MAILING_CREATE,
				BIO_MAILING_EDIT,
				BIO_MAILING_DELETE,
				BIO_MAILING_OTHERS_EDIT,
				BIO_MAILING_OTHERS_DELETE,
				BIO_COURSE_CREATE,
				BIO_COURSE_EDIT,
				BIO_COURSE_DELETE,
				BIO_COURSE_OTHERS_EDIT,
				BIO_COURSE_OTHERS_DELETE,
				BIO_BIOLOGY_THEME_CREATE,
				BIO_BIOLOGY_THEME_EDIT,
				BIO_BIOLOGY_THEME_DELETE,
				BIO_BIOLOGY_THEME_OTHERS_EDIT,
				BIO_BIOLOGY_THEME_OTHERS_DELETE,
				
				BIO_CLASS_CREATE,
				BIO_CLASS_EDIT,
				BIO_CLASS_DELETE,
				BIO_CLASS_OTHERS_EDIT,
				BIO_CLASS_OTHERS_DELETE,
				BIO_OLIMPIAD_TYPE_CREATE,
				BIO_OLIMPIAD_TYPE_EDIT,
				BIO_OLIMPIAD_TYPE_DELETE,
				BIO_OLIMPIAD_TYPE_OTHERS_EDIT,
				BIO_OLIMPIAD_TYPE_OTHERS_DELETE,
				BIO_ROLE_TAXONOMY_CREATE,
				BIO_ROLE_TAXONOMY_EDIT,
				BIO_ROLE_TAXONOMY_DELETE,
				BIO_ROLE_TAXONOMY_OTHERS_EDIT,
				BIO_ROLE_TAXONOMY_OTHERS_DELETE,
				
				BIO_TEST_CATEGORY_CREATE,
				BIO_TEST_CATEGORY_EDIT,
				BIO_TEST_CATEGORY_DELETE,
				BIO_TEST_CATEGORY_OTHERS_EDIT,
				BIO_TEST_CATEGORY_OTHERS_DELETE,
				
				BIO_ARTICLE_ADD_EVENT,
				BIO_ARTICLE_ADD_TEST,
				BIO_ARTICLE_ADD_FAVOR,
				BIO_ARTICLE_ADD_CATEGORY,
				/*	*/	
				BIO_ARTICLE_ADD_INCLUDES,
				BIO_QUESTION_CORRECT,
				BIO_USER_DELETE,
				BIO_REVIEW_CREATE,
				BIO_REVIEW_EDIT,
				BIO_REVIEW_DELETE
			], "editor"),
			"CourseLeader"	=> 	bio_cap_mask([ 
				
				BIO_ARTICLE_CREATE,
				BIO_ARTICLE_EDIT,
				BIO_ARTICLE_DELETE,
				BIO_ARTICLE_ADD_COURSE,
				BIO_TEST_CREATE,
				BIO_TEST_EDIT,
				BIO_TEST_DELETE,
				BIO_EVENT_CREATE,
				BIO_EVENT_EDIT,
				BIO_MAILING_CREATE,
				BIO_MAILING_EDIT,
				BIO_MAILING_DELETE,
				//BIO_COURSE_CREATE,
				BIO_COURSE_EDIT,
				//BIO_COURSE_DELETE,
				BIO_EVENT_ACCESS_REQUEST,
				BIO_COURSE_ACCESS_REQUEST,
				BIO_ARTICLE_ADD_EVENT,
				BIO_ARTICLE_ADD_INCLUDES,
				BIO_ARTICLE_REQUEST_TO_FIRST,
				BIO_REVIEW_CREATE,
				BIO_REVIEW_EDIT,
				BIO_REVIEW_DELETE,
				BIO_FACULTET_EDIT
			], "CourseLeader"),
			
			"TestEditor" => bio_cap_mask([ 					
				BIO_TEST_CREATE,
				BIO_TEST_EDIT,
				BIO_TEST_DELETE,
				BIO_TEST_OTHERS_EDIT,
				BIO_TEST_OTHERS_DELETE,
				BIO_ARTICLE_ADD_TEST,
				BIO_QUESTION_CORRECT,
				BIO_REVIEW_CREATE,
				BIO_REVIEW_EDIT,
				BIO_REVIEW_DELETE
			], "TestEditor"),
			"Teacher" => bio_cap_mask([ 					
				BIO_EVENT_SEND_REQUEST,
				BIO_COURSE_SEND_REQUEST,
				BIO_ARTICLE_ADD_FAVOR,
				BIO_REVIEW_CREATE,
				BIO_REVIEW_EDIT,
				BIO_REVIEW_DELETE
			], "Teacher"),
			"Pupil" => bio_cap_mask([ 					
				BIO_EVENT_SEND_REQUEST,
				BIO_COURSE_SEND_REQUEST,
				BIO_ARTICLE_ADD_FAVOR,
				BIO_REVIEW_CREATE,
				BIO_REVIEW_EDIT,
				BIO_REVIEW_DELETE
			], "Pupil"),
			"HiddenUser" => bio_cap_mask([ 
				BIO_ARTICLE_ADD_FAVOR 
			], "HiddenUser"),
			"contributor" => bio_cap_mask([ 
				BIO_ARTICLE_ADD_FAVOR,
				BIO_REVIEW_CREATE,
				BIO_REVIEW_EDIT,
				BIO_REVIEW_DELETE
			], "contributor")
		];		
		$bio_ts = ["Bio_Article", "Bio_Test", "Bio_Event", 'Bio_Mailing', 'Bio_Mailing_Group'];
		update_option("bio_send", $bio_ts);
		update_option("bio_sending", "elet");
		update_option(BIO, $options);		
		wp_clear_scheduled_hook( 'my_twice_event' );
		wp_schedule_event( time(), 'two_min', 'my_twice_event'); //  twicedaily
	}
	static function deactivate()
	{
		remove_role( "CourseLeader");
		remove_role( "TestEditor");
		remove_role( "EventMember");
		remove_role( "Teacher");
		remove_role( "HiddenUser");
		wp_clear_scheduled_hook('my_twice_event');
	}
	function __construct()
	{		
		static::$options = get_option(BIO);
		add_action( 'admin_menu',					[__CLASS__, 'admin_page_handler'], 9);
		add_action( 'admin_menu',					[__CLASS__, 'admin_page_separator'], 14);
		add_action( 'admin_enqueue_scripts', 		[__CLASS__, 'add_admin_js_script'] );
		add_action( 'wp_enqueue_scripts', 			[__CLASS__, 'add_frons_js_script'], 2 );
		add_action( 'admin_head',					[__CLASS__, 'hook_css']);	
		add_action( 'wp_head',						[__CLASS__, 'hook_css']);	
		add_filter( "smc_add_post_types",	 		[__CLASS__, "init_obj"], 10);
		add_filter( "smc_add_option",	 			[__CLASS__, "smc_add_option"], 9);
		add_filter( 'parent_file',					[__CLASS__,  'admin_menu_separator'] ); //	
		
		//REST API
		add_action( 'rest_api_init', 				[__CLASS__, "rest_api_init"] );
		add_action( 'rest_send_nocache_headers', 	[__CLASS__, "rest_send_nocache_headers"] );
		
		//CPU
		add_action('init', 							[__CLASS__, 'rewrite_rule_my']);
		
		// Отключаем админ панель для всех, кроме администраторов.
		add_action("init", 							[__CLASS__, 'hide_panel_handler']);
		add_filter('login_redirect', 				[__CLASS__, 'admin_default_page']);
		
		//static JSON menu generator
		add_filter("wp_update_nav_menu_item", 		[__CLASS__, "wp_update_nav_menu_item"], 10,3);
		
		//CRON	
		add_action('my_twice_event', 				[__CLASS__, 'my_twice_event'] );
				
	}
	
	static function my_twice_event()
	{
		do_action("bio_twice_event");
	}
	static function hook_css()
	{
		echo "<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=cyrillic' rel='stylesheet'>";
	}
	static function rewrite_rule_my()
	{
		add_rewrite_rule( '^(kabinet)/([^/]*)', 'index.php?pagename=$matches[1]&cab=$matches[2]', 'top' );
		add_filter( 'query_vars', function($vars)
		{
			$vars[] = 'cab';
			return $vars;
		});
	}
	static function admin_page_separator()
	{
		add_submenu_page( 'pe_edu_page', 'wp-menu-separator', '<div class="bio-separator"></div>', 'read', '#', '' );
	}
	static function admin_page_handler()
	{
		add_menu_page( 
			__('PE Edu', BIO), 
			__('PE Edu', BIO),
			'manage_options', 
			'pe_edu_page', 
			array(__CLASS__, 'get_admin'), 
			BIO_URLPATH . "assets/img/pe-oraculii-logotype.svg", //"dashicons-welcome-learn-more", // icon url  
			'2'
		);
		
	}
	
	static function get_admin()
	{
		require_once BIO_REAL_PATH . "tpl/admin.php" ;
	}
	/*
		configs for SMC_Object_type.php
	*/
	static function smc_add_option($init_object)
	{ 
		$init_object = $init_object ? $init_object : [];
		
		$init_object["email"] = [
			'type' => 'string',  
			"name" => __("Administrator email", BIO), 
			"hidden" => false
		];
		$init_object["adress"] = [
			'type' => 'string',  
			"name" => __("Public address", BIO), 
			"hidden" => false
		];
		$init_object["vk"] = [
			'type' => 'url',  
			"name" => __("VK link", BIO), 
			"hidden" => false
		];
		$init_object["youtube"] = [
			'type' => 'url',  
			"name" => __("YouTube Link", BIO), 
			"hidden" => false
		];
		$init_object["android"] = [
			'type' => 'url',  
			"name" => __("Android link", BIO), 
			"hidden" => false
		];
		$init_object["apple"] = [
			'type' => 'url',  
			"name" => __("Apple link", BIO), 
			"hidden" => false
		];
		$init_object["background_video"] = [
			'type' => 'url',  
			"name" => __("Background video URL", BIO), 
			"hidden" => false
		];
		$init_object["info_video"] = [
			'type' => 'url',  
			"name" => __("Info video URL", BIO), 
			"hidden" => false
		];
		$init_object["goods_type"] = [
			'type' => 'string',  
			"name" => __("Paying type", BIO), 
			"hidden" => false
		];
		$init_object["default_img"] = [
			'type' => 'media',  
			"name" => __("Default image", BIO), 
			"hidden" => false,
			"object" => "media" 
		]; 
		$init_object["default_img_name"] = [
			'type' => 'string',  
			"name" => __("Default image name", BIO), 
			"hidden" => true  
		]; 
		$init_object["thumbnail"] = [
			'type' => 'media',  
			"name" => __("Main thumbnail", BIO), 
			"hidden" => false,
			"object" => "media" 
		];
		$init_object["thumbnail_name"] = [
			'type' => 'string',  
			"name" => __("Main thumbnail name", BIO), 
			"hidden" => true 
		];
		$init_object["main_title"] = [
			'type' => 'string',  
			"name" => __("Main title", BIO), 
			"hidden" => true
		];
		$init_object["main_description"] = [
			'type' => 'string',  
			"name" => __("Main description", BIO), 
			"hidden" => true
		];
		$init_object["info_1_title"] = [
			'type' => 'string',  
			"name" => __("Info title 1", BIO), 
			"hidden" => true
		];
		$init_object["info_2_title"] = [
			'type' => 'string',  
			"name" => __("Info title 2", BIO), 
			"hidden" => true
		];
		$init_object["info_1_description"] = [
			'type' => 'string',  
			"name" => __("Info description 1", BIO), 
			"hidden" => true
		];
		$init_object["info_2_description"] = [
			'type' => 'string',  
			"name" => __("Info description 2", BIO), 
			"hidden" => true
		];
		$init_object["advantages_title"] = [
			'type' => 'string',  
			"name" => __("Advantages title", BIO), 
			"hidden" => true
		];
		$init_object["advantages_description"] = [
			'type' => 'string',  
			"name" => __("Advantages description", BIO), 
			"hidden" => true
		];
		$init_object["facultets_title"] = [
			'type' => 'string',  
			"name" => __("Facultets information page title", BIO), 
			"hidden" => true
		];
		$init_object["facultets_description"] = [
			'type' => 'string',  
			"name" => __("Facultets information page description", BIO), 
			"hidden" => true
		];
		$init_object["default_course"] = [
			'type' 		=> "taxonomy", 
			'smc_post'	=> "Bio_Course", 
			"object" 	=> BIO_COURSE_TYPE, 
			"name" 		=> __("Course", BIO), 
			"hidden" 	=> false,
			"object" 	=> "media" 
		];
		$init_object["web_client_url"] = [
			'type' => 'string',  
			"name" => __("Public address", BIO), 
			"hidden" => true
		];
		$init_object["pay_system"] = [
			'type' => 'string',  
			"name" => __("Pay system", BIO), 
			"hidden" => true
		];
		$init_object["robocassa_key"] = [
			'type' => 'string',  
			"name" => __("Robocassa key", BIO), 
			"hidden" => true
		];
		$init_object["robocassa_password_1"] = [
			'type' => 'string',  
			"name" => __("Robocassa Password 1", BIO), 
			"hidden" => true
		]; 
		$init_object["robocassa_password_2"] = [
			'type' => 'string',  
			"name" => __("Robocassa Password 2", BIO), 
			"hidden" => true
		]; 
		$init_object["jitsi_server_url"] = [
			'type' => 'url',  
			"name" => __("jitsi server url", BIO), 
			"hidden" => true
		]; 
		//wp_die( $init_object );
		return $init_object;
	}
	static function init_obj($init_object)
	{
		if(!is_array($init_object)) $init_object = [];
		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_Article'];
		//$point['picto']			= ['type' => "media", "name" => __("Icon", BIO)];
		//$point['test_id']		= ['type' => "post", 'smc_post'=> "Bio_Test", "object" => BIO_TEST_TYPE, "name" => __("Test", BIO)];
		//$point['event_id']		= ['type' => "post", 'smc_post'=> "Bio_Event", "object" => BIO_EVENT_TYPE, "name" => __("Event", BIO)];
		$point['include_id1']	= ['type' => 'media', "object" => "media",  "name" => __("Document 1", BIO), "thread" => false];
		$point['include_id1_name'] = ['type' => 'string',  "name" => __("Title of Document 1", BIO), "hidden" => true];
		$point['include_id2']	= ['type' => 'media', "object" => "media",  "name" => __("Document 2", BIO), "thread" => false];
		$point['include_id3']	= ['type' => 'media', "object" => "media",  "name" => __("Document 3", BIO), "thread" => false];
		$point['include_id4']	= ['type' => 'media', "object" => "media",  "name" => __("Document 4", BIO), "thread" => false];
		$point['order']			= ['type' => "number", "name" => __("Order", BIO)];
		$point['status']		= ['type' => 'post_status', "name" => __("Status", BIO), "hidden" => true];
		$point['is_member']		= ['type' => 'boolean', "name" => __("Current User is member of article's Course", BIO), "hidden" => true];
		$point['is_favorite']	= ['type' => 'boolean', "name" => __("Article in Favourite of current User", BIO), "hidden" => true];
		$point['is_free']		= ['type' => 'boolean', "name" => __("Free", BIO) ];
		$point['is_logged_in']	= ['type' => 'boolean', "name" => __("For logged Users", BIO) ];
		$init_object[BIO_ARTICLE_TYPE]	= $point;

		$point					= [];
		$point['t']				= ['type' => 'post'];
		$point['class']			= ['type' => 'Bio_Test'];
		$point['status']		= ['type' => 'post_status', "name" => __("Status", BIO)];	
		$point['is_timed']		= ['type' => 'boolean', "name" => __("With time limit", BIO)];	
		$point['duration']		= ['type' => 'number', "name" => __("Duration per sec", BIO)];	
		$point['try_count']		= ['type' => 'number', "name" => __("Try count.", BIO)];
		$point['hardness']		= ['type' => 'number', "name" => __("Hardness", BIO)];
		$point['is_show_answer']= ['type' => 'boolean', "name" => __("Show right answer after choose", BIO)];	
		$point['is_show_results']=['type' => 'boolean', "name" => __("Show result after test", BIO)];	
		$point['is_shuffle'] 	= ['type' => 'boolean', "name" => __("Shuffle Questions?", BIO)];	
		$point['is_group'] 		= ['type' => 'boolean', "name" => __("Group by type", BIO)];	
		$point['publish_type']	= ['type' => 'number', "name" => __("Publish type", BIO)];	
		$point['is_member']		= ['type' => 'boolean', "name" => __("Current User is member of test's Course", BIO), "hidden" => true];
		$point['questions']		= [
			'type' 		=> "array",
			"class"		=> "special",
			"object" 	=> "question", 
			"name" 		=> __("Questions", BIO)
		];
		$init_object[BIO_TEST_TYPE]	= $point;
		
		
		$event					= [];
		$event['t']				= ['type' => 'post'];
		$event['class']			= ['type' => 'Bio_Event'];	
		$event['time']			= ['type' => "date", 	"name" => __("Time", BIO)];
		$event['test_id']		= ['type' => "post", 'smc_post'=> "Bio_Test", "object" => BIO_TEST_TYPE, "name" => __("Test", BIO)];
		$event['status']		= ['type' => 'post_status', "name" => __("Status", BIO)];		
		$event['request_count']	= ['type' => 'number', "name" => __("Count of current requests", BIO) ];
		$event['member_status']	= ['type' => 'number', "name" => __("Current User Membership Status", BIO) ];
		$event['request_form']	= ['type' => 'String', "name" => __("Request From data", BIO) ];
		$event['is_member']		= ['type' => 'boolean', "name" => __("Current User is member of event's Course", BIO), "hidden" => true];
		//$point['type']			= ['type' => 'number', "name" => __("Type", BIO)]; // intramural, extramural
		$init_object[BIO_EVENT_TYPE]	= $event; 
		
		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_Mailing'];
		$point['started']		= ['type' => "date", "name" => __("Started", BIO)];
		$point['event_id']		= ['type' => "post", 'smc_post'=> "Bio_Event", "object" => BIO_EVENT_TYPE, "name" => __("Event", BIO)];
		$init_object[BIO_MAILING_TYPE]	= $point;
		
		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_Mailing_Group'];
		$point['mailing_id']	= ['type' => "post", 'smc_post'=> "Bio_Mailing", "object" => BIO_MAILING_TYPE, "name" => __("Mailing", BIO)];
		$init_object[BIO_MAILING_GROUP_TYPE]	= $point;
		
		
		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_Testinomial'];
		$point['display_name']	= ['type' => "string", "name" => __("Author", BIO)];
		$point['raiting']		= ['type' => "number", "name" => __("Raiting", BIO)];
		$init_object[BIO_TESTINOMIAL_TYPE]	= $point;
		
		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['adressee']		= ['type' => "user", "name" => __("Adressee", BIO)];
		$point['name']			= ['type' => "string", "name" => __("Name", BIO)];
		$point['_email']		= ['type' => "string", "name" => __("Email", BIO)];
		$point['class']			= ['type' => 'Bio_Message'];
		$init_object[BIO_MESSAGE_TYPE]	= $point;
		
		$point					= [];
		$point['t']				= ['type' => 'taxonomy'];	
		$point['class']			= ['type' => 'Bio_Course', "order" => "ASC", "order_by_meta" => "order" ];
		$point['order']			= ['type' => "number", "name" => __("Order", BIO)];
		$point[BIO_FACULTET_TYPE]	= ['type' => "taxonomy", 'smc_post'=> "Bio_Facultet", "object" => BIO_FACULTET_TYPE, "name" => __("Facultet", BIO)];
		$point['date']			= ['type' => "string", "name" => __("Фиктивная дата", BIO)];
		$point['thumbnail']		= ['type' => "picto", "name" => __("Icon", BIO), "thread" => false ];
		$point['thumbnail_name']= ['type' => "string", "name" => __("Icon name", BIO), "hidden" => true ];
		$point['include']		= ['type' => 'string', "name" => __("Document 1", BIO), "thread" => false];
		
		 
		$point['testinomials']	= [
			'type' 		=> "array", 
			'class'		=> "Bio_Testinomial", 
			"object" 	=> BIO_TESTINOMIAL_TYPE, 
			"name" 		=> __("Testinomial", BIO)
		];
		
		$point[BIO_EVENT_TYPE]	= [
			'type' 		=> "array", 
			'class'	=> "Bio_Event", 
			"object" 	=> BIO_EVENT_TYPE, 
			"name" 		=> __("Events", BIO)
		];
		$point[BIO_TEST_TYPE]	= [
			'type' 		=> "array", 
			'class'		=> "Bio_Test", 
			"object" 	=> BIO_TEST_TYPE, 
			"name" 		=> __("Tests", BIO)
		];
		$point['articles']	= [
			'type' 		=> "array", 
			"filter"	=> ["Bio_User", "is_member_course"],
			'class'		=> "Bio_Article", 
			"object" 	=> BIO_ARTICLE_TYPE, 
			"name" 		=> __("Article", BIO)
		];
		$point['locked']		= [
			'type' 		=> 'boolean', 
			"name" 		=> __("Course is locked for free subscribe", BIO), 
			"hidden"	=> true
		];
		$point['is_member']		= [
			'type' 		=> 'boolean',
			"name" 		=> __("Current User is member of Course", BIO), 
			"hidden" 	=> true
		];
		$point['is_closed']		= [
			'type' 		=> 'boolean', 
			"name" 		=> __("Users mayn't request membership", BIO), 
			"hidden" 	=> true
		];
		$init_object[BIO_COURSE_TYPE]	= $point;
		
		$point					= [];
		$point['t']				= ['type' => 'taxonomy'];	
		$point['class']			= ['type' => 'Bio_Role_Taxonomy'];
		$point['role']			= ['type' => "role",  "name" => __("Role", BIO)];
		$init_object[BIO_ROLE_TAXONOMY_TYPE]	= $point;
		
		$point					= [];
		$point['t']				= ['type' => 'taxonomy'];	
		$point['class']			= ['type' => 'Bio_Discount'];
		$point['role']			= ['type' => "role",  "name" => __("Discount", BIO)];
		$init_object[BIO_DISCOUNT_TYPE]	= $point;
		
		$point					= [];
		$point['t']				= ['type' => 'taxonomy'];	
		$point['class']			= ['type' => 'Bio_Class', "order" => "ASC", "order_by_meta" => "order" ];
		$point['order']			= ['type' => "number", "name" => __("Order", BIO)];
		
		$point[BIO_EVENT_TYPE]	= [
			'type' 		=> "array", 
			'class'	=> "Bio_Event", 
			"object" 	=> BIO_EVENT_TYPE, 
			"name" 		=> __("Events", BIO)
		];
		$point[BIO_TEST_TYPE]	= [
			'type' 		=> "array", 
			'class'		=> "Bio_Test", 
			"object" 	=> BIO_TEST_TYPE, 
			"name" 		=> __("Tests", BIO)
		];
		$point['articles']	= [
			'type' 		=> "array", 
			"filter"	=> ["Bio_User", "is_member_course"],
			'class'		=> "Bio_Article", 
			"object" 	=> BIO_ARTICLE_TYPE, 
			"name" 		=> __("Article", BIO)
		];
		$init_object[BIO_CLASS_TYPE]	= $point;
		
		$point					= [];
		$point['t']				= ['type' => 'taxonomy'];	
		$point['class']			= ['type' => 'Bio_Biology_Theme'];
		$point['order']			= ['type' => "number", "name" => __("Order", BIO)];
		$point['icon']			= ['type' => "picto", "name" => __("Icon", BIO)];
		$point['thumbnail']		= ['type' => "picto", "name" => __("Thumbnail", BIO)];
		$point['thumbnail_name']= ['type' => "string", "name" => __("Thumbnail name", BIO)];
		$point[BIO_EVENT_TYPE]	= [
			'type' 		=> "array", 
			'class'	=> "Bio_Event", 
			"object" 	=> BIO_EVENT_TYPE, 
			"name" 		=> __("Events", BIO)
		];
		$point[BIO_TEST_TYPE]	= [
			'type' 		=> "array", 
			'class'		=> "Bio_Test", 
			"object" 	=> BIO_TEST_TYPE, 
			"name" 		=> __("Tests", BIO)
		];
		$point['articles']	= [
			'type' 		=> "array", 
			"filter"	=> ["Bio_User", "is_member_course"],
			'class'		=> "Bio_Article", 
			"object" 	=> BIO_ARTICLE_TYPE, 
			"name" 		=> __("Article", BIO)
		];
		$init_object[BIO_BIOLOGY_THEME_TYPE]	= $point;
		
		$point					= [];
		$point['t']				= ['type' => 'taxonomy'];	
		$point['class']			= ['type' => 'Bio_Olimpiad_Type'];
		$point[BIO_EVENT_TYPE]	= [
			'type' 		=> "array", 
			'class'	=> "Bio_Event", 
			"object" 	=> BIO_EVENT_TYPE, 
			"name" 		=> __("Events", BIO)
		];
		$point[BIO_TEST_TYPE]	= [
			'type' 		=> "array", 
			'class'		=> "Bio_Test", 
			"object" 	=> BIO_TEST_TYPE, 
			"name" 		=> __("Tests", BIO)
		];
		$point['articles']	= [
			'type' 		=> "array", 
			"filter"	=> ["Bio_User", "is_member_course"],
			'class'		=> "Bio_Article", 
			"object" 	=> BIO_ARTICLE_TYPE, 
			"name" 		=> __("Article", BIO)
		];
		$init_object[BIO_OLIMPIAD_TYPE_TYPE]	= $point;

		$point					= [];
		$point['t']				= ['type' => 'taxonomy'];	
		$point['class']			= ['type' => 'Bio_Test_Category'];
		$point[BIO_TEST_TYPE]	= [
			'type' 		=> "array", 
			'class'		=> "Bio_Test", 
			"object" 	=> BIO_TEST_TYPE, 
			"name" 		=> __("Tests", BIO)
		];
		$init_object[BIO_TEST_CATEGORY_TYPE]	= $point;
 
		$point					= [];
		$point['t']				= ['type' => 'taxonomy'];	
		$point['class']			= ['type' => 'Bio_Facultet'];		
		$point['icon']			= ['type' => "media", "name" => __("Icon", BIO)];
		$point['icon_name']		= ['type' => "text", "name" => __("Icon name", BIO), "hidden" => true ];
		$point['title_descr']	= ['type' => "text", "name" => __("Title of description", BIO)];
		$point['description']	= ['type' => "text", "name" => __("Full description", BIO)];
		$point['order']			= ['type' => "number", "name" => __("Order", BIO)];
		$point['locked']		= ['type' => "boolean", "name" => __("Locked", BIO)];
		$point['adress']		= ['type' => "string", "name" => __("Adress", BIO), "hidden" => true ];
		$point['finish_date']	= ['type' => "date", "name" => __("Finish Date of use Facultet by current User", BIO)];
		$point['courses']	= [
			'type' 		=> "array", 
			"sortby"	=> "order", 
			'class'		=> "Bio_Course", 
			"object" 	=> BIO_COURSE_TYPE, 
			"name" 		=> __("Courses", BIO)
		];	
		$init_object[BIO_FACULTET_TYPE]	= $point;
		return $init_object;		
	}
	
	
	static function add_frons_js_script()
	{
		//css
		wp_register_style("bootstrap", 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', array());
		wp_enqueue_style( "bootstrap");
		wp_register_style(BIO, BIO_URLPATH . 'assets/css/bio.css', array());
		wp_enqueue_style( BIO);
		wp_register_style("css-file-icons", BIO_URLPATH . 'assets/css/css-file-icons.css', array());
		wp_enqueue_style( "css-file-icons" );
		wp_register_style("fontawesome", 'https://use.fontawesome.com/releases/v5.5.0/css/all.css', []);
		wp_enqueue_style( "fontawesome" );
		//js
		wp_register_script("popper", 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js' , array());
		wp_enqueue_script("popper");
		wp_register_script("bootstrap", 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js' , array());
		wp_enqueue_script("bootstrap");
						
		wp_register_script(BIO, plugins_url( '../assets/js/bio.js', __FILE__ ), array());
		wp_enqueue_script(BIO);
		
		// load media library scripts
		wp_enqueue_media();
		//ajax
		wp_localize_script( 
			'jquery', 
			'myajax', 
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('myajax-nonce')
			)
		);	
	}
	static function add_admin_js_script()
	{	
		//css
		wp_register_style("bootstrap", BIO_URLPATH . 'assets/css/bootstrap.min.css', array());
		wp_enqueue_style( "bootstrap");
		wp_register_style("formhelpers", BIO_URLPATH . 'assets/css/bootstrap-formhelpers.min.css', array());
		wp_enqueue_style( "formhelpers");		
		wp_register_style("periodpicker", BIO_URLPATH . 'assets/css/jquery.periodpicker.min.css', array());
		wp_enqueue_style( "periodpicker");
		wp_register_style("fontawesome", 'https://use.fontawesome.com/releases/v5.5.0/css/all.css', []);
		wp_enqueue_style( "fontawesome" );
		
		wp_register_style(BIO, BIO_URLPATH . 'assets/css/bio.css', array());
		wp_enqueue_style( BIO);
					
		wp_register_script("bio_admin", plugins_url( '../assets/js/bio_admin.js', __FILE__ ), array());
		wp_enqueue_script("bio_admin");
		wp_register_script("bootstrap", BIO_URLPATH . 'assets/js/bootstrap.min.js' , array());
		wp_enqueue_script("bootstrap");
		wp_register_script("datetimepicker", BIO_URLPATH . 'assets/js/jquery.datetimepicker.js' , array());
		wp_enqueue_script("datetimepicker");
		
		// load media library scripts
		wp_enqueue_media();
		//ajax
		wp_localize_script( 
			'jquery', 
			'myajax', 
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('myajax-nonce')
			)
		);			
		$yes = substr_count ($_SERVER["REQUEST_URI"],  "/wp-admin/post.php?post=") && $_GET['action'] == "edit";
		$yes = $yes || $_SERVER["REQUEST_URI"] == "/wp-admin/admin.php?page=pe_edu_page";
		$yes = $yes || substr_count ($_SERVER["REQUEST_URI"],  "/wp-admin/users.php");
		$yes = $yes || substr_count ($_SERVER["REQUEST_URI"],  "/wp-admin/post-new.php");
		$yes = $yes || $_SERVER["REQUEST_URI"] == "/wp-admin/post-new.php?post_type=article";
		
		if( !$yes ) return;
			
			//js
			wp_register_script("bootstrap", plugins_url( '../js/bootstrap.min.js', __FILE__ ), array());
			wp_enqueue_script("bootstrap");
			wp_register_script("formhelpers", plugins_url( '../js/bootstrap-formhelpers.min.js', __FILE__ ), array());
			wp_enqueue_script("formhelpers");
			wp_register_script(BIO, plugins_url( '../assets/js/bio.js', __FILE__ ), array());
			wp_enqueue_script(BIO);
			
			// load media library scripts
			wp_enqueue_media();
			wp_enqueue_style( 'editor-buttons' );

	}
	static function hide_panel_handler()
	{
		if (!current_user_can('manage_options'))  show_admin_bar(false);
	}
	static function confured($elems)
	{
		return implode("", $elems);
	}
	static function admin_default_page()
	{
		 return '/?loginform=true';
	}
	static function wp_update_nav_menu_item( $menu_id, $menu_item_db_id, $args )
	{
		/*
		ob_start();
		var_dump($menu_item_db_id);
		$v1 = ob_get_contents();
		ob_end_clean();
		
		ob_start();
		var_dump($menu_id);
		$v2 = ob_get_contents();
		ob_end_clean();
		
		ob_start();
		var_dump($args);
		$v3 = ob_get_contents();
		ob_end_clean();
		
		file_put_contents(BIO_REAL_PATH."temp/menu.txt", $v1 . " ---  " . $v2 . " --- " . $v3);
		*/
		//require_once BIO_REAL_PATH . "class/Bio_REST.class.php";
		//file_put_contents( ABSPATH."static/json/menu.json", json_encode(Bio_REST::get_menu()) );		
		
	}
	static function admin_menu_separator( $parent_file )
	{
		// Handle main menu separators
		$menu = &$GLOBALS['menu'];
		foreach( $menu as $key => $item )
		{
			if (
				in_array( 'wp-menu-separator', $item )
				AND 5 < count( $item )
				)
			{
				$menu[ $key ][2] = 'separator';
				$menu[ $key ][4] = 'wp-menu-separator';
				unset(
					 $menu[ $key ][5]
					,$menu[ $key ][6]
				);
			}
		}
		return $parent_file;
	}
}
<?php 

require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;


class Bio_Question extends SMC_Post
{
	static function get_type()
	{
		return BIO_QUESTION_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 2);	
		//add_action('admin_menu',				[__CLASS__, 'my_extra_fields2']);
		add_action("pe_graphql_make_schema", 	[__CLASS__, "exec_graphql"], 8);
		parent::init();
	}
	static function exec_graphql()
	{
		try
		{
			static::register_gq( );
		}
		catch(Bio_GraphQLNotAccess $ew)
		{
			
		}
		catch(Bio_GraphQLNotLogged $ew)
		{
			
		}
		catch(Bio_GraphQLNotAdmin $ew)
		{
			
		}
		catch(Bio_GraphQLError $ew)
		{
			
		}
	}
	static function register_gq()
	{
		PEGraphql::add_query( 
			'getFullQuestionHiddenCount', 
			[
				'description' 		=> __( "Get all hidden Questions (Need Admin's approve", BIO ),
				'type' 				=> Type::int(),
				'args'     			=> [ ],
				'resolve' 			=> function( $root, $args, $context, $info )
				{
					global $wpdb;	 
					$query = "SELECT COUNT(q.ID) FROM bio_question q WHERE  ( q.hidden='true' OR q.hidden='1')";
					//wp_die( $query );
					return $wpdb->get_var( $query );
				}
			] 
		);
		PEGraphql::add_query( 
			'getBio_TestQuestions', 
			[
				'description' 		=> __( "Get test's questions array", BIO ),
				'type' 				=> Type::listOf( PEGraphql::object_type("Bio_TestQuestion") ),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("Paging") 
					]
				],
				'resolve' 			=> function( $root, $args, $context, $info )
				{	
					 $args['paging']['offset'] = ceil( $args['paging']['offset'] / $args['paging']['count'] );
					// wp_die($args['paging']);
					return static::_get_all_matrixes( $args['paging'] );
				}
			] 
		);
		PEGraphql::add_query( 
			'getBio_TestQuestionAnswers', 
			[
				'description' 		=> __( "Get test's questions array", BIO ),
				'type' 				=> Type::listOf( PEGraphql::object_type("Bio_TestQuestionAnswer") ),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("Paging") 
					]
				],
				'resolve' 			=> function( $root, $args, $context, $info )
				{	
					 $args['paging']['offset'] = ceil( $args['paging']['offset'] / $args['paging']['count'] );
					// wp_die($args['paging']);
					//return static::_get_all_matrixes( $args['paging'], $root, $context, $info);
					return [];
				}
			] 
		);
		PEGraphql::add_query( 
			"getBio_TestQuestionCount", 
			[
				'description' 		=> __( "Get full count test's questions", BIO ),
				'type' 				=> Type::int(),
				'args'     			=> [  
					'paging'	=> [
						"type" 	=> PEGraphql::input_type("Paging") 
					]
				],
				'resolve' 			=> function( $root, $args, $context, $info )
				{	
					$args['paging']['count'] = 200000000;
					$args['paging']['offset'] = 0;
					$args['paging']['result_fields'] = " COUNT(q.id) AS nc";
					$q = static::get_all_questions_objs( $args['paging']);
					return (int)$q[0]->nc;
				}
			] 
		);
	}
	static function get_all_questions_objs($params=[])
	{
		if(!isset( $params["offset"] ) )
		{
			$params["offset"] = 0;
		}
		if(!isset( $params["count"] ) )
		{
			$params["numberposts"] = 10;
		}
		else
		{
			$params["numberposts"] = $params["count"];
			unset($params["count"]);
		}
		if(
			isset($params["taxonomies"]) 
			&& isset($params["taxonomies"][0]['tax_name']) 
			&& is_array($params["taxonomies"][0]['term_ids']) 
		)
		{
			$params[ $params["taxonomies"][0]['tax_name'] ] = $params["taxonomies"][0]['term_ids'][0];
		}
		if(
			isset($params["metas"]) 
			&& isset($params["metas"][0]['key']) 
		)
		{
			for($i=0; $i < count($params["metas"]); $i++)
			{
				switch( $params["metas"][$i]['key'] )
				{
					case BIO_TEST_TYPE:
					case "single_test_id":
						$params[ "test_id" ] = $params["metas"][$i]['value'];
						break;
					case "type":
						$params[ "type" ] = $params["metas"][$i]['value'];
						break;
					case "single":
						$params[ "single" ] = (bool)$params["metas"][$i]['value_bool'];
						break;
					case "hidden":
						$params[ "hidden" ] = (bool)$params["metas"][$i]['value_bool'];
						break;
					default:
						null;
				}
			}
		}
		unset($params["taxonomies"]);
		unset($params["metas"]);
		unset($params["meta_relation"]);
		unset($params["tax_relation"]);
		//wp_die( $params );
		return Bio_Test_API::get_questions($params);
	}
	static function _get_all_matrixes( $params=[] )
	{
		global $wpdb;
		//wp_die( $params ); 
		$qs = static::get_all_questions_objs($params);
		// wp_die($qs); 
		$matrixes = [];
		foreach($qs as $q)
		{
			$matrixes[]	= static::get_single_matrix( $q->id );
		}
		
		return $matrixes;
	}
	static function get_single_matrix( $id )
	{
		global $wpdb;
		$qs = Bio_Test_API::get_question( $id );
		$q = $qs[0];
		$theme = Bio_Biology_Theme::get_instance( (int)$q->bio_biolody_theme );
		// wp_die( $q );
		$m = [
			"id"				=> $q->id,
			"post_title" 		=> $q->questiontext,
			"post_content" 		=> $q->questiontext,
			"thumbnail"			=> wp_get_attachment_url ($q->image_id, "full" ),
			"post_author"		=> Bio_User::get_user( $q->author_id ),
			"single_test_id" 	=> $q->single_test_id,
			"type" 				=> $q->type,
			"answernumbering" 	=> $q->answernumbering == "1",
			"image_id" 			=> $q->image_id,
			"correctfeedback" 	=> null,
			"defaultgrade" 		=> null,
			"generalfeedback" 	=> null,
			"hidden" 			=> $q->hidden,
			"partiallycorrectfeedback" => null,
			"penalty"	 		=> 	(float)$q->penalty,
			"shuffleanswers" 	=> $q->shuffleanswers == "1",
			"single"			=> $q->single != "false" && $q->single != "" && $q->single != "0",
			"is_deleted"		=> $q->is_deleted,
			"order"				=> $q->position,
			"answers"			=> Bio_Test_API::get_question_answers( $q->id ),
			BIO_BIOLOGY_THEME_TYPE	=> $theme->get_single_matrix(  )
		];
		$tests = $wpdb->get_results("SELECT test_id FROM `bio_test_question` WHERE question_id='".$q->id."'");
		$bt = [];
		foreach( $tests as $test )
		{
			$bt[] = Bio_Test::get_single_matrix( $test->test_id );
		}
		$m[ BIO_TEST_TYPE ] 	= $bt;
		return $m;
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Question", BIO), // Основное название типа записи
			'singular_name'      => __("Question", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Question", BIO), 
			'all_items' 		 => __('Questions', BIO),
			'add_new_item'       => __("add Question", BIO), 
			'edit_item'          => __("edit Question", BIO), 
			'new_item'           => __("add Question", BIO), 
			'view_item'          => __("see Question", BIO), 
			'search_items'       => __("search Question", BIO), 
			'not_found'          => __("no Questions", BIO), 
			'not_found_in_trash' => __("no Questions in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Questions", BIO), 
		);
		register_post_type(
			BIO_QUESTION_TYPE, 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 3,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array('title','editor','thumbnail'),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
	static function add_views_column( $columns )
	{
		$posts_columns 			= parent::add_views_column( $columns ); 
		return $posts_columns;			
	}

    public static function api_action($type, $methods, $code, $pars, $user)
    {
        switch($methods) {
            case "update":
                if(is_numeric($code)) 
				{
                    $data 	= $pars["data"];
                    $qid	= $pars["post_id"];
                    $data	=  Bio_Question::edit($data, $qid);
                    $msg	 	= __("Question saccessful updated", BIO);
                }
				else
				{
                    $msg = 'error';
                }

                break;
            case "delete":
                if(is_numeric($code)) 
				{
                    $msg = 'error';
                }
				else
				{
                    $msg = 'error';
                }
                break;
            case "create":
                break;
            case "read":
            default:
                if(is_numeric($code)) 
				{

                }
				else
				{

                }
                break;

        }
        /*

    type	: "bio_edit_question",
    pars	: {
        data	: @object
            {
                ID	: <int> - если нет или меньше 1 - создать
                post_title: <string> 	- обязательно
                post_content: <string> 	- обязательно
                test_id : <int>
                thumbnail: <int>
                --- данные ответов - очень сложно!!!
            }
        post_id	: <int>
    }
*/
        return Array (
            "msg" => $msg,
            "id" => $code,
            "update"=> $update
        );

    }

    function get_anwers()
	{
		return Bio_Answer::get_all([ "question_id" => $this->id ]);
	}
	function draw()
	{
		$answers 	= $this->get_anwers();
		if($thrumb_url	= get_the_post_thumbnail_url($this->id))
		{
			$thrumb = "<img src='$thrumb_url' class='img-thumbnail'/>";
		}
		shuffle($answers );
		$html  		= "
		<div class='row answer'>
			<div class='col-12'>
				<div class='bio_title'>" . $this->get("post_title") . "</div>
			</div>
			<div class='col-12 bio_descr'>".
				$this->get_descr() .
			"</div><div class='spacer-10'></div>
			<div class='col-12 text-center'>$thrumb</div>
			</div><div class='spacer-10'></div>
			";
		$i = 0;
		$html 	.="<div class='row'>";
		switch(count($answers))
		{
			case 2:
				$cc = 6;
				break;
			case 3:
			case 6:
			case 9:
				$cc = 4;
				break;
			case 4:
			case 7:
			case 8:
				$cc = 3;
				break;
			
			case 5:
			default:
				$cc = 2;
				break;
		}
		foreach( $answers as $answer )
		{
			$inpt 	= in_array($this->get_meta("type"), [1,2]) ? "checkbox" : "radio";
			$ans 	= Bio_Answer::get_instance( $answer );
			$pic	= get_the_post_thumbnail_url( $ans->id );
			
			$html 	.= $pic ? "
			<div class='bio_question_pic_instance col-md-$cc col-12'>
				<div class='bio_answer_pic' style='background-image:url($pic);''></div>
				<input type='$inpt' name='test" . $this->id . "' id='ans".$ans->id."' class='$inpt' value='".$ans->id."'/> 
				<label for='ans".$ans->id."'>" . $ans->get("post_title") . "</label>
			</div>" : "
			<div class='bio_question_instance'>
				<input type='$inpt' name='test" . $this->id . "' id='ans".$ans->id."' class='$inpt' value='".$ans->id."'/> 
				<label for='ans".$ans->id."'>" . $ans->get("post_title") . "</label>
			</div>";
			$i++;
		}
		$html  		.= "</div>";
		return $html;
	}
	function get_descr()
	{
		switch($type = $this->get_meta("type"))
		{
			case 0:
				$html = __("Choose right variant", BIO);
				break;
			case 1:
				$html = __("Choose all right variants", BIO);
				break;
			case 2:
			default:
				$html = __("Sort answers", BIO);
				break;
		}
		return $html;
	}
	function get_admin_form()
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		$icon_id	= get_post_thumbnail_id( $this->id );
		$asns		= Bio_Answer::get_all( [ "question_id" => $this->id ] );
		$answers = "
		<form class='cab_edit row' command='bio_edit_question' post_id='$this->id'>
			
				<subtitle class='col-12'>". __( "Title" ). "</subtitle>
				<div class='col-12'>
					<input type='text' name='post_title' value='".
						$this->get("post_title") . 
					"' class='form-control' />
				</div>
				<div class='spacer-10'></div>
			
				<subtitle class='col-12'>". __("Icon", BIO). "</subtitle>
				<div class='col-12'>
					<div class='clearfix'>".
						get_input_file_form2( "group_icon", $icon_id, "thumbnail", "" ) .
					"</div>
					<div class='bio_descr'>".
						__("Insert the Article icon. Maximum size - 400px", BIO) .
					"</div>	
				</div>	
				<div class='spacer-10'></div>
				
				<subtitle class='w-100'>". __("Answers", BIO). "</subtitle>
				<ul class='bio_ans-form'>";
		$i=0;
		foreach($asns as $ans)
		{
			$answers 	.= Bio_Answer::get_admin_stroke($ans, $i, false);
			$i++;
		}
		$answers .= "</ul>
				<input type='hidden' name='answer_count' value='$i' />
				<div class='row'>
					<div class='col-10'>
						<input type='submit' class='btn btn-primary' value='". __("Save") . "' />
					</div>
					<div class='col-2 text-right bio_add_answer'>
						<div class='btn btn-outline-secondary btn-sm'><i class='fas fa-plus'></i></div>
					</div>
				</div>
		</form>";
		$answers 	.= Bio_Answer::get_admin_stroke( );
		return $answers;
	}
	
	function get_answers_form()
	{
		$asns		= Bio_Answer::get_all( [ "question_id" => $this->id ] );
		$answers = "
			<subtitle class='w-100'>". __("Asnwers", BIO). "</subtitle>
			<ul class='bio_ans-form'>";
		foreach($asns as $ans)
		{
			$answer 	= Bio_Answer::get_instance($ans);
			$idd = "ans_". $this->id ."_" . $answer->id;
			$answers .= "<li class='" . ( $answer->get_meta("is_right") ? "active" : "" ) . "'>
				<span>".
					$answer->get("post_title") . 
				"</span>
			</li>";
		}
		$answers .= "</ul>";
		return $answers;
	}
	function get_admin_form2()
	{
		
		$html = "
			<div class='spacer-10'></div>
			<subtitle class='w-100'>". __("Question", BIO). "</subtitle>" . 
				static::wp_dropdown([
					"id"			=> "admin_test_quest_" . $this->id,
					"class"			=> "form-control ",
					"selected"		=> $this->id,
					"posts"			=> static::get_all([], -1, 0, 'title', 'ASC')
				]) . 
			"<div class='spacer-10'></div>".
			$this->get_answers_form() .
			"<div class='handle center' title='Delete'>
				<i class='fas fa-times'></i>
			</div>";
		return $html;
	}
	static function edit($data, $id=-1)
	{
		$post_title	= $data['post_title'];	
		$thumbnail	= $data['thumbnail'];
		$count 		= $data['answer_count'];
		
		if($id == -1)
		{
			$quest = static::insert([
				"post_title"	=> $post_title,
				"post_name"		=> $post_title,
				"post_content"	=> "",
				"post_status"	=> Bio_User::is_user_roles("administrator", "TestEditor") ? "publish": "draft"
			]);
			$id 	= $quest->id;
		}
		else
		{
			wp_update_post([
				"ID"			=> $id,
				"post_title"	=> $post_title
			]);
		}
		if($thumbnail)
			set_post_thumbnail($id, $thumbnail);
		$q 			= static::get_instance( $id );
		
		$sr 		= [];
		$aids		= [];
		for($i=0; $i<$count; $i++)
		{
			$aid 		= $data[ "answer_id[$i" ];	
			$title 		= $data[ "answer_text[$i" ];	
			$thumb 		= $data[ "answer_thumbnail[$i" ];	
			$is_right 	= (int)$data[ "answer_is_right[$i" ];
			
			if($aid == -1)
			{
				$aid = Bio_Answer::insert([
					"post_title"	=> $title,
					"post_name"		=> $title,
					"post_content"	=> "",
					"post_status"	=> "publish",
					"is_right"		=> $is_right,
				]);
				$aid = $aid->id;
			}
			else
			{
				wp_update_post([
					"ID"			=> $aid,
					"post_title"	=> $title,
				]);
			}
			update_post_meta($aid, "is_right", $is_right );
			update_post_meta($aid, "question_id", $id );
			if($thumbnail)
				set_post_thumbnail($aid, $thumb);
			$sr[] = ["answer_id"=>$aid, "answer_text"=>$title, "answer_thumbnail"=>$thumb, "answer_is_right"=>$is_right, "question_id" => $id ];
			$aids[] = $aid;
		}
		
		$all 		= Bio_Answer::get_all_ids(["question_id" => $id]);
		$diff		= array_diff($all, $aids);
		foreach( $diff as $iid)
		{
			wp_delete_post($iid);
		}
		return true;
	}

    static function get_question($id)
    {
        $question = Bio_Question::get_instance($id);
        $q = [
            "post_title"	=> $question->get("post_title"),
            "post_content"	=> $question->get("post_content"),
            "trumbnail"		=> get_the_post_thumbnail_url( $id, "full" ),
        ];
        $q['answers']	= [];
        return $q;

    }




}
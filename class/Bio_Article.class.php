<?php 

class Bio_Article extends Bio_Conglomerath
{
	static function get_type()
	{
		return BIO_ARTICLE_TYPE;
	}
	static function admin_menu()
	{
		
	}
	/**/
	static function get_all_matrixes($params)
	{
		//wp_die($params);
		if(isset($params['metas'] ) && ( $params['metas'][0]["key"] == "is_favorite"  ) )
		{
			$value = $params['metas'][0]["value"];
			unset($params['metas'][0]);
			$params['is_favorite'] = $value;
		}
		unset($params['is_admin'] );
		$matrixes = apply_filters( "bio_articles_get_all_matrixes", parent::get_all_matrixes($params), $params );
		return $matrixes;
	}
	
	
	
	static function init()
	{
		add_action('init', 							[ __CLASS__, 'register_all' ], 2);
		add_action("admin_menu",					[ __CLASS__, "admin_menu"] );
		add_filter("smc_post_matrix",				[ __CLASS__, "smc_post_matrix"], 10, 2);
		add_filter("bio_articles_get_all_matrixes", [ __CLASS__, "only_is_member_facultet"], 10, 2);
		add_filter("bio_articles_get_all_matrixes", [ __CLASS__, "filter_is_favorite"], 11, 2);
		add_filter("smc_post_get_all_args", 		[ __CLASS__, "smc_post_get_all_args"] );
		parent::init(); 
	}
	static function smc_post_get_all_args( $args )
	{ 
		if(current_user_can("manage_options"))	return $args;
		if( $args["post_type"] == static::get_type() )
		{
			if($args['is_admin'])
			{
				if( !isset( $args["tax_query"] ) )
				{
					$args["tax_query"] = [ "relation" => "OR" ];
				}
				$args["tax_query"][] = [
					"taxonomy"		=> BIO_COURSE_TYPE,
					"fields"		=> "id",
					"terms"			=> Bio_Facultet::get_facultet_courses(Bio_Facultet::get_user_facultet( null, true ) , true)
				];
			}
			if($args['is_favorite'])
			{
				$all_favorite = static::all_favorite();
				//wp_die( $all_favorite );
			}
			//wp_die( $args );
		}
		return $args;
	}
	
	static function filter_is_favorite($matrixes, $params)
	{
		if( isset( $params['is_favorite'] ) )
		{
			$ms = [];
			$all_favorite = static::all_favorite();
			// wp_die( $all_favorite );
			foreach($matrixes as $matrix)
			{
				if( in_array( $matrix['id'], $all_favorite ) )
				{
					$ms [] = $matrix;
				}
			}
			 $matrixes = $ms;
		}
		//wp_die( $matrixes );
		return $matrixes;
	}
	
	static function only_is_member_facultet($matrixes, $params)
	{
		/**/
		if( current_user_can("manage_options")  ) 
		{
			foreach($matrixes as &$matrix)
			{
				$matrix['is_member'] = true;
			}
			return $matrixes;
			
		}
		
		$courses	= is_user_logged_in() 
			?  
			Bio_Facultet::get_facultet_courses( Bio_Facultet::get_user_facultet( null, true ) , true) 
			: 
			[];
		//wp_die( Bio_Facultet::get_facultet_courses( [] , true)  );
		$ms 		= [];
		foreach($matrixes as $matrix)
		{
			$bio_courses = wp_get_object_terms( $matrix['id'], BIO_COURSE_TYPE, ["fields" =>"ids"]);
			//
			$yes = false;
			if($matrix['is_free'])
			{
				$ms [] = $matrix;
				continue;
			}
			if($matrix['is_logged_in'] && is_user_logged_in() )
			{
				$ms [] = $matrix;
				continue;
			}
			foreach($bio_courses as $bio_course)
			{
				if( in_array($bio_course, $courses ) )
				{
					$yes = true;
					break;
				}				
			}
			if(!$yes)
			{
				$matrix['post_content'] = "";
				$matrix['is_member'] = false;
			}
			else
			{
				$matrix['is_member'] = true;
			}
			$ms [] = $matrix;
			
		}
		$matrixes = $ms;
		return $matrixes;
	}
	
	static function smc_post_matrix( $fields, $post)
	{
		if(is_user_logged_in() )
		{
			if ($post->post_type != static::get_type())
				return $fields;
			$ar		= static::get_instance($post);
			$fields['is_favorite']	= $ar->is_favorite() > 0;
			
		}
		return $fields;
	}
	
	function access_req_cat($req_id)
	{
		global $wpdb;
		$query = "SELECT * FROM ".$wpdb->prefix."article_cat_user_requests WHERE ID='$req_id'";
		$rest_call 	= $wpdb->get_results($query);
		$ts 		= wp_get_object_terms( $this->id, Bio_Category::get_type() );
		$terms		= [];
		foreach($ts as $t)
		{
			$terms[] = $t->term_id;
		}
		$terms[]	= (int)($rest_call[0]->cat_id);
		wp_set_object_terms(
			$this->id,
			$terms,
			Bio_Category::get_type()
		);
		$query1 = "DELETE FROM ".$wpdb->prefix."article_cat_user_requests WHERE ID='$req_id'";
		$wpdb->query($query1);
		return (int)($rest_call[0]->cat_id);
	}
	function delete_req_cat($req_id)
	{
		global $wpdb;
		$query1 = "DELETE FROM ".$wpdb->prefix."article_cat_user_requests WHERE ID='$req_id'";
		return $wpdb->query($query1);
	}
	static function delete_cat_request($article_id, $user_id)
	{
		global $wpdb;
		$query="DELETE FROM ".$wpdb->prefix."article_cat_user_requests WHERE article_id='$article_id' AND user_id='$user_id'";
		$wpdb->query($query);
		return $query;
	}
	static function update_cat_request($article_id, $cat_id, $user_id)
	{
		global $wpdb;
		$query="DELETE FROM `".$wpdb->prefix."article_cat_user_requests` WHERE article_id='$article_id' AND cat_id='$cat_id' AND user_id='$user_id'";
			$wpdb->query($query);
		$query = "INSERT INTO `".$wpdb->prefix."article_cat_user_requests` (`ID`, `article_id`, `cat_id`, `user_id`, `date`) VALUES (NULL, '$article_id', '$cat_id', '$user_id', CURRENT_TIMESTAMP);";
			$editors = get_users(["role__in" => ["editor"]]);
			
			$emails =[];
			foreach($editors as $editor)
			{
				$emails[] = $editor->user_email;
			}	
			$course	= get_term_by("id", $cat_id, Bio_Category::get_type());
			$article = static::get_instance( $article_id );
			$user	= get_user_by("id", $user_id);
			Bio_Mailing::send_mail(
				sprintf( __("User request add «%s» to «%s»", BIO),  $article->get("post_title"), $course->name), 
				sprintf( __("Please goto admin panel «%s»", BIO), $article->get("post_title")) ,
				wp_get_current_user(), 
				$emails
			);
			/**/
		return $wpdb->query($query);
	}
	function get_category_requests()
	{
		global $wpdb;
		$query = "SELECT DISTINCT * from `".$wpdb->prefix."article_cat_user_requests` WHERE article_id='" . $this->id ."';";
		return $wpdb->get_results($query);
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Article", BIO), // Основное название типа записи
			'singular_name'      => __("Article", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Article", BIO), 
			'all_items' 		 => __('Articles', BIO),
			'add_new_item'       => __("add Article", BIO), 
			'edit_item'          => __("edit Article", BIO), 
			'new_item'           => __("add Article", BIO), 
			'view_item'          => __("see Article", BIO), 
			'search_items'       => __("search Article", BIO), 
			'not_found'          => __("no Articles", BIO), 
			'not_found_in_trash' => __("no Articles in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Articles", BIO), 
		);
		$pt = register_post_type(
			BIO_ARTICLE_TYPE, 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 3,
				"taxonomies"		 => ["category"],
				"menu_icon"			 => "dashicons-megaphone",
				'show_in_nav_menus'  => true,
				'supports'           => array('title','editor','author','thumbnail','excerpt','comments'),
				"rewrite"			 => ["slug" => ""]
			]
		);
		//register_taxonomy_for_object_type("category", BIO_ARTICLE_TYPE);
		//require_once ABSPATH."wp-admin/includes/nav-menu.php"; 
		//add_meta_box( "add-post-type-".BIO_ARTICLE_TYPE , __("Article", BIO), 'wp_nav_menu_item_post_type_meta_box', 'nav-menus', 'side', 1 , $pt);
	}
	static function add_views_column( $columns )
	{
		$columns = parent::add_views_column( $columns );
		unset($columns["include_id1"]);
		unset($columns["include_id2"]);
		unset($columns["include_id3"]);
		unset($columns["include_id4"]);
		$columns['inclides']	= __("Includes", BIO);
		$columns['author']		= __("Author");
		return $columns;
	}
	static function fill_views_column($column_name, $post_id) 
	{
		switch($column_name)
		{
			case "inclides":
				$include	= "<table>";
				for($i=1; $i<5; $i++)
				{
					$inc  	= wp_get_attachment_url( get_post_meta($post_id, "include_id$i", true) );
					if( $inc )
					{
						$name = substr($inc, strrpos($inc, "/")+1, -1);
						$include .= "<tr><td><a href='$inc'>$name</a></td></tr>";
					}
				}
				$include	.= "</table>";
				echo $include;
				break;
			case "status":
				$req		= "Курсы";
				$categories	= get_the_terms( $post_id, "category" ) ;
				$html = "
				<div class='row' post_id='$post_id'>
					<div class='col-9'>".
						wp_dropdown_categories([
							'show_option_none'   => '--',
							'orderby'            => 'name',
							'echo'               => 0,
							'selected'           => $categories[0] ? $categories[0]->term_id : -1,
							'name'               => 'bio_cat',
							'class'              => 'form-control bio-change-cat',
							'hide_if_empty'      => false,
							'hide_empty'      	=> false,							
						]) .
					"</div>
					<div class='col-3'>
						<div class='btn btn-outline-secondary bio_aticle_req_cat w-100' bio_cat='" . $req . "' >
							<i class='fas fa-question-circle'></i>
						</div>
					</div>
				</div>
				
				<div class='spacer-10'></div>";				
				echo $html;
				parent::fill_views_column($column_name, $post_id);
				break;
			default:
				parent::fill_views_column($column_name, $post_id);
		}
	}
	static function save_admin_edit($lt)
	{
		$arr = parent::save_admin_edit($lt);
		return $arr;
	}
	
	static function insert($data)
	{
        $data['post_name'] = $data['post_title'];
		$id		= wp_insert_post(
			array(
				"post_type"		=> $data['post_type'] ? $data['post_type'] : static::get_type(),
				'post_name'    	=> $data['post_name'],
				'post_title'    => $data['post_title'],
				'post_content'  => $data['post_content'],
				'post_status'   => 'publish',
				"post_author"	=> $data['post_author'] ? $data['post_author'] : get_current_user_id()
			)
		);
		$data['post_content']	= Bio_Assistants::extract_img( $data['post_content'], "post_image" );
		$id					= wp_update_post( [
			"ID"			=> $id,
			"post_content"	=> $data['post_content']
		]);
        if( $data['thumbnail_id'] < 1 )
        {
            $media = Bio_Assistants::insert_media([
                "data" => $data['thumbnail'],
                "media_name"=> $data['media_name']
            ], $id);
			wp_set_object_terms( $media['id'], (int)Bio::$options['test_media_free'], BIO_MEDIA_TAXONOMY_TYPE );
            $data['thumbnail_id']	= $media['id'];
            $data['thumbnail']		= $media['url'];
        }

        if( $data["thumbnail_id"] > 0 )
        {
            set_post_thumbnail(
                $id,
                (int) $data["thumbnail_id"]
            );
        }
		
		$post	= static::get_instance($id);
        if( $data["trumbnail"] )
        {
            set_post_thumbnail(
                $id,
                (int)$data["trumbnail"]
            );
            unset ($data["trumbnail"]);
        }
		
        if( $data["course"] )
		{
			$terms = [];
			foreach($data["course"] as $term)
			{
				$terms[] = (int)$term;
			}
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Course::get_type()
			);
			unset ($data["course"]);
		}
		/* only for GraphQL */
        if( $data[BIO_COURSE_TYPE] )
		{
			$terms = [];
			foreach($data[BIO_COURSE_TYPE] as $term)
			{
				$terms[] = (int)$term;
			}
			wp_set_object_terms(
				$id,
				$terms,
				BIO_COURSE_TYPE
			);
			unset ($data[BIO_COURSE_TYPE]);
		}
		
		if( $data[Bio_Biology_Theme::get_type()] )
		{
			if(is_array($data[Bio_Biology_Theme::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Biology_Theme::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Biology_Theme::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Biology_Theme::get_type()
			);
			unset ($data[Bio_Biology_Theme::get_type()]);
		}
		
		if( $data[Bio_Category::get_type()] )
		{
			if(is_array($data[Bio_Category::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Category::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Category::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Category::get_type()
			);
			unset ($data[Bio_Category::get_type()]);
		}
		
		
		//request for first 
		
		static::delete_cat_request( $id, get_current_user_id() );
		if( $data["req_categories"] &&  is_array( $data["req_categories"] ) )
		{
			foreach($data["req_categories"] as $req)
			{
				static::update_cat_request( $id, $req, get_current_user_id() );
			}
		}
		unset ($data["req_categories"]);
		
		
		//
		if( $data[Bio_Class::get_type()] )
		{
			if(is_array($data[Bio_Class::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Class::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Class::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Class::get_type()
			);
			unset ($data[Bio_Class::get_type()]);
		}
		//
		if( $data[Bio_Olimpiad_Type::get_type()] )
		{
			if(is_array($data[Bio_Olimpiad_Type::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Olimpiad_Type::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Olimpiad_Type::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Olimpiad_Type::get_type()
			);
			unset ($data[Bio_Olimpiad_Type::get_type()]);
		}
		
		if( $data[Bio_Role_Taxonomy::get_type()] )
		{
			if(is_array($data[Bio_Role_Taxonomy::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Role_Taxonomy::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Role_Taxonomy::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Role_Taxonomy::get_type()
			);
			unset ($data[Bio_Role_Taxonomy::get_type()]);
		}
		//includes
		for($i=1; $i<5; $i++)
		{
			delete_post_meta($id, "include_id" . $i);
		}
		$i=1;
		foreach($data['includes'] as $include )
		{
			if( $include['id'] == -1)
			{
				$media 		= Bio_Assistants::insert_media([
					"data" 		=> $include['url'],
					"media_name"=> $include['name']
				], $id);
				wp_set_object_terms( $media['id'], (int)Bio::$options['test_media_free'], BIO_MEDIA_TAXONOMY_TYPE );
				$data["include_id" . $i] = $media['id'];
				//$post->update_meta("include_id" . $i, $media['id']); 
			}
			else if( $include['id'] > 0 )
			{
				$data["include_id" . $i] = $include['id'];
				//$post->update_meta( "include_id" . $i, $include['id'] ); 
			}
			$i++;
		}
		unset($data['includes']);
		//$artdata = [ "data" => $data ];
		
		//test
		if($data['test'] > 0)
		{
			$data['test_id'] = $data['test'];
			unset($data['test']);
		}
		
		//event
		if($data['event'] > 0)
		{
			$data['event_id'] = $data['event'];
			unset($data['event']);
		}


		$post->update_metas($data);
		return $post;
	}


	static function update($data, $id )
	{
		global $artdata, $rest_log;

		$cd = [];
		//$data['post_content']	= Bio_Assistants::extract_img( $data['post_content'], "post_image", $id);
		foreach($data as $key => $val)
		{
			if(in_array(
				$key, 
				["post_type", 'post_name', 'post_title', 'post_content', 'post_status', "post_author", "thumbnail"]
			))
				$cd[$key]	= $val;
		}

		$cd['ID']	= $id;
		$rest_log	= $cd;
		$id			= wp_update_post( $cd );
		$post		= static::get_instance($id);

		$post->update_thumbnail($data);	

		/* FOR GRAPH_QL */
		if( $data[BIO_COURSE_TYPE])
		{
			$terms = [];
			foreach($data[BIO_COURSE_TYPE] as $term)
			{
				$terms[] = (int)$term;
			}
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Course::get_type()
			);
			unset ($data[ BIO_COURSE_TYPE ]);
		}
		/*  END FOR GRAPH_QL */
		
		if( $data["course"] )
		{
			$terms = [];
			foreach($data["course"] as $term)
			{
				$terms[] = (int)$term;
			}
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Course::get_type()
			);
			unset ($data["course"]);
		}
		if( $data[Bio_Biology_Theme::get_type()] )
		{
			if(is_array($data[Bio_Biology_Theme::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Biology_Theme::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Biology_Theme::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Biology_Theme::get_type()
			);
			unset ($data[Bio_Biology_Theme::get_type()]);
		}
		if( $data[Bio_Category::get_type()] )
		{
			if(is_array($data[Bio_Category::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Category::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
			{
				$terms = [(int)$data[Bio_Category::get_type()]];
			}
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Category::get_type()
			);
			unset ($data[Bio_Category::get_type()]);
		}
		
		//request for first 
		
		static::delete_cat_request( $id, get_current_user_id() );
		if( $data["req_categories"] &&  is_array( $data["req_categories"] ) )
		{
			foreach($data["req_categories"] as $req)
			{
				static::update_cat_request( $id, $req, get_current_user_id() );
			}
		}
		unset ($data["req_categories"]);
		
		
		//
		if( $data[Bio_Class::get_type()] )
		{
			if(is_array($data[Bio_Class::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Class::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Class::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Class::get_type()
			);
			unset ($data[Bio_Class::get_type()]);
		}
		if( $data[Bio_Olimpiad_Type::get_type()] )
		{
			if(is_array($data[Bio_Olimpiad_Type::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Olimpiad_Type::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Olimpiad_Type::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Olimpiad_Type::get_type()
			);
			unset ($data[Bio_Olimpiad_Type::get_type()]);
		}
		
		if( $data[Bio_Role_Taxonomy::get_type()] )
		{
			if(is_array($data[Bio_Role_Taxonomy::get_type()]))
			{
				$terms = [];
				foreach($data[Bio_Role_Taxonomy::get_type()] as $term)
				{
					$terms[] = (int)$term;
				}
			}
			else
				$terms = [(int)$data[Bio_Role_Taxonomy::get_type()]];
			wp_set_object_terms(
				$id,
				$terms,
				Bio_Role_Taxonomy::get_type()
			);
			unset ($data[Bio_Role_Taxonomy::get_type()]);
		}
		//includes
		for($i=1; $i<5; $i++)
		{
			delete_post_meta($id, "include_id" . $i);
		}
		$i=1;
		foreach($data['includes'] as $include )
		{
			if( $include['id'] == -1)
			{
				$media 		= Bio_Assistants::insert_media([
					"data" 		=> $include['url'],
					"media_name"=> $include['name']
				], $id);
				wp_set_object_terms( $media['id'], (int)Bio::$options['test_media_free'], BIO_MEDIA_TAXONOMY_TYPE );
				$data["include_id" . $i] = $media['id'];
				//$post->update_meta("include_id" . $i, $media['id']); 
			}
			else if( $include['id'] > 0 )
			{
				$data["include_id" . $i] = $include['id'];
				//$post->update_meta( "include_id" . $i, $include['id'] ); 
			}
			$i++;
		}
		unset($data['includes']);
		//$artdata = [ "data" => $data ];
		
		//test
		if($data['test'] > 0)
		{
			$data['test_id'] = $data['test'];
			unset($data['test']);
		}
		
		//event
		if($data['event'] > 0)
		{
			$data['event_id'] = $data['event'];
			unset($data['event']);
		}
		
		if(isset($data['is_favorite']))
		{
			if($data['is_favorite'])
			{
				$post->add_to_favorite(true);
			}
			else
			{
				$post->add_to_favorite(false);
			}
			unset($data['is_favorite']);
		}
		
		
		$post->update_metas($data);
		return $post;
	}
	
	/*
    public static function get_post( $p, $is_full = true )
	{
		return static::get_article( $p, true );
	}
	*/
    public static function get_article( $p, $is_full = true )
    {
			return [];
			global $wpdb;
            $art				= Bio_Article::get_instance($p);
            $a					= [];
            $a['id']			= $art->id;
            $a['ID']			= $art->id;
			
			// request for category
			if($is_full)
			{
				$aee 	= $art->get_category_requests();
				$a['req_categories'] 	= [ ];
				$exists = [];
				foreach($aee as $req)
				{
					if(in_array((int)$req->cat_id, $exists)) continue;
					$a['req_categories'][] = [
						"article_id"	=> (int)$art->id,
						"cat_id"		=> (int)$req->cat_id,
						"id"			=> (int)$req->ID,
						"category"		=> Bio_Category::get_category( (int)$req->cat_id ),
						"user"			=> Bio_User::get_user($req->user_id),
					];
					$exists[] = (int)$req->cat_id;
				}
			}
			
            $a['post_title']	= $art->get("post_title");
            
            $a['post_status']	= $art->get("post_status");
            $a['post_content']	= $is_full ?
                $art->get("post_content") :
                wp_strip_all_tags(wp_trim_words($art->get("post_content"), 20));
            $a['post_date']		= date_i18n( 'j F Y', strtotime( $art->get("post_date") ) );
            $thumbnail			= get_the_post_thumbnail_url( $art->id, "full" );
            $a['thumbnail']		= $thumbnail ? $thumbnail : BIO_EMPTY_IMG;
            $a['thumbnail_id']	= (int)get_post_thumbnail_id( $art->id ) ;
            // Тест
            $t					= [];
			
			$test_id 			= $art->get_meta("test_id");
            if( $test_id > 0)
            {
                $test			= Bio_Test::get_instance($test_id);
                $t['id']		= $test->id;
                $t['ID']		= $test->id;
                $t['post_title']= $test->get("post_title");
            }
			
            $a['test']			= $t;
            $a['test_id']		= $t;

            // Вложения
            $a['includes']		= [];
            for( $i = 1; $i < 5; $i++ )
            {
                $inc			= [];
                $inc_id			= $art->get_meta("include_id" . $i); 
                if($inc_url = wp_get_attachment_url( $inc_id ))
                {
                    $inc['id']	= $inc_id;
                    $inc['url']	= $inc_url;
                }
                $a['includes'][] = $inc;
            }
			/**/
            // Курс
            $course				= [];
			$bio_courses		= [];
            $cours	= get_the_terms($art->id, BIO_COURSE_TYPE);
            if($cours)
            {
				foreach($cours as $crs)
                {
					$course[] 	= Bio_Course::get_course($crs);
					$__course 	= Bio_Course::get_instance($crs );
					$bio_courses[] = $__course->get_single_matrix();
				}
            }
            $a["course"]		= $course;
            $a[BIO_COURSE_TYPE]	= $bio_courses;
  
            
            // Categories
            $cats				= [];
            $categs	= get_the_terms($art->id, "category");
            if(count($categs) && $categs && !is_wp_error($categs))
            {
                foreach($categs as $categ)
                {
                    $cats[]			= Bio_Category::get_category( $categ );
                }
            }
            $a["category"]		= $cats;

            // Role Taxonomy type
            $rtax				= [];
            $olps	= get_the_terms($art->id, BIO_ROLE_TAXONOMY_TYPE);
            if(count($olps) && $olps && !is_wp_error($olps))
            {
                foreach($olps as $categ)
                {
                    $rtax[]			= Bio_Category::get_category( $categ );
                }
            }
            $a[BIO_ROLE_TAXONOMY_TYPE]		= $rtax;

            // Olimpiad type
            $olp				= [];
            $olps	= get_the_terms($art->id, BIO_OLIMPIAD_TYPE_TYPE);
            if(count($olps) && $olps && !is_wp_error($olps))
            {
                foreach($olps as $categ)
                {
                    $olp[]			= Bio_Category::get_category( $categ );
                }
            }
            $a[BIO_OLIMPIAD_TYPE_TYPE]		= $olp;

            // BIO_BIOLOGY_THEME_TYPE
            $cats				= [];
            $categs	= get_the_terms($art->id, BIO_BIOLOGY_THEME_TYPE);
            if(count($categs) && $categs && !is_wp_error($categs))
            {
                foreach($categs as $categ)
                {
                    $cats[]			= Bio_Olimpiad_Type::get_category( $categ );
                }
            }
            $a[BIO_BIOLOGY_THEME_TYPE]		= $cats;

            // BIO_CLASS_TYPE
            $cats				= [];
            $categs	= get_the_terms($art->id, BIO_CLASS_TYPE);
            if(count($categs) && $categs && !is_wp_error($categs))
            {
                foreach($categs as $categ)
                {
                    $cats[]			= Bio_Biology_Theme::get_category( $categ );
                }
            }
            $a[BIO_CLASS_TYPE]		= $cats;
			
			
			
			
            // author
            $user				= get_user_by("id", $art->get("post_author"));
            $auth				= [];
			$auth["id"]			= $user->ID;
			$auth["ID"]			= $user->ID;
            $auth["display_name"]= $user->display_name;
            $a['post_author']	= $auth;
			
			$a['order']			= $art->get_meta("order");
            //event
            $event_id			= $art->get_meta("event_id");
            if($event_id)
            {
                $a['event']		= Bio_Event::get_event( $event_id );
                $a['event_id']	= Bio_Event::get_event( $event_id );
				if ($a['event_id']['id'] == -1)
				{
					$a['event_id'] = null;
				}
            }
			else 
			{
				$a['event']		= Bio_Event::get_event( -1 );
				$a['event_id']	= null;//Bio_Event::get_event( -1 );
			}
			
			//includes
			$includes 	= [];
			for($i = 1; $i < 5; $i++)
			{
				$inc_id	= get_post_meta($art->id, "include_id$i", true);
				$inc  	= wp_get_attachment_url( $inc_id );
				if( $inc )
				{
					//$ext		= wp_check_filetype($inc)['ext'];
					$ext		= substr($inc, strrpos($inc, ".")+1);
					
					$includes[]	= [
						"url"	=> $inc,
						"name"	=> substr($inc, strrpos($inc, "/")+1), //basename($inc),
						"ext"	=> $ext,
						"id"	=> $inc_id
					];
					$a['include_id'.$i] = $inc;
					$include .= "	
						<div class='fi fi-$ext fi-size-xs'>
							<div class='fi-content'>$ext</div>
						</div>";
				}
				
			}
			$a['includes']	= $includes;
			
             
            //is favorite
            $a['is_favor']		= $art->is_favorite() ? 1 : 0;
            return $a;
        }

    public static function api_action($type, $methods, $code, $pars, $user)
    {
		global $artdata, $rest_log;
		$count		= -1;
        $articles	= [];
        switch($methods) {
            case "update":
                if(is_numeric($code)) 
				{
                    Bio_Article::update($pars, $code);
                    $articles[]	= get_post($code);
                    $msg = 'success';
                }
				else
				{
                    $msg = 'error';
                }
                break;
            case "delete":
                if(is_numeric($code)) 
				{
					Bio_User::access_caps(BIO_ARTICLE_DELETE, "Delete article");
                    Bio_Article::delete($code);
                    $msg = __("Article removed succesfully", BIO);
                }
				else
				{
                    $msg = 'error';
                }
                break;
            case "create":
                if(($code>0)) 
				{
					Bio_User::access_caps(BIO_ARTICLE_EDIT, "Update article");
                    Bio_Article::update($pars, $code);
                    $art = get_post($code);
					$msg = sprintf( __("Article «%s» updated successfully", BIO), $art->post_title ); 
					$articles[]	=  static::get_article( $art, true );
                }
				else
				{
					Bio_User::access_caps(BIO_ARTICLE_CREATE, "Insert article");
                    $article = Bio_Article::insert($pars);
                    $articles[]	= static::get_article( $article->body, true );
                    $msg = __("Article inserted successfully", BIO);
                }
                break;
            case "read":
            default:
                if(is_numeric($code))
				{
                    $ar			= static::get_article( $code, true );					
					$articles[]	= $ar;
                }
				else
				{
					$author = $pars['author'] == -2 && !Bio_User::is_user_roles(['administrator', 'editor'])
						? get_current_user_id() 
						: $pars['author'];
                    $all 		= apply_filters(
						"bio_get_articles", 
						static::get_all(
							isset($pars['metas']) 			? $pars['metas'] 		: -1, 		// []
							isset($pars['numberposts'])		? $pars['numberposts'] 	: -1,  		// -1
							isset($pars['offset'])			? $pars['offset']		: 0,  		// 0
							isset($pars['order_by'])		? $pars['order_by']		: "date", 	// 'title'
							isset($pars['order'])			? $pars['order']		: 'DESC', 	// 'DESC'
							isset($pars['order_by_meta'])	? $pars['order_by_meta']: "", 		// ""
							"all", 																// $pars['fields'],
							isset($pars['relation'])		? $pars['relation']		: "AND",	// "AND",
							$author,
							isset($pars['taxonomies'])		? $pars['taxonomies']	: -1,		//
							"OR", 
							isset($pars['post_status'])		? $pars['post_status']	: "publish"		//

						), 
						$pars
					);
                    foreach($all as $p)
                    {
                        $a 					= static::get_article( $p, $pars['is_full'] );
                        $articles[]			= $a;
                    }
					
                    $count 		= count(static::get_all(
                        isset($pars['metas']) 			? $pars['metas'] 		: -1, 		// []
                        -1,  		// -1
                        0,  		// 0
                        "date", 	// 'title'
                        'DESC', 	// 'DESC'
                        isset($pars['order_by_meta'])	? $pars['order_by_meta']: "", 		// ""
                        "ids", 																// $pars['fields'],
                        isset($pars['relation'])		? $pars['relation']		: "AND",	// "AND",
                        $author,
                        isset($pars['taxonomies'])		? $pars['taxonomies']	: -1,		//
						"OR", 
						isset($pars['post_status'])		? $pars['post_status']	: "publish"		//
                    ));
                }
        }
        return [
            "articles" => $articles,
            "id" => $code,
			"count" => (int)$count,
			"numberposts" => (int)$pars['numberposts'],
			"offset" => (int)$pars['offset'],
			"msg"	=> $msg
        ];


    }

    function add_to_favorite($check=1)
	{
		global $wpdb;
		$user_id = get_current_user_id();
		if(!$check)
		{
			$query = "DELETE FROM `". $wpdb->prefix ."user_article` WHERE post_id='$this->id' AND user_id='$user_id';";
		}
		else
		{
			$query = " INSERT INTO `". $wpdb->prefix ."user_article` ( `ID` , `user_id` , `post_id` , `date` ) VALUES ( NULL , '$user_id', '$this->id', CURRENT_TIMESTAMP );";
		}
		return $wpdb->query($query);
	}
	function is_favorite()
	{
		global $wpdb;
		$user_id = get_current_user_id();
		$query = "SELECT COUNT(*) FROM `". $wpdb->prefix ."user_article` WHERE post_id='".$this->id."' AND user_id='".$user_id."' LIMIT 1;";
		// wp_die([$query, $wpdb->get_var($query)]);
		return $wpdb->get_var($query);
	}
	static function all_favorite()
	{
		global $wpdb;
		$user_id = get_current_user_id();
		$query = "SELECT post_id FROM `". $wpdb->prefix ."user_article` WHERE user_id='".$user_id."' ";
		// wp_die([$query, $wpdb->get_var($query)]);
		$all = $wpdb->get_results($query);
		$all_favorite = [];
		foreach($all as $single)
		{
			$all_favorite[] = $single->post_id;
		}
		//  wp_die( $all_favorite );
		return $all_favorite;
	}
	function draw()
	{
		$cats		= get_the_term_list(
			$post->ID, 
			"category",
			"<span class='bio_cats_list'>",
			" ", 
			"</span>"
		);
		$cours		= get_the_terms($post->ID, BIO_COURSE_TYPE);
		$courses	= "<div class='bio_course_list'>";
		if(count($cours) && $cours && !is_wp_error($cours) )
		{
			foreach($cours as $crs)
			{
				$icon = get_term_meta( $crs->term_id, 'icon', true ); 
				$logo = wp_get_attachment_image_src($icon, "full")[0];
				$courses .= "
				<a class='bio-course-icon' style='background-image:URL($logo);' href='" . get_term_link($crs->term_id, BIO_COURSE_TYPE) . "'>".
					$crs->name . 
				"</a>";
			}
		}
		$courses .= "</div>";
				
		//event form
		$event_id = (int)$this->get_meta("event_id");
		if( $event_id > 0 )
		{
			$event = Bio_Event::get_instance($event_id);
			$event_form	= $event->request_form();
		}
		
		if(is_user_logged_in())
		{
			if($is_favorite = $this->is_favorite())
			{
				$favor  = "
					<span class='bio_added' post_id='$this->id'>
						<i class='fas fa-heart  text-danger' title='" . __('In my favorites', BIO)."'></i>
					</span>";
			}
			else
			{
				$favor  = "
					<span class='bio_add' post_id='$this->id'>
						<i class='far fa-heart' title='" . __("Add to favorites", BIO) ."'></i>
					</span>";
			}
			$include	= "<div class='bio_div'>";
			for($i=1; $i<5; $i++)
			{
				$inc  	= wp_get_attachment_url( get_post_meta($this->id, "include_id$i", true) );
				if( $inc )
				{
					$ext	= wp_check_filetype($inc)['ext'];
					$include .= "	
						<div class='fi fi-$ext fi-size-xs'>
							<div class='fi-content'>$ext</div>
						</div>";
				}
			}
			$include	.= "</div>";
			//test
			$test_id 	= get_post_meta($this->id, "test_id", true);
			if($test_id > 0)
			{
				$t = Bio_Test::get_instance($test_id);
				$test = "
				<div class='bio_div'>
					<a class='bio_ptest' href='" . get_permalink($test_id) . "'>
						<i class='fas fa-envelope-open-text'></i> ".
						$t->get("post_title").
					"</a>
				</div>";
			}
		}
		$html = "
			<div class='row bio_post'>
				<div class='col-12 bio_title'>".
					$this->get("post_title").
				"</div>
				<div class='col-12 bio_meta'>
					<span class='bio_pdate'>
						<i class='far fa-clock'></i> ".
						$this->get("post_date").
					"</span>
					
					<span class='bio_pauthor'>
						<i class='far fa-user'></i> ".
						get_user_by("id", $this->get("post_author"))->display_name.
					"</span>	
					$cats				
					$favor					
				</div>
				
				<div class='col-12'>".
					apply_filters( "the_content", $this->get("post_content") ) .
				"</div>
				<div class='col-12 bio_meta'>
					$test
					$include
					$courses
					$event_form
				</div>
			</div>";
		return $html;
	}
	function draw_arch( $is_full = false )
	{
		$post		= $this->body;
		$article	= $this;
		$cours		= get_the_terms($post->ID, BIO_COURSE_TYPE);
		$cats		= get_the_term_list(
			$post->ID, 
			"category",
			"<span class='bio_cats_list'>",
			" ", 
			"</span>"
		);
		$courses	= "<div class='bio_course_list'>";
		if( $cours !== false && !is_wp_error( $cours ) && count($cours) )
		{
			foreach($cours as $crs)
			{
				$icon = get_term_meta( $crs->term_id, 'icon', true ); 
				$logo = wp_get_attachment_image_src($icon, "full")[0];
				$courses .= "
				<a class='bio-course-icon' style='background-image:URL($logo);' href='" . get_term_link($crs->term_id, BIO_COURSE_TYPE) . "'>".
					$crs->name . 
				"</a>";
			}
		}
		$courses .= "</div>";
		
		
		if(is_user_logged_in())
		{
			
			if($is_favorite = $article->is_favorite())
			{
				$favor  = "
					<span class='bio_added ' post_id='$this->id' >
						<i class='fas fa-heart text-danger' title='" . __('In my favorites', BIO)."'></i>
					</span>";
			}
			else
			{
				$favor  = "
					<span class='bio_add' post_id='$post->ID'>
						<i class='far fa-heart' title='" . __("Add to favorites", BIO) ."'></i>
					</span>";
			}
			
			$test_id 	= get_post_meta($post->ID, "test_id", true);			
			if($test_id > 0)
			{
				
			}
		}
		
		$html .= "
		<div class='row bio_post'>
			<div class='col-12 bio_title'>
				<a href='" . get_permalink($article->id) . "'>".
					$article->get("post_title").
				"</a>
			</div>
			<div class='col-12 bio_meta'>
				<span class='bio_pdate'>
					<i class='far fa-clock'></i> ".
					$article->get("post_date").
				"</span>
				
				<span class='bio_pauthor'>
					<i class='far fa-user'></i> ".
					get_user_by("id", $article->get("post_author"))->display_name.
				"</span>
				
				$cats
				$favor
				
			</div>
			
			<div class='col-12' aid='$article->id'>".
				//wp_trim_words(apply_filters( "the_content", $article->get("post_content") ), 35).
				wp_trim_words(wp_strip_all_tags($article->get("post_content")), 35, "... ") .				
			"</div>
			<div class='spacer-20'></div>
			<div class='col-12 bio_meta'>
				$courses 
			</div>
			<i class='fas fa-chevron-circle-down fa-2x bio-more-arrow' cocmd='bio_article_more' data='$article->id'></i>
		</div>";
		return $html;
	}
	
	function get_admin_form()
	{
		require_once(BIO_REAL_PATH."tpl/input_file_form.php");
		ob_start();
		wp_editor($this->get('post_content'), "post_content");
		$editor 	= ob_get_contents();
		ob_end_clean();
		
		
		$courses	= get_the_terms( $this->id, BIO_COURSE_TYPE ) ;
		$course		=  is_wp_error($courses) ? -1 : $courses[0]->term_id;

		$include_id1		= $this->get_meta("include_id1");
		$include_id2		= $this->get_meta("include_id2");
		$include_id3		= $this->get_meta("include_id3");
		$include_id4		= $this->get_meta("include_id4");
		$icon_id	= get_post_thumbnail_id( $this->id );
		$html = "
		<div class='row justify-content-md-center' post_id='$this->id' command='bio_edit_article'>
			<ul class='nav nav-pills mb-3 justify-content-center text-center' id='pills-tab' role='tablist'>
				<li class='nav-item'>
					<a class='nav-link active' id='pills-home-tab' data-toggle='pill' href='#pills-home' role='tab' aria-controls='pills-home' aria-selected='true'>".
					  __("Settings" ).
					"</a>
				</li>
				<li class='nav-item'>
					<a class='nav-link' id='pills-news-tab' data-toggle='pill' href='#pills-news' role='tab' aria-controls='pills-news' aria-selected='true'>".
					  __("Publish in News" ).
					"</a>
				</li>
				<li class='nav-item'>
					<a class='nav-link' id='pills-profile-tab' data-toggle='pill' href='#pills-profile' role='tab' aria-controls='pills-profile' aria-selected='false'>".
					  __("Pupils", BIO).
					"</a>
				</li>
			</ul>
			<div class='tab-content col-12' id='pills-tabContent'>
				<div class='tab-pane fade show active' id='pills-home' role='tabpanel' aria-labelledby='pills-home-tab'>
					
					<div class='card'>
						  <div class='card-body'>
							<div class='row'>
								<form class='cab_edit' >
									<div class='spacer-10'></div>
									<subtitle class='col-12'>". __("Title", BIO). "</subtitle>
									<div class='col-12'>
										<input type='text' class='form-control' name='post_title' value='".$this->get("post_title")."' />
									</div>		
									<div class='spacer-10'></div>
									<subtitle class='col-12'>". __("Content", BIO). "</subtitle>
									<div class='col-12'>".
										$editor.
									"</div>	
									<div class='spacer-10'></div>
									
									<subtitle class='col-12'>". __("Icon", BIO). "</subtitle>
									<div class='col-12'>".
										get_input_file_form2( "group_icon", $icon_id, "trumbnail", "" ) .
										"<div class='bio_descr'>".
											__("Insert the Article icon. Maximum size - 400px", BIO) .
										"</div>	
									</div>	
									<div class='spacer-10'></div>
									
									<subtitle class='col-12'>". __("Course", BIO). "</subtitle>
									<div class='col-12'>".
										Bio_Course::wp_dropdown([
											"class" 	=> "form-control",
											"name"		=> "course_id",
											"selected"	=> $course	
										]).
									"</div>	
									<div class='spacer-10'></div>	
									
									<subtitle class='col-12'>". __("Includes", BIO). "</subtitle>
									<div class='col-12' style='display: flex;' >".
										get_input_file_form3( "include_id1", $include_id1, "include_id1", "" ) .
										get_input_file_form3( "include_id2", $include_id2, "include_id2", "" ) .
										get_input_file_form3( "include_id3", $include_id3, "include_id3", "" ) .
										get_input_file_form4( "include_id4", $include_id4, "include_id4", "" ) .
									"
										<div class='bio_descr'>".
											__("You may include for 3 files for download", BIO) .
										"</div>
									</div>	
									<div class='spacer-10'></div>	
									<div class='col-12'>
										<input type='submit' class='btn btn-primary' value='". __("Edit", BIO) . "' />		
									</div>		
								</form>
							</div>
						  </div>
					</div>
					
				</div>
				<div class='tab-pane fade' id='pills-news' role='tabpanel' aria-labelledby='pills-news-tab'>
					
					<div class='card'>
						<div id='collapseTwo' class='collapse show' aria-labelledby='headingTwo' data-parent='#accordion'>
							<div class='card-body'>".
								"Published in News".
							"</div>
						</div>
					</div>
					
				</div>
				<div class='tab-pane fade' id='pills-profile' role='tabpanel' aria-labelledby='pills-profile-tab'>
					
					<div class='card'>
						<div id='collapseTwo' class='collapse show' aria-labelledby='headingTwo' data-parent='#accordion'>
							<div class='card-body'>".
								static::users_box_func( $this->id, false ) . 
							"</div>
						</div>
					</div>
					
				</div>
			</div>
				
		</div>
		";
		return $html;
	}
}
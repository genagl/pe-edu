<?php


require_once(__DIR__ . "/jwt-session/jwt-session.class.php");

// $=-1, $=0, $='title', $='DESC', $="", $="all", $=

class Bio_REST
{
	static $token;
	static protected $memcache_obj;
	
	static function memcached()
	{
		if (!self::$memcache_obj)
		{
			self::$memcache_obj = new Memcache;
			self::$memcache_obj->connect('localhost', 11211);
		}
		return self::$memcache_obj;
	}
	
	static function auth_auth( )
	{
		$username 		= $_SERVER['PHP_AUTH_USER'];
		$password 		= $_SERVER['PHP_AUTH_PW'];
		$user = wp_signon([
			'user_login'    => $username,
			'user_password' => $password,
			'remember'      => true,
		], true);		
		if(!is_wp_error($user))
		{
			wp_set_current_user( $user->ID, $user->user_login );
			wp_set_auth_cookie( $user->ID );
			do_action( 'wp_login', $user->user_login );
			return $user;
		}
		else
		{
			return false;
		}
	}
	static public function set_session($user)
	{
		/*
		session_id(md5(microtime()));
		session_start();
		//$response->token	= session_id();
		$_SESSION["wordpress_user_id"] = $user->ID;
		$_SESSION["start_time"] = time();
		*/
		static::$token = JwtSession::sign($user->ID, JWT_KEY);
		setcookie("token", static::$token, time() + 3600, "/", "." . $_SERVER['SERVER_NAME']);
	
		wp_set_current_user( $user->ID, $user->user_login );
		wp_set_auth_cookie( $user->ID );
		do_action( 'wp_login', $user->user_login );
		session_write_close();
	}
	static function auth( $lgn, $psw )
	{
		$user = wp_signon([
			'user_login'    => $lgn,
			'user_password' => $psw,
			'remember'      => true,
		], true);
		
		if(!is_wp_error($user))
		{
			static::set_session($user);
			return $user;
		}
		else
		{
			return false;
		}
	}
	
	static function get_user_token($token)
	{
		/*
		if($token)
		{
			session_id($token);
			session_start();
		}
		$tm = 60 * 60 * 24 * 2;
		//$tm = 60;
		if (isset($_SESSION["wordpress_user_id"]) && isset($_SESSION["start_time"]) && time()-$_SESSION["start_time"] < $tm)
		{	
			session_write_close();
			return wp_set_current_user($_SESSION["wordpress_user_id"]);
		}
		else
		{
			session_write_close();
			//throw new ExceptionNotLoggedREST();
			return false;
		}
		*/
		try
		{
			if ($user_id = JwtSession::verify($token, JWT_KEY))
			{	
				return wp_set_current_user($user_id);
			}
			else
			{
				return false;
			}
		}
		catch (Exception $e)
		{
			return false;
		}
	}


    static function api_routing_right($recieve, $methods )
    {
        try
        {        
			$response	 		= static::api_routing( $recieve, $methods );
			return $response;        
        }
        catch(ExceptionNotAccessREST $ew)
        {
            $response 			= new StdClass;
            $response->msg		= sprintf( __("User not access rights: %s", BIO), $ew->getMessage() );
            $response->msg_type	= "access_error";
            return $response;
        }
        catch(ExceptionNotLoggedREST $ew)
        {
            $response 			= new StdClass;
            $response->msg 	= __("User not logged in", BIO);
            return $response;
        }
        catch(ExceptionNotAdminREST $ew)
        {
            $response 			= new StdClass;
            $response->msg = __("User not Admin", BIO);
            return $response;
        }
        catch(Exception $ex)
        {
            $response 			= new StdClass;
            $response->msg = __("Unknown error", BIO);
            $response->recieve = $recieve;
            $response->message = $ex->getMessage();
            $response->methods = $methods;
            return $response;
        }
        /**/
    }


	static function api_routing($recieve, $methods )
	{
		global $rest_log, $wpdb;
	    require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		$response 			= new StdClass;	
		$type				= $recieve->get_param('type');
		$code 				= $recieve->get_param('code');
		$pars 				= $recieve->get_param('pars');
        $request_headers	= getallheaders();
        $user				= static::get_user_token( $request_headers['token'] );
		$user_id			= $user->ID;
		
		if($user)
		{
			$account_activated 	= get_user_meta($user->ID, 'account_activated', true);
			if( !$account_activated )
			{
				//$user 		= static::get_user_token( "-1" );
				$response->alert=__("Your email address is not confirmed. You have been sent an email with a verification code. Read the message and follow the instructions.", BIO);
				//wp_logout();
			}
		}	
		$response->type		= $type;
		$response->user		= $user 
			? Bio_User::get_user($user->ID)
			: ["ID" => -1, "display_name"=> __("Unlogged User", BIO), "__typename" => "User" ];		
		
		//
		switch($type) 
		{
            case "init":
				global $bio_ts;
				$avatar;
				if($user)
				{					
					if( !$account_activated && false )
					{						
						//$user 				= static::get_user_token( "-1" );
						//$avatar 			= "/assets/img/user_icon.svg";
						//wp_logout();
					}
					else
					{
						$avatar = get_user_meta($user->ID, "avatar", true);
						$avatar = $avatar != "" ? get_bloginfo("url").$avatar : "/assets/img/user_icon.svg";
					}
				}
				else
				{
					$avatar = "/assets/img/user_icon.svg";
				}
				/*
                $response->user		= $user 
				
					? Bio_User::get_user($user_id)
					//["ID" => $user_id, "display_name"=> $user->display_name, "roles"=> Bio_User::get_roles(), "__typename" => "User", "account_activated" => $account_activated ? 1 : 0 ] 
					: ["ID" => -1, "display_name"=> __("Unlogged User", BIO), "__typename" => "User", "account_activated" => 0];
				*/
				$response->user['avatar'] = $avatar;
                $response->presets = [
                    "email"		=> Bio::$options['email'],
                    "adress"	=> Bio::$options['adress'],
                    "name"		=> get_bloginfo("name"),
                    "vk"		=> Bio::$options['vk'],
                    "youtube"	=> Bio::$options['youtube'],
                    "android"	=> Bio::$options['android'],
                    "apple"		=> Bio::$options['apple'],
                    "default_img"=> wp_get_attachment_url( Bio::$options['default_img'] ),
                    "404"		=> wp_get_attachment_url( Bio::$options['404'] ),
                ];	
				
                break;
            // Main page
            case "main_page":
                $news = get_categories([
                    'orderby'      => 'meta_value_num',
                    'order'        => 'ASC',
					"meta_key"	   => "order",
                    'hide_empty'   => false,
                    'hierarchical' => 0,
                    'exclude'      => '',
                    'number'       => 0,
                    'taxonomy'     => 'category',
					"meta_query"	=> [
						"relation"	=> "OR",
						[
							"key"	=> "order",
							"value"	=> 0,
							"compare" => ">"
						]
					]
                ]);
                $categories = [];
                foreach($news as $new)
                {
                    $cat = static::get_category($new);
                    $atricles 	= [];
                    $all 		= Bio_Article::get_all(
                        [], 		// []
                        4,  		// -1
                        0,  		// 0
                        "post_date", 		// 'title'
                        'DESC', 	// 'DESC'
                        "", 		// ""
                        "all", 		// $pars['fields'],
                        "AND",		// "AND",
                        -1,			// "",
                        ["category" => $new->term_id] // ["bio_course" => 5, "category" => 4]
                    );
                    foreach($all as $p)
                    {
                        $a 					= static::get_article( $p, $pars['is_full'] );
                        $atricles[]			= $a;
                    }
                    $cat['articles'] = $atricles;
                    $categories[] = $cat;
                }
                $response->categories		= $categories;
                $about = get_page(Bio::$options['about_id']);
                $response->about			= ["post_title" => $about->post_title, "post_content" => $about->post_content];
                break;
            case "auth":
                $recieve->lgn	= $pars['email'];
                $recieve->psw	= $pars['psw'];
                $user			= static::auth( $recieve->lgn, $recieve->psw );
                if($user)
                {
                    $response->user = ["ID" => $user->ID, "display_name"=> $user->display_name, "roles"=>Bio_User::get_roles(), "__typename" => "User" ];
                    $response->token= static::$token;//session_id();
                    $response->msg	= sprintf( __( " Successful logged in by %s", BIO ), $user->display_name );
                    $response->msg_type	= "login";
                }
                else
                {
                    $response->alert = __("User with this login and password no exists.", BIO);
                }
                break;
            case "role":
                $rules = [];
                switch($methods) {
                    case "update":
                        $response->update = 'error';
                        break;
                    case "delete":
                        $response->update = 'error';
                        break;
                    case "create":
                        $response->update = 'error';
                        break;
                    case "read":
                    default:
                        foreach( get_full_bio_roles() as $r)
                        {
                            $rules[] = $r[0];
                        }
                }

                $response->rules 	= $rules;
                break;
            case "left_menu":
                $left_menu = [];
                switch($methods) 
				{
                    case "update":
                        $response->msg = 'error';
                        break;
                    case "delete":
                        $response->msg = 'error';
                        break;
                    case "create":
                        $response->msg = 'error';
                        break;
                    case "read":
                    default:
                        $rules = $user->roles;
                        $left_menu_json =  json_decode( file_get_contents( BIO_REAL_PATH."temp/leftmenu_main.json" ) ); //static::get_menu();
						foreach ($left_menu_json as $menu_item)
						{
							$cap = array_intersect($rules, $menu_item->cap);
							if(count($cap) > 0 )
							{
								$lm 		= apply_filters("bio_left_menu_rest", $menu_item);
								$left_menu[]= $lm;
							}
						}
                }

                $response->cap = $cap;
                $response->rules = $rules;
                $response->left_menu = $left_menu;
                
                break;
		    case "get_search":
				global $wpdb;
				$s 			= $pars['s'];
                $articles	= [];
				$terms		= [];
                switch($methods) 
				{
                    case "update":                       
                    case "delete":                       
                    case "create":                        
                    case "read":
                    default:   
					
					  //articles
					  $query = "SELECT ID, post_title, post_content, post_author, post_date 
					  FROM `" . $wpdb->prefix . "posts` 
					  WHERE post_type='article' 					  
					  AND post_status ='publish' 
					  AND (
						LOCATE( '$s', post_title ) > 0 
						OR LOCATE( '$s', post_content ) > 0
					)";					  
					  $ts = $wpdb->get_results($query);
					  foreach($ts as $art)
					  {
							//if($art->stitle == 0 && $art->scontent == 0) continue;
							$t			= static::get_article( $art );
							$articles[] = $t;
					  } 
					  
					  //terms
                      $query2 = "SELECT " . $wpdb->prefix . "terms.term_id, " . $wpdb->prefix . "terms.name, tt.description, LOCATE('$s', " . $wpdb->prefix . "terms.name) AS sname, LOCATE('$s', tt.description) as sdescription, tt.taxonomy
					  FROM `" . $wpdb->prefix . "terms` LEFT JOIN " . $wpdb->prefix . "term_taxonomy AS tt ON tt.term_id=" . $wpdb->prefix . "terms.term_id 
					  WHERE tt.taxonomy IN('category','bio_course','bio_biology_theme','bio_olimpiad_type');";
					  $ts = $wpdb->get_results($query2);
					  foreach($ts as $term)
					  {
							if($term->sname == 0 && $term->sdescription == 0) continue;
							$t	= static::get_category( $term );
							$t["taxonomy"] = get_taxonomy($term->taxonomy);
							$terms[] = $t;
					  }
                }
                $response->articles		= $articles;
                $response->terms		= $terms;
                $response->search		= $query;
                break;
			case "page":
                $data = Bio_Page::api_action($type, $methods, $code, $pars, $user);
                $response->msg		= $data["msg"];
                $response->pages	= $data["pages"];
                $response->id		= $data["id"];
                break;
		    case BIO_ARTICLE_TYPE:
				global $rests_hook;
                $data = Bio_Article::api_action($type, $methods, $code, $pars, $user);
                $response->msg		= $data["msg"]; 
                $response->articles	= $data["articles"];
                $response->id		= $data["id"];
                $response->do		= $rests_hook;
                $response->count	= $data["count"];
                $response->numberposts	= $data["numberposts"];
                $response->offset	= $data["offset"];
                break;

            case BIO_ROLE_TAXONOMY_TYPE:
                /*
                    type - "bio_class"
                */
                $data = Bio_Role_Taxonomy::api_action($type, $methods, $code, $pars, $user);
                $response->msg		= $data["msg"];
                $response->bio_role_taxonomy	= $data["bio_role_taxonomy"];
                $response->id	= $data["id"];
                $response->arcticles	= $data["arcticles"];
                $response->update	= $data["update"];
                break;

            case BIO_CLASS_TYPE:
                /*
                    type - "bio_class"
                */
                $data = Bio_Class::api_action($type, $methods, $code, $pars, $user);
                $response->msg		= $data["msg"];
                $response->classes	= $data["classes"];
                $response->id	= $data["id"];
                $response->arcticles	= $data["arcticles"];
                $response->update	= $data["update"];
                break;

            case BIO_BIOLOGY_THEME_TYPE:
                /*
                    type - "bio_biology_theme"
                */

                $data = Bio_Biology_Theme::api_action($type, $methods, $code, $pars, $user);
                $response->msg		= $data["msg"];
                $response->bio_biology_theme	= $data["bio_biology_theme"];
                $response->id	= $data["id"];
                $response->arcticles	= $data["arcticles"];
                $response->update	= $data["update"];
                break;
            case BIO_OLIMPIAD_TYPE_TYPE:		
                /*
                    type - "bio_olimpiad_type"
                */
                $data = Bio_Olimpiad_Type::api_action($type, $methods, $code, $pars, $user);
                $response->bio_olimpiad_type	= $data["bio_olimpiad_type"];
                $response->id	= $data["id"];
                $response->articles	= $data["arcticles"];
                $response->update	= $data["update"];
                $response->msg		= $data["msg"];
                $response->msg_type	= $data["msg_type"];
                break;
            case BIO_TEST_CATEGORY_TYPE:		
                /*
                    type - "bio_olimpiad_type"
                */
                $data = Bio_Test_Category::api_action($type, $methods, $code, $pars, $user);
                $response->bio_test_category	= $data["bio_test_category"] ? $data["bio_test_category"] : [];
                $response->id	= $data["id"];
                $response->articles	= $data["arcticles"];
                $response->update	= $data["update"];
                $response->msg		= $data["msg"];
                $response->msg_type	= $data["msg_type"];
                break;
            case "category":
                $data = Bio_Category::api_action($type, $methods, $code, $pars, $user);
                $response->msg		= $data["msg"];
                $response->category	= $data["category"];
                $response->id		= $data["id"];
                $response->articles	= $data["articles"];
                $response->update	= $data["update"];
                $response->msg		= $data["msg"];
                $response->msg_type	= $data["msg_type"];
                break;
            case BIO_MAILING_GROUP_TYPE:
                $data = Bio_Mailing_Group::api_action($type, $methods, $code, $pars, $user);
               $response->mailing_groups	= $data["mailing_groups"];
                $response->id				= $data["id"];
                $response->msg				= $data["msg"];
                $response->msg_type			= $data["msg_type"];
                break;
            case BIO_MAILING_TYPE:
				Bio_Mailing::api_action($response, $methods, $code, $pars, $user);
                break;
            case BIO_EVENT_TYPE:
                $data =  Bio_Event::api_action($type, $methods, $code, $pars, $user, $response);
                $response->msg		= $data["msg"];
                $response->bio_event = $data[BIO_EVENT_TYPE];
                $response->id		= $data["id"];
                $response->msg_type	= $data["msg_type"];
                break;
            case BIO_TEST_TYPE:
				$data =  Bio_Test::api_action($type, $methods, $code, $pars, $user);
                $response->tests		= $data["tests"];
                $response->count		= $data["count"];
                $response->numberposts	= $data["numberposts"];
                $response->offset		= $data["offset"];
                $response->rest_log		= $rest_log;
                $response->msg			= $data["msg"];
                $response->msg_type		= $data["msg_type"];
                break;

            case "question":
                $data =  Bio_Question::api_action($type, $methods, $code, $pars, $user);
                $response->msg		= $data["msg"];
                $response->msg_type	= $data["msg_type"];
                break;
            case BIO_COURSE_TYPE:
                $data =  Bio_Course::api_action($type, $methods, $code, $pars, $user);
                $response->msg		= $data["msg"];
                $response->msg_type	= $data["msg_type"];
                $response->courses	= $data["courses"];
                $response->articles	= [];//$data["articles"];
				
                break;
            case "current_user":				
                $data =  Bio_Current_User::api_action($type, $methods, $code, $pars, $user);
                $response->users	= $data["users"];
                $response->user 	= $data["user"];
                $response->token 	= $data["token"];
                $response->msg		= $data["msg"];
                $response->msg_type	= $data["msg_type"];
                break;
            case "user":
				global $ggggg;
                $data = Bio_User::api_action($type, $methods, $code, $pars, $user);
                $response->users		= $data["users"];
                $response->count		= $data["count"];
                $response->numberposts	= $data["numberposts"];
                $response->offset		= $data["offset"];
                $response->do			= $ggggg;
                break;
            /*
            *
            *	PERSONAL CABINET FOR PUPILS
            *
            */
            //TODO current_user_favorite_article

            case "user_favorite_article":
                $data 				= Bio_Favorite_Article::api_action($type, $methods, $code, $pars, $user);
                $response->articles = $data["articles"];
                $response->is_favor = $data["is_favor"];
                $response->message	= $data["msg"];
                $response->id 		= $data["id"];
                break;

            case "user_course":
                $articles	= [];
                switch($methods) 
				{
                    case "update":

                        if(is_numeric($code)) {

                        }else{
                            $response->update = 'error';
                        }

                        break;
                    case "delete":
                        if(is_numeric($code)) {

                            if(!is_user_logged_in())	throw new ExceptionNotLoggedREST();
                            $course_id = $code;
                            $is = Bio_Course::withdraw_permission($course_id);
                            $response->msg = __($is ? "Succesful leave your request." : "Error!", BIO);
                            $response->is_req = $is;
                            $response->permission = Bio_Course::get_permission($course_id);

                            $response->update = 'error';
                        }else{
                            $response->update = 'error';
                        }
                        break;
                    case "create":
                        if(is_numeric($code)) {
                            if(!is_user_logged_in())	throw new ExceptionNotLoggedREST();
                            $course_id = $code;
                            $is = Bio_Course::request_permission($course_id);
                            $response->msg = __($is ? "Succesful send your request. Wait for answer from Course Leader." : "Error!", BIO);
                            $response->is_req = $is;
                            $response->permission = Bio_Course::get_permission($course_id);

                            $response->update = 'error';
                        }
						else
						{
                            $response->update = 'error';
                        }


                        break;
                    case "read":
                    default:
                        if(!$user_id) $user_id = $user->ID;
                        $courses	= Bio_User::get_courses( $user_id );
                        $crss		= [];
                        foreach($courses as $course)
                        {
							$cr 	= static::get_course( (int)$course['term_id'] );
							$cr['time']	= strtotime($course['date']);
							$cr['date']	= date_i18n( 'j F Y H:i', $cr['time'] );
                            $crss[] = $cr;
                        }
                        $response->courses = $crss;
                        $courses	= Bio_User::get_courses( $user_id, "course_user_requests" );
                        $req		= [];
                        foreach($courses as $course)
                        {
							$cr 	= static::get_course( (int)$course['term_id'] );
							$cr['time']	= strtotime($course['date']);
							$cr['date']	= date_i18n( 'j F Y H:i', $cr['time'] );
                            $req[]	= $cr;
                        }
                        $response->requests = $req;
                        break;
                }

                $response->articles = $articles;
                //$response->pars2		= Bio_Article::$args;
                break;
            case "user_event":
                $articles	= [];
                switch($methods) {
                    case "update":

                        if(is_numeric($code)) {

                        }else{
                            $response->update = 'error';
                        }

                        break;
                    case "delete":
                        if(is_numeric($code)) {
                            $response->update = 'error';
                        }else{
                            $response->update = 'error';
                        }
                        break;
                    case "create":
                        break;
                    case "read":
                    default:
                        $user_id 	= $code;
                        if(!$user_id) $user_id = $user->ID;
                        $events		= Bio_Event::get_all_mine( $user_id );
                        $requests	= [];
                        $usage		= [];
                        $completed	= [];
                        if(count( $events['requests'] ))
                            foreach($events['requests'] as $evt)
                            {
                                $ev				= static::get_event( $evt->ID );
                                $ev['date']		= $evt->date;
                                $ev['r'] 		= strtotime( $ev['date'] );
                                $ev['req_date'] = date_i18n( 'j F Y H:i', $ev['r'] );                               
                                $ev['_time'] 	= get_post_meta($evt->ID, "time", true);
                                $ev['time'] 	= date_i18n( 'j F Y', $ev['_time'] );
                                if( $ev['_time'] > time() )
                                    $requests[] 	= $ev;
                            }
                        if(count( $events['usage'] ))
						{
                            foreach($events['usage'] as $evt)
                            {
                                $ev				= static::get_event( $evt->ID );
                                $ev['date']		= $evt->date;
                                $ev['r'] 		= strtotime( $ev['date'] );
                                $ev['req_date'] = date_i18n( 'j F Y H:i', $ev['r'] );
                                $ev['_time'] 	= get_post_meta($evt->ID, "time", true);
                                $ev['time'] 	= date_i18n( 'j F Y', $ev['_time'] );
                                if( $ev['_time'] > time() )
                                    $usage[] 	= $ev;
                                else
                                    $completed[] = $ev;
                            }
						}
                        $response->requests 	= $requests;
                        $response->usage 		= $usage;
                        $response->bio_event 	= $completed;
                        //$response->events	 	= $events;
                        break;
                }

                $response->articles = $articles;
                //$response->pars2		= Bio_Article::$args;
                break;
            case "user_request":
                switch($methods) {
                    case "update":
                    case "delete":
                    case "create":
                        $response->update = 'error';
                        break;
                    case "read":
                    default:
                        $response->requests = [
                            "lead_courses" 	=> Bio_Course::get_requests_count(),
                            "lead_events"	=> Bio_Event::get_requests_count()
                        ];
                        break;
                }

                break;
			case "test_users":
				$query = "SELECT 
					DISTINCT (r.user_id),
					UNIX_TIMESTAMP(r.start_time) AS start_time, 
					UNIX_TIMESTAMP(r.end_time) AS end_time, 
					r.credits, 
					r.right_count, 
					r.test_id,  
					u.display_name,
					u.user_email
				FROM `bio_result` r
				LEFT JOIN {$wpdb->prefix}users u ON u.ID=r.user_id
				WHERE test_id={$code} AND r.user_id>0
				ORDER BY user_id, end_time DESC";
				$res = $wpdb->get_results($query);
				$response->users = $res;
				break;
            case "user_test":
				require_once BIO_REAL_PATH . "class/Bio_User_Test.class.php";
				$uid = $pars && $pars["id"] ? $pars["id"] : $user->ID;
				$response->data_tests = Bio_User_Test::api_action($type, $methods, $code, $pars, $uid);                
				//$response->msg = $pars;                         
                break;
            /*
            *
            *	PERSONAL CABINET FOR TEACHERS
            *
            */
            case "admin_permission":
                switch($methods) 
				{
                    case "update":

                        if(is_numeric($code)) 
						{
                            $id 		= $code;
                            $type 		= $pars['type'];
                            $user_id 	= $pars['user_id'];
                            switch($type)
                            {
                                case BIO_COURSE_TYPE:									
									$response->boo =  Bio_Course::accept_permission( $code, $user_id);
                                    $reqs		= Bio_Course::get_pupils_requests( $id );
                                    $pups		= Bio_Course::get_pupils( $id );
									$response->post = Bio_Course::get_course( $id, true, true );
                                    break;
                                case BIO_EVENT_TYPE:			
                                    $event		= Bio_Event::get_instance( $id );
									$form_data 	= serialize( $event->get_request($user_id) );
									$response->boo =  Bio_Event::set_access_request( $code, $user_id, ", form_data", ", '$form_data'" );
                                    $reqs		= $event->get_requests( $id );
                                    $pups		= $event->get_users( $id );
									$response->post = Bio_Event::get_event( $id, true, true );
                                    break;
                            }
							
							// TODO: выпилено из Event. Надо выпилить и из Course
                            $requests 	= [];
                            $pupils 	= [];
                            foreach($reqs as $req)
                            {
                                $requests[] = [
                                    "id"			=> $req->ID,
                                    "display_name"	=> $req->display_name,
                                    "user_email"	=> $req->user_email
                                ];
                            }
                            foreach($pups as $pup)
                            {
                                $pupils[] = [
                                    "id"			=> $pup->ID,
                                    "display_name"	=> "".$pup->display_name,
                                    "user_email"	=> $pup->user_email
                                ];
                            }
                            $response->type 		= $type;
                            $response->id 			= $id;
                            $response->user_id 		= $user_id;
                            $response->requests 	= $requests;
                            $response->pupils 		= $pupils;
							$response->msg 			= __("Users are added to Members group", BIO);
                        }
						else
						{
                            $response->msg = 'error';
                        }

                        break;
                    case "delete":
                        if(is_numeric($code)) {
                            $type 		= $pars['type'];
                            $user_id 	= $pars['user_id'];
                            $comment 	= $pars['comment'];
                            $response->type 	= $type;
                            $response->id 		= $code;
                            $response->user_id 	= $user_id;
                            switch($type)
                            {
                                case BIO_COURSE_TYPE:
                                    Bio_Course::refuse_permission( $code, $user_id, $comment);
                                    $reqs			= Bio_Course::get_pupils_requests( $id );
									$response->post = Bio_Course::get_course( $code, true, true );
                                    break;
                                case BIO_EVENT_TYPE:
									$response->msg  = Bio_Event::refuse_permission($code, $user_id, "refuse of organizers");
                                    $event			= Bio_Event::get_instance( $code );
                                    $reqs			= $event->get_requests( $code );
									$response->post = Bio_Event::get_event( $code, true, true );
                                    break;
                            }

                            $requests 	= [];
                            foreach($reqs as $req)
                            {
                                $requests[] = [
                                    "id"			=> $req->ID,
                                    "display_name"	=> 'goo '.$req->display_name,
                                    "user_email"	=> $req->user_email
                                ];
                            }
                            $response->comment 		= $comment;
                            $response->requests 	= $requests;

                            break;
                        }
						else
						{
                            $response->update = 'error';
                        }
                        break;
                    case "create":
                        break;
                    case "read":
                    default:
                        break;
                }

//                $response->articles = $articles;
                $response->articles = [];
                //$response->pars2		= Bio_Article::$args;
                break;

            case "admin_user_course":
                $articles	= [];
                switch($methods) {
                    case "update":

                        if(is_numeric($code)) {

                        }else{
                            $response->update = 'error';
                        }

                        break;
                    case "delete":
                        if(is_numeric($code)) {
                            $response->update = 'error';
                        }else{
                            $response->update = 'error';
                        }
                        break;
                    case "create":
                        break;
                    case "read":
                    default:
                        if(is_numeric($code)) {
                            $p			= static::get_course( $code );
                            $requests 	= Bio_Course::get_pupils_requests( $code );
                            $us			= [];
                            foreach($requests as $u )
                            {
                                $user = get_user_by("id", $u->ID);
                                $us[] = [
                                    "id"			=> $u->ID,
                                    "display_name"	=> $user->display_name,
                                    "user_email"	=> $user->user_email
                                ];
                            }

                            $users 			= Bio_Course::get_pupils( $code );
                            $us2			= [];
                            foreach($users as $u )
                            {
                                $user = get_user_by("id", $u->ID);
                                $us2[] = [
                                    "id"			=> $u->ID,
                                    "display_name"	=> $user->display_name,
                                    "user_email"	=> $user->user_email
                                ];
                            }
                            $p['requests'] 		= $us;
                            $p['users'] 		= $us2;
                            $response->course 	= $p;
                            $response->update = 'error';
                        }else{
                            if(!is_user_logged_in())	throw new ExceptionNotLoggedREST();
                            $all = Bio_Course::get_all_per_author( $user->ID );
                            $selus 	= Bio_User::get_users_per_courses( $user->ID );
                            $a = new StdClass;
                            $a->ID = -1;
                            $a->post_title = __("add Course", BIO);
                            $a->count = count($all);

                            $courses = [];
                            foreach($all as $aa)
                            {
                                $crs 	= static::get_course( $aa->term_id );
                                $crs['req']= count($selus[ $aa->term_id ]['users']);
                                $courses[] =  $crs;
                            }
                            $courses[] =  $a;
                            $response->courses = $courses;

                            $response->update = 'error';
                        }

                        break;
                }

                $response->articles = $articles;
                //$response->pars2		= Bio_Article::$args;
                break;
            case "admin_user_event":
                $articles	= [];
                switch($methods) {
                    case "update":

                        if (is_numeric($code)) {

                        } else {
                            $response->update = 'error';
                        }

                        break;
                    case "delete":
                        if (is_numeric($code)) {
                            $response->update = 'error';
                        } else {
                            $response->update = 'error';
                        }
                        break;
                    case "create":
                        break;
                    case "read":
                    default:
                        if (is_numeric($code)) {
                            $pst = static::get_event($code);
                            $p = Bio_Event::get_instance($code);
                            if (!$p->is_user_author()) throw new ExceptionNotLoggedREST();
                            $users = $p->get_requests();
                            $us = [];
                            foreach ($users as $u) {
                                $user = get_user_by("id", $u->ID);
                                $us[] = [
                                    "id" => $u->ID,
                                    "display_name" => $user->display_name,
                                    "user_email" => $user->user_email
                                ];
                            }
                            $users2 = $p->get_users();
                            $us2 = [];
                            foreach ($users2 as $u) {
                                $user = get_user_by("id", $u->ID);
                                $us2[] = [
                                    "id" => $u->ID,
                                    "display_name" => $user->display_name,
                                    "user_email" => $user->user_email
                                ];
                            }
                            $pst['requests'] = $us;
                            $pst['users'] = $us2;
                            $response->event = $pst;
                            $response->update = 'error';
                        } else {
                            if (!is_user_logged_in()) throw new ExceptionNotLoggedREST();
                            $all = Bio_Event::get_all([], -1, 0, 'title', 'DESC', "", "all", "AND", $user->ID);
                            $selus = Bio_Event::get_users_per_post($user->ID);
                            $a = new StdClass;
                            $a->ID = -1;
                            $a->post_title = __("add Event", BIO);
                            $events = [];
                            foreach ($all as $aa) {
                                $eev = static::get_lead_article($aa);
                                $eev['req'] = $selus[$eev['id']][0];
                                $events[] = $eev;
                            }
                            $events[] = $a;
                            $response->selus = $selus;
                            $response->events = $events;

                            $response->update = 'error';
                        }
                }
                break;


            case "test_result":
                $articles = [];
                switch ($methods) {
                    case "update":

                        if (is_numeric($code)) {

                        } else {
                            $response->update = 'error';
                        }

                        break;
                    case "delete":
                        if (is_numeric($code)) {
                            $response->update = 'error';
                        } else {
                            $response->update = 'error';
                        }
                        break;
                    case "create":
                        if (is_numeric($code)) 
						{

                            $test_id 	= $code;
                            if(!$pars)
                            {
                                $response->results	= "No parameters";
                                break;
                            }
                            $start_time	= $pars['start_time'];
                            $end_time	= $pars['end_time'];
                            $result		= $pars['result'];
                            $results	= Bio_Test_API::add_result($test_id, $user->ID, $start_time, $end_time, $result);
                            $response->results	= $results;
                        } else {
                            $response->msg = 'error';
                        }
                        break;
                    case "read":
                    default:
                        if (is_numeric($code)) 
						{

                        } 
						else 
						{

                        }
                }

                break;


            case "user_class_change":
                $user_id			= $code;
                $class_id 			= $pars['class_id'];
                update_usermeta($user_id, "studentclass", $class_id);
                $response->_class 	= $class_id;
                break;
            case "user_rule_change":
                $user_id			= $code;
                $_u					= get_user_by("id", $user_id);
                $role_id			= $pars['role_id'];
                $is_check			= $pars['is_check'];
                if($is_check)
                {
                    $_u->add_role($role_id);
                }
                else
                {
                    $_u->remove_role($role_id);
                }
                $response->is_check	= $is_check;
                $response->rules 	= $_u->roles;
                $response->user_id 	= $user_id;
                break;
            case "user_rules_change":
                $user_id			= $code;
                $roles				= (array)$pars['roles'];
                $_u					= get_user_by("id", $user_id);
				$_a	= [];
				
				foreach(get_full_bio_roles() as $role)
				{					
					$_u->remove_role($role[0]);
				}
				$ur = [];
				foreach($roles as $role)
				{
					$_u->add_role($role);
					$ur[] = __($role, BIO);
				}
				$letter_text		= sprintf( __(BIO_USER_CHANGE_ROLES, BIO), implode(", ", $ur));
				$response->do	 	= Bio_Mailing::send_mail(
					__(BIO_USER_CHANGE_ROLES_TILE, BIO), 
					$letter_text, 
					-1, 
					[$_u->user_email ]
				);
				/**/
                $response->user_id 	= $user_id;
                $response->roles 	= $_u->roles;
                $response->all	 	= $roles;//get_full_bio_roles();
				
                break;
			case "bio_course_members":
				$response->members = Bio_Course::get_pupils( $code );
				$response->id = $code;
				break;
			case "leave_course":
				Bio_Course::withdraw_permission($code);
				$response->permission = Bio_Course::get_permission($code);
				break;
			case "find_email":
				$response->email = Bio_User::find_user_email($pars['search']);
				break;
			case "check_email":
				require_once ABSPATH . WPINC .'/registration.php';
				$response->email = email_exists($pars['search']);
				break;
			case "find_questions":
				$response->questions = Bio_Test::search_question($pars['search']);
				break;
			case "bio_test_question":
				switch($methods)
				{
					case "create":
						if($code)
						{
							// TODO update question
							$questions 				= Bio_Test::get_single_question($code);
							$questions['answers'] 	= Bio_Test::get_answers_single_question($code);
							$response->questions 	= [ $questions ];
							$response->msg			= __("This REST method UPDATE not works now.", BIO);
						}
						else
						{
							// TODO insert question
							$response->msg			= __("This REST method CREATE not works now.", BIO);
						}
						break;
					case "delete":
						
						break;
					default:
						if($code)
						{
							$questions 				= Bio_Test::get_single_question($code);
							$questions['answers'] 	= Bio_Test::get_answers_single_question($code);
							$response->rest_log 	= $rest_log;
							$response->questions 	= $questions;
						}
						else if($pars['id'])
						{
							$questions 				= Bio_Test::get_single_question($pars['id']);
							$questions['answers'] 	= Bio_Test::get_answers_single_question($pars['id']);
							$response->rest_log 	= $rest_log;
							$response->questions 	= [ $questions ];
						}
						else
						{
							$pars['numberposts']	= !$pars['numberposts'] ? 20 : $pars['numberposts'];
							$pars['offset']			= (int)$pars['offset'];
							$pars['test_id']		= (int)$pars['test_id'];						
							$pars['bio_biology_theme'] = (int)$pars['bio_biology_theme'];						
							$questions 				= Bio_Test::get_all_questions( $pars );
							$response->rest_log 	= $rest_log;
							//$questions['answers'] = Bio_Test::get_answers_single_question($pars['id']);
							$response->numberposts 	= $pars['numberposts'];
							$response->offset 		= $pars['offset'];
							$response->count 		= count(Bio_Test::get_all_questions([
								"numberposts"	=> 100000000000,
								"offset"		=> 0,
								"test_id"		=> (int)$pars['test_id'],
								"bio_biology_theme" => (int)$pars['bio_biology_theme'],
								"type"			=> $pars['type'],
								"is_errors"		=> $pars['is_errors']
							]));
							$response->questions 	= $questions;
							$response->type 		= $pars['type'];
						}
				}
				break;
            case "update_menu":
                $menu = $pars['menu'];
                if(!is_array($menu))	throw new Exception("menu must be array");
                $new_menu = [];
                foreach($menu as $m)
                {
                    $nmenu = [];
                    $nmenu['uniq']			= $m['uniq'];
                    if( $m['icon_id'] < 1 )
                    {
                        $media = Bio_Assistants::insert_media([ "data" => $m['icon'], "media_name"=> $m['media_name']]);
                        wp_set_object_terms( $media['id'], Bio::$options['icon_media_term'], BIO_MEDIA_TAXONOMY_TYPE );
						$nmenu['icon_id']	= $media['id'];
                        $nmenu['icon']		= $media['url'];
                    }
                    else
                    {
                        $nmenu['icon_id'] 	= $m['icon_id'];
                        $nmenu['icon'] 		= $m['icon'];
                    }
                    $nmenu['is_check']		= $m['is_check'];
                    $nmenu['label']			= $m['label'];
                    $nmenu['component']		= $m['component'];
                    $nmenu['is_archive']	= $m['is_archive'];
                    $nmenu['params']		= $m['params'];
                    $nmenu['is_parent']		= $m['is_parent'];
                    $nmenu['parent_id']		= $m['parent_id'];
                    $nmenu['submenu_dbld']	= $m['submenu_dbld'];
                    $nmenu['submenu_label']	= $m['submenu_label'];
                    $new_menu[]		 		= $nmenu;
                }
                file_put_contents( ABSPATH."static/json/menu.json", json_encode( $new_menu ) );
                $response->menu = $new_menu;
				$response->msg  = __("Successful update main menu.", BIO);
                break;
            case "menu":
                $response->menu = json_decode(file_get_contents(ABSPATH."static/json/menu.json")); //static::get_menu();
                break;
            case "update_test_users_result":
                $response->msg = "This REST method not work now";
                break;
            case "update_test_users_result_xls":			
				require_once( BIO_REAL_PATH . "lib/save_event.php" );
				$test = Bio_Test::get_instance( $pars['test_id'] );
				$response->file3 = [
					$pars["user_id"][5]['user_email'],
					$pars["user_id"][5]['display_name'],
					$pars["user_id"][5]['credits'],
					$pars["user_id"][5]['duration'],
					$pars["user_id"][5]['right_count'],
				];
				$response->file = save_event( 
					wp_strip_all_tags( $test->get( "post_title" ) ),
					$pars["user_id"]
				);
                break;
            case "update_test_users_xls":				
				require_once( BIO_REAL_PATH . "lib/load_event.php" );
				$response->members = load_event([
					'data'			=> $pars['test_url'],
					'media_name'	=> $pars['test_name']
				], $pars['event_id']);
                break;
			case "export_users_list":
				Bio_User::access_caps(BIO_USER_DELETE, "Delete user");			
				require_once( BIO_REAL_PATH . "lib/save_users.php" );
				$response->users = save_users( __("User List", BIO) );
				break;
            case "sort_members":
				switch($pars['post_type'])
				{
					case BIO_EVENT_TYPE:
					default:
						$event 		= Bio_Event::get_instance( $pars['post_id'] );
						$response->members = $event->get_users( 
							$pars["sort_method"], 
							[ "test_id" => $pars["test_sort"]] 
						);
						$response->requests = $event->get_requests( 
							$pars["sort_method"], 
							[ "test_id" => $pars["test_sort"]] 
						);
						$test 		= Bio_Test::get_instance($pars["test_sort"]);
						$response->test_sort = $test->get("post_title");
						$response->sort_method = $pars["sort_method"];
						break;
					
				}
                //$response->msg = "This REST method not work now";
                break;
            case "menu_component":
                $comp_id 	= $code;
                $list 		= [];
                switch($comp_id)
                {
                    case BIO_COURSE_TYPE:
                        $t	= Bio_Course::get_all();
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->term_id,
                                "post_title" => $o->name,
                                "icon" => wp_get_attachment_image_src(get_term_meta( $o->term_id, "icon", true), "full")[0]
                            ];
                        }
                        break;
                    case BIO_ARTICLE_TYPE:
                        $t	= Bio_Article::get_all();
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->ID,
                                "post_title" => $o->post_title
                            ];
                        }
                        break;
                    case BIO_OLIMPIAD_TYPE_TYPE:
                        $t	= Bio_Olimpiad_Type::get_all();
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->term_id,
                                "post_title" => $o->name,
                                "icon" => wp_get_attachment_image_src(get_term_meta( $o->term_id, "icon", true), "full")[0]
                            ];
                        }
                        break;
                    case BIO_BIOLOGY_THEME_TYPE:
                        $t	= Bio_Biology_Theme::get_all();
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->term_id,
                                "post_title" => $o->name,
                                "icon" => wp_get_attachment_image_src(get_term_meta( $o->term_id, "icon", true), "full")[0]
                            ];
                        }
                        break;
                    case BIO_TEST_CATEGORY_TYPE:
                        $t	= Bio_Test_Category::get_all();
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->term_id,
                                "post_title" => $o->name,
                                "icon" => wp_get_attachment_image_src(get_term_meta( $o->term_id, "icon", true), "full")[0]
                            ];
                        }
                        break;
                    case BIO_ROLE_TAXONOMY_TYPE:
                        $t	= Bio_Role_Taxonomy::get_all();
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->term_id,
                                "post_title" => $o->name,
                                "icon" => wp_get_attachment_image_src(get_term_meta( $o->term_id, "icon", true), "full")[0]
                            ];
                        }
                        break;
                    case "category":
                        $terms = get_terms( array(
                            'taxonomy'      => "category",
                            'orderby'       => 'name',
                            'order'         => 'ASC',
                            'hide_empty'    => false,
                            'fields'        => 'all',
                        ) );
                        $t	= $terms;
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->term_id,
                                "post_title" => $o->name
                            ];
                        }
                        break;
                    case "page":
                        $t	= get_posts([
                            "post_type"	=>"page",
                            "post_status" => "publish",
                            "numberposts" => -1
                        ]);
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->ID,
                                "post_title" => $o->post_title
                            ];
                        }
                        break;
                    case BIO_EVENT_TYPE:
                        $t	= Bio_Event::get_all();
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->ID,
                                "post_title" => $o->post_title
                            ];
                        }
                        break;
                    case BIO_TEST_TYPE:
                        $t	= Bio_Test::get_all();
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->ID,
                                "post_title" => $o->post_title
                            ];
                        }
                        break;
                    case BIO_ARTICLE_TYPE:
                    default:
                        $t	= Bio_Article::get_all();
                        foreach($t as $o)
                        {
                            $list[] = [
                                "id" => $o->ID,
                                "post_title" => $o->post_title
                            ];
                        }
                        break;
                }
                $response->list 		= $list;
                break;

			
            case "send_error_test":
				$test			= Bio_Test::get_instance($pars['test_id']);
				switch($pars['method'])
				{
					case "create":
						$response->do = $test->send_error_test($pars);
						$response->msg = __("Success sending comment. Thank you.", BIO);
						break;
					case "remove":
						$response->question_id = $pars['question_id'];
						if($pars['is_accept']) { }
						$response->do = $test->remove_error_test($pars['error_id']);
						$response->msg = __("Successfully removed Error content.", BIO);
						break;
				}
				break;
            case "send_test_message":
				$question			= Bio_Test::get_single_question( $code );
				$data = [
					"post_content"	=> sprintf(
						__("User <b>%s</b> send message: <p><B>%s</B>.</p><p>Edit this question: %s", BIO), 
						wp_get_current_user()->display_name, 
						(string)$pars['text'],
						$pars['url']
					),
					"post_title"	=> sprintf(__("Comment about test Question '%s'.", BIO), $question[0]->questiontext), 
					"started"		=> time(),					
				];
				$uss = Bio_User::get_all2( [ "role__in" => [ "administrator", "editor", "TestEditor" ] ] );
				$mailing = Bio_Mailing::insert($data, $uss);
                $response->msg = sprintf(__("Comment about test Question \"%s\" sended to Test Editors succesfully.", BIO), $question[0]->questiontext);
                break;
			
            case "api_test":
                $response->msg = "API work success";
                $response->msg_type	= "error_login";
                break;
			
            case "request_article_cat":
                $response->msg = "request_article_cat ". $methods;
				switch($pars['method'])
				{
					case "update":
						$req_id 		= (int)$pars['req_id'];
						$article		= Bio_Article::get_instance( $pars['article_id'] );
						$response->do 	= $article->access_req_cat($req_id);
						Bio_Article::clear_instance( $pars['article_id'] );
						$response->article = Bio_Article::get_article( $pars['article_id'], true );
						break;
					case "delete":
						$req_id 		= (int)$pars['req_id'];
						$article		= Bio_Article::get_instance( $pars['article_id'] );
						$response->do 	= $article->delete_req_cat($req_id);
						Bio_Article::clear_instance( $pars['article_id'] );
						$response->article = Bio_Article::get_article( $pars['article_id'], true );
				}
                break;
			
            case "request_verify":
				Bio_Current_User::request_verify( $pars );
                $response->msg = "The mailbox, which you indicated in your profile, was sent an instruction with verification of your account. Follow it to gain access to the full functionality.";
                break;
			
            case "verify_user":
                $response->user_verified = (int)$code;
				$pattern			= $pars['pattern'];
				$activation_code	= get_user_meta((int)$code, 'activation_code', $true);
				if($activation_code[0] == $pattern || $activation_code[0] == "")
				{
					update_user_meta((int)$code, 'account_activated', 1);
					$success = 1;
				}
				else
				{
					$success = 0;
				}
                $response->success	= $success;
                $response->current_user_id	= $user_id;
                $response->look		= [$activation_code[0], $pattern];
                break;
				
            case "bio_event_request":
				global $rest_log1;
                /*

                    type	: "bio_event_request",
                    pars	: {
                        event_id	: <int>,
                        email		: <email>,
                        user		: <string>
						form_data	: <array>
                    }
                */
                $data						= $pars;
                $event						= Bio_Event::get_instance($data["event_id"]);
				switch ($pars['method'])
				{
					case "read":
						$response->event_request		= $event->get_request($code);
						break;
					case "update":
						$new_user = $event->send_request($data);
						if( $new_user )
						{
							$response->token			= static::$token;
						}
						$response->is_user_requested 	= 1;
						$response->msg			 		= __( $response->is_user_requested ? "Successful. Your request is register. Wait for Event owners send access to you.": "Error", BIO);
						$response->do			 		= $rest_log1;
						break;
						
                }
				break;
			case "do_add_over_course":
				if( is_array($pars["user_id"] ))
				{
					$user_list = [];
					$course = get_term( $pars['course_id'], Bio_Course::get_type() );
					foreach($pars["user_id"] as $uid)
					{
						Bio_Course::accept_permission( $pars['course_id'], $uid );
						$user_list[] 		= get_user_by("id", $uid )->display_name;
					}
					$response->msg 		= sprintf(
						__("%s now members of %s.", BIO), 
						implode(", ", $user_list), 
						$course->name
					);
				}
				else
				{
					$response->msg 		= __("No users select!",Bio);
				}
				break;
			case "do_password_restore":
				if( !email_exists($pars['email']) )
				{
					$response->msg = Bio_Messages::send_email_no_exists( $pars['email'] );
					break;
				}
				if( !is_email( $pars['email'] ) )
				{
					$response->msg = Bio_Messages::send_no_email( $pars['email'] );
					break;
				}
				$u = get_user_by("email", $pars['email']);
				$uniq = MD5( $pars['email'] . time());
				update_user_meta($u->ID, "restore_password", $uniq);
				$response->do = Bio_Mailing::send_mail(
					BIO_RESTORE_PASSWORD_TITLE, 
					sprintf(
						"Go to <a href='%s'>link</a> to restore your password.",
						$pars['url'] . "/?cache=" . $uniq ."&user=".$u->display_name
					), 
					$author, 
					[ $pars['email'] ]
				);
				$response->msg = __("We send your instructions to email. ", BIO);
				break;
			case "do_password_restore_final":
				require_once ABSPATH . WPINC .'/registration.php';	
				$users = get_users([
					"meta_query" => [
						'relation' => 'OR',
						[
							"key" 	=> "restore_password",
							"value"	=> $pars["uniq"]
						]
					]
				]);
				if(count($users) == 0)
				{
					$response->msg = __("No users with this cache. Goto to authantification page and restore your password anover", BIO);
				}
				else if( count($users) > 1)
				{
					$response->msg = __("More Users has this chache. Goto to authantification page and restore your password anover", BIO);
				}
				else
				{
					$response->do = wp_update_user([
						"ID"		=> $users[0]->ID,
						"user_pass"	=> $pars['psw']
					]);
					if( Bio_User::is_user_role( "HiddenUser", $users[0]->ID) )
					{
						$users[0]->add_role("contributor");
						$users[0]->remove_role("HiddenUser");
					}
					$response->msg = __("Password changed successfully.", BIO);
				}
				break;
			case "do_add_over_event":
				if( is_array($pars["user_id"] ))
				{
					$user_list = [];
					foreach($pars["user_id"] as $uid)
					{
						Bio_Event::set_access_request( $pars['event_id'], $uid );
						$event				= Bio_Event::get_instance($pars['event_id']);
						$over 				= $event->get_my_over();
						$byUser				= Bio_Event::get_by_user( $uid );
						$response->events 	= [];
						$response->over 	= $byUser;
						foreach($over as $e)
						{
							if( !in_array($e->ID, $byUser))
								$response->events[] = static::get_event($e);
						}
						$user_list[] 		= get_user_by("id", $uid )->display_name;
					}
					$response->msg 		= sprintf(
						__("%s now members of %s.", BIO), 
						implode(", ", $user_list), 
						$event->get("post_title")
					);
				}
				else
				{
					$response->msg 		= __("No users select!",Bio);
				}
				break;
            //TODO разделение старого и нового API, убрать после рефакторинга.

			default:				
				break;
		}

		$response->pars		= $recieve->get_params();
		return $response;
	}
	



	static function get_page( $p )
	{
		$art				= is_numeric($p) ? get_post($p) : $p;
		$a					= [];
		$a['id']			= $art->ID;
		$a['post_title']	= $art->post_title;
		$a['post_content']	= $art->post_content;
		$a['post_date']		= date_i18n( 'j F Y', strtotime( $art->post_date ) );
		$thumbnail			= get_the_post_thumbnail_url( $art->ID, "full" );
		$a['thumbnail']		= $thumbnail ? $thumbnail : BIO_EMPTY_IMG;
		
		// Categories
		
		$cats				= [];
		$categs	= get_the_terms($art->ID, "category");
		if(count($categs) && $categs && !is_wp_error($categs))
		{
			foreach($categs as $categ)
			{
				$cats[]			= static::get_category( $categ );
			}
		}
		$a["category"]		= $cats;
		/**/
		// author
		$user				= get_user_by("id", $art->post_author);
		$auth				= [];
		$auth["id"]			= $user->ID;
		$auth["display_name"]= $user->display_name;
		$a['post_author']	= $auth;
		
		return $a;
	}

	static function get_article( $p, $is_full = false )
	{
		$art				= Bio_Article::get_instance($p);
		$a					= [];
		$a['id']			= (int)$art->id;
		$a['post_title']	= $art->get("post_title");
		$a['post_content']	= $is_full ? 
			$art->get("post_content") : 
			wp_strip_all_tags(wp_trim_words($art->get("post_content"), 20));
		$a['post_date']		= date_i18n( 'j F Y', strtotime( $art->get("post_date") ) );
		$thumbnail			= get_the_post_thumbnail_url( $art->id, "full" );
		$a['thumbnail']		= $thumbnail ? $thumbnail : BIO_EMPTY_IMG;
		$a['thumbnail_id']	= get_post_thumbnail_id( $art->id );
		
		// Тест
		$t					= [];
		if($test_id = $art->get_meta("test_id") > 0)
		{
			$test			= Bio_Test::get_instance($test_id);
			$t['id']		= $test->id;
			$t['post_title']= $test->get("post_title");
		}
		$a['test']			= $t;
		
		// Вложения
		$a['includes']		= [];
		for( $i = 1; $i < 4; $i++ )
		{
			$inc			= [];
			$inc_id			= $art->get_meta("include_id$i");
			if($inc_url = wp_get_attachment_url( $inc_id ))
			{
				$inc['id']	= $inc_id;
				$inc['url']	= $inc_url;
			}
			$a['includes'][] = $inc;
		}
		
		// Курс
		$course				= [];
		$cours	= get_the_terms($art->id, BIO_COURSE_TYPE);
		$cour	= $cours[0];
		if($cour)
		{
			$course = static::get_course($cour);			
		}
		$a["course"]		= $course;
		
		// Categories
		
		$cats				= [];
		$categs	= get_the_terms($art->id, "category");
		if(count($categs) && $categs && !is_wp_error($categs))
		{
			foreach($categs as $categ)
			{
				$cats[]			= static::get_category( $categ );
			}
		}
		$a["category"]		= $cats;
		
		// Olimpiad type		
		$ot				= [];
		$categs	= get_the_terms($art->id, BIO_OLIMPIAD_TYPE_TYPE);
		if(count($categs) && $categs && !is_wp_error($categs))
		{
			foreach($categs as $categ)
			{
				$ot[]			= static::get_category( $categ );
			}
		}
		$a[BIO_OLIMPIAD_TYPE_TYPE]		= $ot;
		
		// BIO_BIOLOGY_THEME_TYPE		
		$bot				= [];
		$categs	= get_the_terms($art->id, BIO_BIOLOGY_THEME_TYPE);
		if(count($categs) && $categs && !is_wp_error($categs))
		{
			foreach($categs as $categ)
			{
				$bot[]			= static::get_category( $categ );
			}
		}
		$a[BIO_BIOLOGY_THEME_TYPE]		= $bot;
		
		// BIO_CLASS_TYPE		
		$cls				= [];
		$categs	= get_the_terms($art->id, BIO_CLASS_TYPE);
		if(count($categs) && $categs && !is_wp_error($categs))
		{
			foreach($categs as $categ)
			{
				$cls[]			= static::get_category( $categ );
			}
		}
		$a[BIO_CLASS_TYPE]		= $cls;
		
		/**/
		// author
		$user				= get_user_by("id", $art->get("post_author"));
		$auth				= [];
		$auth["id"]			= $user->ID;
		$auth["display_name"]= $user->display_name;
		$a['post_author']	= $auth;
		
		//event
		$event_id			= $art->get_meta("event_id");
		if($event_id)
		{
			$a['event']		= static::get_event( $event_id );
		}
		//is_favorite
		/*
			TODO - много запросов к базе
		*/
		$a['is_favor']		= $art->is_favorite() ? 1 : 0;
		return $a;
	}

	static function get_mailing( $p )
	{
		$art				= Bio_Mailing::get_instance($p);
		$a					= [];
		$a['id']			= $art->id;
		$a['post_title']	= $art->get("post_title");
		$a['post_content']	= $art->get("post_content");
		$a['post_date']		= date_i18n( 'j F Y', strtotime( $art->get("post_date") ) );
		$thumbnail			= get_the_post_thumbnail_url( $art->id, "full" );
		$a['thumbnail']		= $thumbnail ? $thumbnail : BIO_EMPTY_IMG;
		
		// Тест
		$t					= [];
		if($test_id = $art->get_meta("test_id") > 0)
		{
			$test			= Bio_Test::get_instance($test_id);
			$t['id']		= $test->id;
			$t['post_title']= $test->get("post_title");
		}
		$a['test']			= $t;
				
		// Курс
		$course				= [];
		$cours	= get_the_terms($art->id, BIO_COURSE_TYPE);
		$cour	= $cours[0];
		if($cour)
		{
			$course = static::get_course($cour);			
		}
		$a["course"]		= $course;
		
		// Categories
		
		$cats				= [];
		$categs	= get_the_terms($art->id, "category");
		if(count($categs) && $categs && !is_wp_error($categs))
		{
			foreach($categs as $categ)
			{
				$cats[]			= static::get_category( $categ );
			}
		}
		$a["category"]		= $cats;
		/**/
		// author
		$user				= get_user_by("id", $art->get("post_author"));
		$auth				= [];
		$auth["id"]			= $user->ID;
		$auth["display_name"]= $user->display_name;
		$a['post_author']	= $auth;
		return $a;
	}
	static function get_lead_article( $p )
	{
		$art				= Bio_Article::get_instance($p);
		$a					= [];
		$a['id']			= $art->id;
		$a['post_title']	= $art->get("post_title");
		return $a;
	}
	
	static function get_event( $p )
	{
		$art				= Bio_Event::get_instance($p);
		$a					= [];
		$a['id']			= $art->id;
		$a['post_title']	= $art->get("post_title");
		$a['post_content']	= $art->get("post_content");
		$time				= $art->get_meta("time");
		$a['_time']			= strtotime( $time );
		$a['time']			= date_i18n('j F Y H:i', strtotime( $time ));
		$thumbnail			= get_the_post_thumbnail_url( $art->id, "full" );
		$a['thumbnail']		= $thumbnail ? $thumbnail : BIO_EMPTY_IMG;
		// Тест
		$t					= [];
		$test_id = $art->get_meta("test_id");
		if( $test_id > 0)
		{
			$test			= Bio_Test::get_instance($test_id);
			$t['id']		= $test->id;
			$t['post_title']= $test->get("post_title");
		}
		$a['test']			= $t;
		
		// Курс
		$course				= [];
		$cours	= get_the_terms($art->id, BIO_COURSE_TYPE);
		$cour	= $cours[0];
		if($cour)
		{
			$course = static::get_course($cour);			
		}
		$a["course"]		= $course;
		
		$user				= get_user_by("id", $art->get("post_author"));
		$auth				= [];
		$auth["id"]			= $user->ID;
		$auth["display_name"]= $user->display_name;
		$a['post_author']	= $auth;
		return $a;
	}
	static function get_test( $p, $is_full = false )
	{
		$id = is_numeric($p) ? $p : $p->ID;			
		//Bio_Test_API::test_refresh( $id );
		return static::make_test($p, $is_full);
		//return static::make_test($p);
	}
	
	static function make_test( $p, $is_full = false )
	{
		//$result = self::memcached()->get("bio_rest|test|{$p}");
		//if (false && !$result)
		//{
			
			$art				= Bio_Test::get_instance($p);
			$a					= $is_full ? Bio_Test_API::test_get( $art->id ) : [];
			$a['id']			= $art->id;
			$a['post_title']	= $art->get("post_title");
			$a['post_date']		= date_i18n( 'j F Y', strtotime( $art->get("post_date") ) );
			$a['is_timed']		= $art->get_meta("is_timed");
			$a['duration']		= $art->get_meta("duration");
			
			// Курс
			$course				= [];
			$cours	= get_the_terms($art->id, BIO_COURSE_TYPE);
			$cour	= $cours[0];
			if($cour)
			{
				$course = static::get_course($cour);			
			}
			$a["course"]		= $course;
			
			// Тип Олимпиад
			$course				= [];
			$cours	= get_the_terms($art->id, BIO_OLIMPIAD_TYPE_TYPE);
			$cour	= $cours[0];
			if($cour)
			{
				$course = static::get_course($cour);			
			}
			$a["bio_olimpiad_type"]		= $course;
			
			// Класс
			$classes			= [];
			$clss	= get_the_terms( $art->id, BIO_CLASS_TYPE );
			$cls	= $clss[0];
			if($cls)
			{
				$class = [
					"id"			=> $cls->term_id,
					"post_title"	=> $cls->name
				];
			}			
			$a["_class"]		= $class;
			/**/
			
			
			// Темы
			$classes			= [];
			$clss	= get_the_terms( $art->id, BIO_BIOLOGY_THEME_TYPE );
			$cls	= $clss[0];
			if($cls)
			{
				$bio_theme = [
					"id"			=> $cls->term_id,
					"post_title"	=> $cls->name
				];
			}			
			$a["bio_theme"]		= $bio_theme;
			/**/
			
			$user				= get_user_by("id", $art->get("post_author"));
			$auth				= [];
			$auth["id"]			= $user->ID;
			$auth["display_name"]= $user->display_name;
			$a['post_author']	= $auth;			
			
			/**/
			
			$result = $a;
		//}
		//self::memcached()->set("bio_rest|test|{$p}", $result);
		return $result;
	}
	
	static function flush_test( $p )
	{
		self::memcached()->delete("bio_rest|test|{$p}");
	}

	static function get_course($p)
	{
		if(is_numeric($p))
		{
			$course = get_term($p, BIO_COURSE_TYPE);
		}
		else
		{
			$course = $p;
		}
		$c = [];
		if(is_wp_error($course) || !$course)
			return $c;
		$c['id']			= $course->term_id;
		$c['post_title']	= $course->name;
		$c['post_content']	= $course->description;
		$c['icon_id']		= get_term_meta( $course->term_id, "icon", true);
		$c['icon']			= wp_get_attachment_image_src( $c['icon_id'], "full" )[0];
		$c['adress']		= get_term_meta( $course->term_id, "adress", true);
		$c['locked']		= (int)get_term_meta( $course->term_id, "locked", true);
		$author				= get_user_by("id", get_term_meta( $course->term_id, "author", true));
		$c['author']		= [
			"id"			=> $author->ID,
			"display_name"	=> $author->display_name,		
		];
		
		return $c;
	}
    static function get_class($class)
    {
        if(is_numeric($class))
        {
            $class = get_term($class, BIO_CLASS_TYPE);

        }
        $c = [];
        $c['id']			= $class->term_id;
        $c['post_title']	= $class->name;
        return $c;

    }

	static function get_classes($p)
	{
		if(is_numeric($p))
		{
			$course = get_term($p, BIO_CLASS_TYPE);
		}
		else
		{
			$course = $p;
		}
		$c = [];
		if(is_wp_error($course) || !$course)
			return $c;

		$c['id']			= $course->term_id;
		$c['post_title']	= $course->name;
		$c['post_content']	= $course->description;
		
		return $c;
	}

	static function get_bio_themes($p)
	{
		if(is_numeric($p))
		{
			$course = get_term($p, BIO_BIOLOGY_THEME_TYPE);
		}
		else
		{
			$course = $p;
		}
		$c = [];
		if(is_wp_error($course) || !$course)
			return $c;
		$c['id']			= $course->term_id;
		$c['post_title']	= $course->name;
		$c['post_content']	= $course->description;
		$c['articles_count']= Bio_Course::get_article_count($course->term_id);
		$c['tests_count']	= Bio_Course::get_test_count($course->term_id);
		$c['count']			= Bio_Course::get_article_count($course->term_id);
		$c['icon_id']		= get_term_meta( $course->term_id, "icon", true);
		$c['icon']			= wp_get_attachment_image_src($c['icon_id'], "full")[0];
		return $c;
	}

	static function get_olimpiad_type($p)
	{
		if(is_numeric($p))
		{
			$course = get_term($p, BIO_OLIMPIAD_TYPE_TYPE);
		}
		else
		{
			$course = $p;
		}
		$c = [];
		if(is_wp_error($course) || !$course)
			return $c;
		$c['id']			= $course->term_id;
		$c['post_title']	= $course->name;
		$c['post_content']	= $course->description;
		$c['articles_count']= Bio_Course::get_article_count($course->term_id);
		$c['count']			= Bio_Course::get_article_count($course->term_id);
		$c['icon_id']		= get_term_meta( $course->term_id, "icon", true);
		$c['icon']			= wp_get_attachment_image_src($c['icon_id'], "full")[0];
		return $c;
	}

	static function get_category($p)
	{
		if(is_numeric($p))
		{
			$course = get_term($p, "category");
		}
		else
		{
			$course = $p;
		}
		$c = [];
		if(is_wp_error($course) || !$course)
			return $c;
		$c['id']			= $course->term_id;
		$c['post_title']	= $course->name;
		$c['icon_id']		= get_term_meta( $course->term_id, "icon", true);
		$c['icon']			= wp_get_attachment_image_src($c['icon_id'], "full")[0];
		return $c;
	}

	static function get_question($id)
	{
		$question = Bio_Question::get_instance($id);
		$q = [
			"post_title"	=> $question->get("post_title"),
			"post_content"	=> $question->get("post_content"),
			"trumbnail"		=> get_the_post_thumbnail_url( $id, "full" ),
		];
		$q['answers']	= [];
		return $q;
		
	}

	static function get_menu()
	{
		$menu_name 	= 'primary';
		$locations 	= get_nav_menu_locations();
		$menus 		= [];
		if( $locations && isset($locations[ $menu_name ]) )
		{
			$menu 		= wp_get_nav_menu_object( $locations[ $menu_name ] );
			$menu_items = wp_get_nav_menu_items( $menu );
			foreach($menu_items as $m)
			{
				$menus[] = [
					"title"			=> $m->title, 
					"post_name"		=> $m->post_name, 
					"menu_order"	=> $m->menu_order, 
					"object"		=> $m->object,
					"type"			=> $m->type,
					"object_id"		=> $m->object_id,
					"post_parent"	=> $m->post_parent
				];
			}
		}
		return $menus;
	}
	static function parse_classes($cls, $is_set = true)
	{
		$classes= [];
		$all 	= Bio_Class::get_all();
		$i 		= 0;
		foreach($all as $class)
		{
			if($cls[$i])
				$classes[] = $is_set ? $class->term_id : $class;
			$i++;
		}
		return $classes;
	}
	static function parse_rules( $cls )
	{
		$rules = [];
		$i 		= 0;
		foreach( get_full_bio_roles() as $r)
		{
			if($cls[$i])
				$rules[] = $r[0];
			$i++;
		}
		return $rules;
	}
	static function get_lead_all_courses()
	{
		$all = Bio_Course::get_all();
		$courses = [];
		foreach($all as $a)
		{
			$courses[] 	= [
				"post_title"	=> $a->name,
				"id"			=> $a->term_id
			];
		}
		return $courses;
	}
	
	static function insert_media($data, $name, $post_id =0, $return ="html")
	{
		if(!function_exists('wp_handle_upload'))
		{
			require_once ABSPATH . 'wp-admin/includes/image.php';
			require_once ABSPATH . 'wp-admin/includes/file.php';
			require_once ABSPATH . 'wp-admin/includes/media.php';
		}
		// get rid of everything up to and including the last comma
		$imgData1 = $data;
		$imgData1 = substr($imgData1, 1+strrpos($imgData1, ','));
		// write the decoded file
		$filePath	= ABSPATH . 'static/temp/' . $name;
		$fileURL	= ABSPATH . 'static/temp/' . $name;
		file_put_contents($filePath, base64_decode($imgData1));
		$mediaId = media_sideload_image( $fileURL, $post_id, $name, $return );
		unlink($filePath);
		return $mediaId;
	}
}



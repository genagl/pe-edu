<?php

/*Абстрактный класс*/
abstract class Bio_Api
{

    abstract static function init();
    abstract static function get_type();
    abstract static function insert($data);
    abstract static function update($data, $id);
    abstract static function delete($id);
    abstract static function get_item($id, $bool);
    abstract static function get_all($metas=-1, $numberposts=-1, $offset=0, $order_by='title', $order='DESC');
    abstract static function get_all_count();



    public static function api_action($response, $methods, $code, $pars, $user, $type)
    {
        $count		= -1;
        $data = [];



        switch($methods) {
            case "update":
                if(is_numeric($code))
                {
                    static::update($pars, $code);


                    $msg = 'success';

                    $response->id	= $data["id"];
                    $response->articles	= $data["articles"];
                    $response->msg		= $data["msg"];

                }
                else
                {
                    $msg = 'error';
                }
                break;
            case "delete":
                if(is_numeric($code))
                {
                    static::delete($code);
                    $msg = __("Article removed succesfully", BIO);

                    $response->id	= $data["id"];
                    $response->msg		= $data["msg"];
                }
                else
                {
                    $msg = 'error';
                }
                break;
            case "create":

                $article = static::insert($pars);
                $articles[]	= static::get_item( $article->body, true );
                $msg = __("Article inserted succesfully", BIO);

                $response->articles	= $data["articles"];
                $response->msg		= $data["msg"];
                break;
            case "read":
            default:
                if(is_numeric($code))
                {
                    $item			= static::get_item( $code, true );

                    $all[]	= $item;

                    $response->id	= $data["id"];
                    $response->articles	= $data["articles"];
                    $response->msg		= $data["msg"];
                }
                else
                {
                    $all  = static::get_all(
                        isset($pars['metas']) 			? $pars['metas'] 		: -1, 		// []
                        isset($pars['numberposts'])		? $pars['numberposts'] 	: -1,  		// -1
                        isset($pars['offset'])			? $pars['offset']		: 0,  		// 0
                        isset($pars['order_by'])		? $pars['order_by']		: "date", 	// 'title'
                        isset($pars['order'])			? $pars['order']		: 'DESC', 	// 'DESC'
                    );

                    $count 		= static::get_all_count();

                    $response->articles	= $data["articles"];
                    $response->count	= $data["count"];;
                    $response->numberposts	= $data['numberposts'];
                    $response->offset		= $data['offset'];
                    $response->msg		= $data["msg"];

                }
        }

    }

}
<?php
class Bio_Simple_Taxonomy extends SMC_Taxonomy
{
	static function get_type()
	{
		return BIO_SIMPLE_TAXONOMY_TYPE;
	}
	static function init()
	{
		add_action( 'init', 				array( __CLASS__, 'create_taxonomy'), 19);
		add_action( 'parent_file',			array( __CLASS__, 'tax_menu_correction'), 1);	
		add_action( 'admin_menu', 			array( __CLASS__, 'tax_add_admin_menus'), 19);		
		
	}
	static function create_taxonomy()
	{
		register_taxonomy(
			static::get_type(), 
			array( "post", BIO_ARTICLE_TYPE, BIO_EVENT_TYPE, BIO_TEST_TYPE ), 
			array(
				'label'                 => '', // определяется параметром $labels->name
				'labels'                => array(
					'name'              => __("Simple Taxonomy", BIO),
					'singular_name'     => __("Simple Taxonomy", BIO),
					'search_items'      => __('search Simple Taxonomy', BIO),
					'all_items'         => __('all Simple Taxonomys', BIO),
					'view_item '        => __('view Simple Taxonomy', BIO),
					'parent_item'       => __('parent Simple Taxonomy', BIO),
					'parent_item_colon' => __('parent Simple Taxonomy:', BIO),
					'edit_item'         => __('edit Simple Taxonomy', BIO),
					'update_item'       => __('update Simple Taxonomy', BIO),
					'add_new_item'      => __('add Simple Taxonomy', BIO),
					'new_item_name'     => __('new Simple Taxonomy Name', BIO),
					'menu_name'         => __('Simple Taxonomy', BIO),
				),
				'description'           => '', // описание таксономии
				'public'                => true,
				'publicly_queryable'    => null, // равен аргументу public
				'show_in_nav_menus'     => true, // равен аргументу public
				'show_ui'               => true, // равен аргументу public
				'show_in_menu'          => true, // равен аргументу show_ui
				'show_in_nav_menus' 	=> true,
				'show_tagcloud'         => true, // равен аргументу show_ui
				'show_in_rest'          => null, // добавить в REST API
				'rest_base'             => null, // $taxonomy
				'hierarchical'          => true,
				'update_count_callback' => '',
				'rewrite'               => true,
				//'query_var'             => $taxonomy, // название параметра запроса
				'capabilities'          => array(),
				'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
				'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
				'_builtin'              => false,
				'show_in_quick_edit'    => null, // по умолчанию значение show_ui
			) 
		);
		//wp_nav_menu_taxonomy_meta_boxes() ;
	}
	static function tax_menu_correction($parent_file) 
	{
		global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == static::get_type() )
			$parent_file = 'pe_edu_page';
		return $parent_file;
	}
	static function tax_add_admin_menus() 
	{
		add_submenu_page( 
			'pe_edu_page', 
			__("Simple Taxonomy", BIO), 
			__("Simple Taxonomy", BIO), 
			'manage_options', 
			'edit-tags.php?taxonomy=' . static::get_type()
		);
		add_meta_box( "add-".BIO_SIMPLE_TAXONOMY_TYPE."", __("Simple Taxonomy", BIO), 'wp_nav_menu_item_taxonomy_meta_box', 'nav-menus', 'side', 'default', static::get_type() );	
    }
}
	
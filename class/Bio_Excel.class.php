<?php
require_once(__DIR__ . "/vendor/autoload.php");

class Bio_Excel
{
	public static function generateExcel($data, $filename)
	{
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$spreadsheet->getActiveSheet()->fromArray($data, NULL, 'A1');
		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
		$writer->save($filename);
	}

}
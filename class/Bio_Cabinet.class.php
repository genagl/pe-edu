<?php

class Bio_Cabinet
{
	function __construct()
	{
		
	}
	static function get_tabs()
	{
		global $wpdb, $prefix;
		$arr = [
			[
				"id"	=> "lead_articles",
				"icon"	=> '<i class="fas fa-chalkboard-teacher"></i>',
				"title"	=> __("Lead Articles", BIO),
				"cap"	=> [ "administrator", "CourseLeader", "EventMember", "editor", "TestEditor" ],
			],
			[
				"id"	=> "lead_courses",
				"title"	=> __("Lead Courses", BIO),
				"icon"	=> "<i class='fas fa-user-graduate'></i>",
				"cap"	=> [ "administrator", "CourseLeader" ],
				"req"	=> "SELECT COUNT(*) FROM ".$wpdb->prefix."course_user_requests WHERE course_id IN ( SELECT term_id FROM ".$wpdb->prefix."termmeta WHERE meta_key='author' AND meta_value=" . get_current_user_id() . " )"
			],
			[
				"id"	=> "lead_events",
				"title"	=> __("Lead Events", BIO),
				"icon"	=> '<i class="fas fa-file-signature"></i>',
				"cap"	=> [ "administrator", "CourseLeader", "EventMember" ],
				"req"	=> "SELECT COUNT(*) FROM ". Bio_Event::get_table_required_name() . " WHERE post_id IN ( SELECT post_id FROM ".$wpdb->prefix."posts WHERE post_author='" . get_current_user_id() . "' )"
			],
			[
				"id"	=> "lead_tests",
				"title"	=> __("Lead Tests", BIO),
				"icon"	=> '<i class="fas fa-calendar-check"></i>',
				"cap"	=> [ "administrator", "CourseLeader", "TestEditor" ],
			],
			[
				"id"	=> "lead_questions",
				"title"	=> __("Lead questions to Tests", BIO),
				"icon"	=> '<i class="fas fa-question-circle"></i>',
				"cap"	=> [ "administrator", "CourseLeader", "TestEditor" ],
			],
			[
				"id"	=> "mailings",
				"title"	=> __("Mailings", BIO),
				"icon"	=> "<i class='fas fa-envelope'></i>",
				"cap"	=> [ "administrator", "CourseLeader", "EventMember", "editor", "TestEditor" ],
			],
			[
				'id'	=> 'separator',
				"cap"	=> [ "administrator", "CourseLeader", "EventMember", "editor", "TestEditor", "contributor" ],
			],
			[
				"id"	=> "favorites",
				"title"	=> __("Favorites", BIO),
				"icon"	=> "<i class='fas fa-th-list'></i>",
				"cap"	=> [ "contributor" ],
			],
			[
				"id"	=> "events",
				"title"	=> __("My Events", BIO),
				"icon"	=> "<i class='fas fa-calendar-alt'></i>",
				"cap"	=> [ "contributor" ],
			],
			[
				"id"	=> "courses",
				"title"	=> __("My Courses", BIO),
				"icon"	=> "<i class='fas fa-microscope'></i>",
				"cap"	=> [ "contributor" ],
			],
			[
				"id"	=> "tests",
				"title"	=> __("My Tests", BIO),
				"icon"	=> "<i class='fas fa-award'></i>",
				"cap"	=> [ "contributor" ],
			],
			[
				"id"	=> "params",
				"title"	=> __("My parameter", BIO),
				"icon"	=> "<i class='fas fa-user-cog'></i>",
				"cap"	=> [ "administrator", "CourseLeader", "EventMember", "editor", "TestEditor", "contributor" ],
			],
			
		];
		return apply_filters( "bio_cabinet_tabs", $arr);
	}
	static function get_tab($param = "params")
	{
		foreach($tabs = static::get_tabs() as $tab)
		{
			if($param == $tab['id'])
				return $tab;
		}
		return $tabs[0];
	}
	static function get_plate($page_id = "params",  $user_id = -1)
	{
		global $wpdb;
		
		$user_id 	= $user_id == -1 ? get_current_user_id(): $user_id;
		$tab 		= static::get_tab( $page_id );
		
		$html 		= "
		<div class='row'>
			<div class='col-12'>
				<div class='bio_title'>" . 
					$tab['icon'] . ' ' . $tab['title'] . 
					"<div class='seq'>$in</div>".
				"</div>
			</div>
		</div>";
		switch($page_id)
		{
			case "separator":
				break;
			case "lead_articles":
			case "lead_courses":
			case "lead_events":
			case "lead_tests":
			case "lead_questions":
			case "params":
			case "courses":
			case "favorites":
			case "tests":
			case "pupils":
			case "mailings":
				$html .= static::$page_id( $user_id );
				break;
			default:
				$html .= apply_filters("bio_cabinet_plate", "", $user_id, $page_id);
		}
		return $html;
	}
	static function lead_articles( $user_id )
	{
		return static::get_lead_articles_cab();
	}
	static function lead_courses( $user_id )
	{
		return static::get_lead_courses_cab();
	}
	static function lead_events( $user_id )
	{
		return static::get_lead_events_cab();
	}
	static function lead_tests( $user_id )
	{
		return static::get_lead_tests_cab();
	}
	static function params( $user_id )
	{
		return static::get_lead_params_cab();
	}
	static function get_lead_params_cab( $user_id=-1)
	{
		require_once BIO_REAL_PATH . "tpl/core/personal.php";
		return get_bio_cab_plate();
	}
	static function courses( $user_id )
	{
		require_once BIO_REAL_PATH . "tpl/core/courses.php";
		return get_bio_cab_plate();
	}
	static function favorites( $user_id )
	{
		return static::get_favorites_cab();		
	}
	static function tests( $user_id )
	{
		return "tests";		
	}
	static function lead_questions( $user_id )
	{
		return static::get_lead_question_cab();
	}
	static function pupils( $user_id )
	{
		return "pupils";		
	}
	static function mailings( $user_id )
	{
		return static::get_lead_mailing_cab();
	}
	
	static function get_lead_articles_cab($user_id =-1)
	{
		if($user_id ==-1) $user_id = get_current_user_id();
		$all = Bio_Article::get_all([], -1, 0, 'title', 'DESC', "", "all", "AND", $user_id);
		$a = new StdClass;
		$a->ID = -1;
		$a->post_title = __("add Article", BIO);
		$all[] = $a;
		$html = "
		<div class='alert alert-success bio_descr w-100' role='alert'>".
			__("In this section you can create new Articles and edit old ones.", BIO).
		"</div>".
		Bio_Article::wp_dropdown([
			"posts"		=> $all,
			"name"		=> "",
			"none_zero"	=> true,
			"class"		=> "form-control",
			"special"	=> "ccmd,get_lead_articles_cab"
		]);
		$html .= "<div id='cab_choose_lead_content'></div>
		
		<script>
			bio_send(['get_lead_articles_cab', $('[ccmd]').val()]);
		</script>";
		return $html;
	}
	static function get_lead_events_cab($user_id =-1)
	{
		if($user_id ==-1) $user_id = get_current_user_id();
		$all = Bio_Event::get_all([], -1, 0, 'title', 'DESC', "", "all", "AND", $user_id);
		$a = new StdClass;
		$a->ID = -1;
		$a->post_title = __("add Event", BIO);
		$all[] = $a;
		$ssel = Bio_Event::get_users_per_post( $user_id );
		$html = "
		<div class='alert alert-success bio_descr w-100' role='alert'>".
			__("In this section you can create new Events and edit old ones.", BIO).
		"</div>".
		Bio_Event::div_dropdown([
			"posts"		=> $all,
			"name"		=> "",
			"none_zero"	=> true,
			"class"		=> "btn-outline-secondary",
			"special"	=> "cocmd,get_lead_events_cab",
			"ssel"		=> $ssel
		]);
		$html .= "<div id='cab_choose_lead_content'></div>
		
		<script>
			bio_send(['get_lead_events_cab', $('[cocmd]').attr('data')]);
		</script>";
		return $html;
	}
	static function get_lead_question_cab($user_id =-1)
	{
		if($user_id ==-1) $user_id = get_current_user_id();
		$all = Bio_Question::get_all([], -1, 0, 'title', 'DESC', "", "all", "AND", $user_id);
		$a = new StdClass;
		$a->ID = -1;
		$a->post_title = __("add Question", BIO);
		$all[] = $a;
		$html = "
		<div class='alert alert-success bio_descr w-100' role='alert'>".
			__("In this section you can create new Question and edit old ones.", BIO).
		"</div>".
		Bio_Mailing::wp_dropdown([
			"posts"		=> $all,
			"name"		=> "",
			"none_zero"	=> true,
			"class"		=> "form-control",
			"special"	=> "ccmd,get_lead_question_cab"
		]);
		$html .= "<div id='cab_choose_lead_content'></div>
		
		<script>
			bio_send(['get_lead_question_cab', $('[ccmd]').val()]);
		</script>";
		return $html;
	}
	static function get_lead_mailing_cab($user_id =-1)
	{
		if($user_id ==-1) $user_id = get_current_user_id();
		$all = Bio_Mailing::get_all([], -1, 0, 'title', 'DESC', "", "all", "AND", $user_id);
		$a = new StdClass;
		$a->ID = -1;
		$a->post_title = __("add Mailing", BIO);
		$all[] = $a;
		$html = "
		<div class='alert alert-success bio_descr w-100' role='alert'>".
			__("In this section you can create new Mailing and edit old ones.", BIO).
		"</div>".
		Bio_Mailing::wp_dropdown([
			"posts"		=> $all,
			"name"		=> "",
			"none_zero"	=> true,
			"class"		=> "form-control",
			"special"	=> "ccmd,get_lead_mailing_cab"
		]);
		$html .= "<div id='cab_choose_lead_content'></div>
		
		<script>
			bio_send(['get_lead_mailing_cab', $('[ccmd]').val()]);
		</script>";
		return $html;
	}
	
	
	static function get_lead_tests_cab($user_id =-1)
	{
		if($user_id ==-1) $user_id = get_current_user_id();
		$all = Bio_Test::get_all([], -1, 0, 'title', 'DESC', "", "all", "AND", $user_id);
		$a = new StdClass;
		$a->ID = -1;
		$a->post_title = __("add Test", BIO);
		$all[] = $a;
		$html = "
		<div class='alert alert-success bio_descr w-100' role='alert'>".
			__("In this section you can create new Tests and edit old ones.", BIO).
		"</div>".
		Bio_Test::wp_dropdown([
			"posts"		=> $all,
			"name"		=> "",
			"none_zero"	=> true,
			"class"		=> "form-control",
			"special"	=> "ccmd,get_lead_tests_cab"
		]);
		$html .= "<div id='cab_choose_lead_content'></div>
		
		<script>
			bio_send(['get_lead_tests_cab', $('[ccmd]').val()]);
		</script>";
		return $html;
	}
	static function get_lead_courses_cab()
	{
		$terms = get_terms( array(
			'taxonomy'      => BIO_COURSE_TYPE, 
			'orderby'       => 'name', 
			'order'         => 'ASC',
			'hide_empty'    => false, 
			'fields'        => 'all', 
			'meta_query'    => [
				[
					"key"	=> "author",
					"value"	=> get_current_user_id()
				]
			]
		) );
		$selus 	= Bio_User::get_users_per_courses( get_current_user_id() );
		$i=0;
		if( count($terms) )
			foreach($terms as $term)
			{
				$ss		= count($selus[ $term->term_id ]['users']);
				$ss		= $ss == 0 ? "" : "<div class='indic' >$ss</div>";
				$ddds 	.= " 
				<a class='dropdown-item' href='#' cocmd='cab_choose_lead_course' data='" . $term->term_id . "'>".
					$term->name. $ss.
				"</a>";
				if($i==0) $label = $term->name. $ss;
				$i++;
			}
		$label = $label ? $label : "--";
		$html = "
		<div class='btn-group w-100'>
			<div class='btn btn-outline-secondary dropdown-toggle w-100 text-left' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' >".
				$label . 
			"</div>
			<div class='dropdown-menu w-100'>";		
		$html .= $ddds . "
				<a class='dropdown-item ' href='#' cocmd='cab_choose_lead_course' data='-1'>" . 
					__('add new Course', BIO) . 
				"</a>
			</div>
		</div>";	
			
		/*
		//$html 		= "<select name='lead_courses_cab' class='form-control' ccmd='cab_choose_lead_course'>";
		if(count($terms))
		{
			foreach($terms as $term)
			{
				//$html	.= "<option value='".$term->term_id."'>".$term->name."</option>";
			}
		}
		*/
		$html .= "
			<!--option value='-1'>" . __("New", BIO) . "</option>
		</select-->
		<div id='cab_choose_lead_content'></div>
		<script>
			bio_send(['cab_choose_lead_course', $('[cocmd]').attr('data') ]);	
		</script>";
		return $html;
	}
		
	static function get_favorites_cab($user_id=-1)
	{
		global $wpdb;
		$user_id 	= $user_id == -1 ? get_current_user_id(): $user_id;
		$art_ids	= Bio_User::get_favorites($user_id);
		$html		= " ";
		if(count( $art_ids ))
			foreach($art_ids as $art_id)
			{
				$article	= Bio_Article::get_instance( $art_id->post_id );
				$html 		.= $article->draw();
			}
		else
			$html .= "
			<div class='alert alert-success text-center w-100' role='alert'>".
				__("No articles in favorites", BIO).
				"<div class='bio_descr w-100'>".
					__("Select everyone Article in Main page or Course", BIO).
				"</div>".
			"</div>";
		return $html;
	}
}
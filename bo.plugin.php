<?php
/*
Plugin Name: PE Edu
Plugin URI: http://wp-ermak.ru/
Description: Protopia Ecosystem remote education server
Version: 0.0.1
Author: Genagl
Author URI: http://wp-ermak.ru/author
License: GPL2
Text Domain:   bio
Domain Path:   /lang/
*/
/*  Copyright 2018  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/ 
error_reporting(E_ERROR);
//библиотека переводов
function init_textdomain_bio() 
{ 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("bio", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
}
add_action('plugins_loaded', 'init_textdomain_bio');

//Paths
define('BIO_URLPATH', plugins_url() . '/pe-edu/');
define('BIO_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('_REAL_PATH', BIO_REAL_PATH);
define('BIO', 'bio');

//TODO rename article to bio_article
define('BIO_ARTICLE_TYPE', 'article');
define('BIO_EVENT_TYPE', 'bio_event');
define('BIO_TEST_TYPE', 'bio_test');
define('BIO_FACULTET_TYPE', 'bio_facultet');
define('BIO_QUESTION_TYPE', 'bio_question');
define('BIO_ANSWER_TYPE', 'bio_answer');
define('BIO_COURSE_TYPE', 'bio_course');
define('BIO_MAILING_TYPE', 'bio_mailing');
define('BIO_MAILING_GROUP_TYPE', 'bio_mailing_group');
define('BIO_CLASS_TYPE', 'bio_class');
define('BIO_BIOLOGY_THEME_TYPE', 'bio_biology_theme');
define('BIO_OLIMPIAD_TYPE_TYPE', 'bio_olimpiad_type');
define('BIO_ROLE_TAXONOMY_TYPE', 'bio_role_taxonomy');
define('BIO_MEDIA_TAXONOMY_TYPE', 'bio_media_taxonomy');
define('BIO_TEST_CATEGORY_TYPE', 'bio_test_category');

//add to MSchool
define('BIO_TARIF_TYPE', 'bio_tarif');
define('BIO_MESSAGE_TYPE', 'bio_message');
define('BIO_DISCOUNT_TYPE', 'bio_discount');
define('BIO_NOTIFICATION_TYPE', 'bio_notification');
define('BIO_AVATAR_TYPE', 'bio_avatar');
define('BIO_IMAGE_RESULT_TYPE', 'bio_image_result');
define('BIO_COMMENT_TYPE', 'bio_comment');
define('BIO_ADVANTAGE_TYPE', 'bio_advantage');

define('BIO_EMAIL', "noreply@bio.kb.protopia-home.ru");
define('BIO_EMPTY_IMG', BIO_URLPATH . "assets/img/empty.png");

define("BIO_DEFAULT_ORDER", 0);
define("BIO_FREE_TEST_ORDER", 1);
define("BIO_INTRAMURAL_TEST_ORDER", 2);
define("BIO_ALPHABETICAL_ORDER", 3);

//capabilities
define("BIO_POST_CREATE", 0);
define("BIO_POST_EDIT", 0);
define("BIO_PAGE_CREATE", 0);
define("BIO_PAGE_EDIT", 0);


define("BIO_ARTICLE_CREATE", 1);
define("BIO_ARTICLE_EDIT", 2);
define("BIO_ARTICLE_DELETE", 3);

define("BIO_ARTICLE_ADD_COURSE", 4);
define("BIO_ARTICLE_ADD_CLASS", 5);
define("BIO_ARTICLE_ADD_BIOLOGY_THEME", 6);
define("BIO_ARTICLE_ADD_OLIMPIAD_TYPE", 7);
define("BIO_ARTICLE_ADD_ROLE_TAXONOMY_TYPE", 8);

define("BIO_TEST_CREATE", 9);
define("BIO_TEST_EDIT", 10);
define("BIO_TEST_DELETE", 11);

define("BIO_EVENT_CREATE", 12);
define("BIO_EVENT_EDIT", 13);
define("BIO_EVENT_DELETE", 14);

define("BIO_MAILING_CREATE", 15);
define("BIO_MAILING_EDIT", 16);
define("BIO_MAILING_DELETE", 17);

define("BIO_COURSE_CREATE", 18);
define("BIO_COURSE_EDIT", 19);
define("BIO_COURSE_DELETE", 20);

define("BIO_CLASS_CREATE", 21);
define("BIO_CLASS_EDIT", 22);
define("BIO_CLASS_DELETE", 23);

define("BIO_BIOLOGY_THEME_CREATE", 24);
define("BIO_BIOLOGY_THEME_EDIT", 25);
define("BIO_BIOLOGY_THEME_DELETE", 26);

define("BIO_OLIMPIAD_TYPE_CREATE", 27);
define("BIO_OLIMPIAD_TYPE_EDIT", 28);
define("BIO_OLIMPIAD_TYPE_DELETE", 29);

define("BIO_ROLE_TAXONOMY_CREATE", 30);
define("BIO_ROLE_TAXONOMY_EDIT", 31);
define("BIO_ROLE_TAXONOMY_DELETE", 32);

define("BIO_TEST_CATEGORY_CREATE", 33);
define("BIO_TEST_CATEGORY_EDIT", 34);
define("BIO_TEST_CATEGORY_DELETE", 35);

define("BIO_ARTICLE_REQUEST_TO_FIRST", 36);
define("BIO_EVENT_SEND_REQUEST", 37);
define("BIO_COURSE_SEND_REQUEST", 38);
define("BIO_EVENT_ACCESS_REQUEST", 39);
define("BIO_COURSE_ACCESS_REQUEST", 40);
define("BIO_ARTICLE_ADD_EVENT", 41);
define("BIO_ARTICLE_ADD_TEST", 42);
define("BIO_ARTICLE_ADD_FAVOR", 43);
define("BIO_ARTICLE_ADD_CATEGORY", 44);
define("BIO_ARTICLE_ADD_INCLUDES", 45);

define("BIO_QUESTION_CREATE", 46);
define("BIO_QUESTION_CREATE_NOT_VERIFIED", 47);
define("BIO_QUESTION_EDIT", 48);
define("BIO_QUESTION_EDIT_NOT_VERIFIED", 49);
define("BIO_QUESTION_VERIFY", 50);
define("BIO_QUESTION_DELETE", 51);

define("BIO_COURSE_AUTHOR_CHANGE", 52);
define("BIO_QUESTION_CORRECT", 53);
define("BIO_USER_DELETE", 54);

define("BIO_REVIEW_CREATE", 1);
define("BIO_REVIEW_EDIT", 1);
define("BIO_REVIEW_DELETE", 57);

define("BIO_FACULTET_CREATE", 58);
define("BIO_FACULTET_EDIT", 59);
define("BIO_FACULTET_DELETE", 60);


require_once(BIO_REAL_PATH.'class/Bio.class.php');
require_once(BIO_REAL_PATH.'class/SMC_Post.php');
require_once(BIO_REAL_PATH.'class/SMC_Taxonomy.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Conglomerath.class.php');
require_once(BIO_REAL_PATH.'class/SMC_Object_type.php');
require_once(BIO_REAL_PATH.'class/Bio_User.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Class.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Biology_Theme.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Olimpiad_Type.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Article.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Event.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Test.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Question.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Answer.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Assistants.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Facultet.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Course.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Ajax.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Messages.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Mailing.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Mailing_Group.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Category.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Role_Taxonomy.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Test_API.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Test_Category.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Page.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Post.class.php');
//
//add to MSchool
require_once(BIO_REAL_PATH.'class2/Bio_Tarif.class.php');
require_once(BIO_REAL_PATH.'class2/Bio_Message.class.php');
require_once(BIO_REAL_PATH.'class2/Bio_Discount.class.php');
require_once(BIO_REAL_PATH.'class2/Bio_Notification.class.php');
require_once(BIO_REAL_PATH.'class2/Bio_Avatar.class.php');
require_once(BIO_REAL_PATH.'class2/Bio_ImageResult.class.php');
require_once(BIO_REAL_PATH.'class2/Bio_Comment.class.php');
require_once(BIO_REAL_PATH.'class2/Bio_Advantage.class.php');
//

require_once(BIO_REAL_PATH.'class/Bio_Current_User.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Favorite_Article.class.php');
require_once(BIO_REAL_PATH.'class/Bio_Media_Taxonomy.class.php');
require_once(BIO_REAL_PATH.'class/Bio_REST_hooks.class.php');
require_once(BIO_REAL_PATH.'class/Bio_GraphQL.class.php');

if (function_exists('register_activation_hook'))
{
	register_activation_hook( __FILE__, [ "Bio",			'activate' ] );
	register_activation_hook( __FILE__, [ "Bio_Article",	'activate' ] );
	register_activation_hook( __FILE__, [ "Bio_Event", 	'activate' ] );
	register_activation_hook( __FILE__, [ "Bio_Mailing", 	'activate' ] );
	register_activation_hook( __FILE__, [ "Bio_Mailing_Group", 'activate' ] );
	register_activation_hook( __FILE__, [ "Bio_Test", 	'activate' ] );
}

if (function_exists('register_deactivation_hook'))
{
	register_deactivation_hook(__FILE__, array("Bio", 'deactivate'));
}
function get_bio_roles()
{
	return [  
		[
			"CourseLeader", 	
			__("Course Leader", BIO ),
			
		], 
		[
			"TestEditor", 		
			__("Test Editor", BIO ),
			
		],
		[
			"Teacher", 		
			__("Teacher", BIO ),
			
		],
		[
			"Pupil", 			
			__("Pupil", BIO )
		],
		[
			"HiddenUser", 		
			__("Hidden User", BIO )
		]
	];
}
function get_full_bio_roles()
{
	$arr = get_bio_roles();
	array_unshift($arr, [
		"editor",			
		__("Editor", BIO),
		
	]);
	array_unshift($arr, [
		"administrator", 	
		__("Administrator", BIO),
		
	]);
	array_push($arr, 	[
		"contributor", 	
		__("Contributor", BIO)
	]);
	return $arr;
}
function bio_cap_mask( $arr, $role="contributor" )
{
	if(!$arr) return 0;
	if(!is_array($arr))
		$arr = [$arr];
	$res = [0,0,0];
	foreach($arr as $cap)
	{
		$len 			= 30;
		$boo 			= (int)($cap / $len);
		$res[ $boo ] 	|= pow( 2, $cap % $len );
	}
	return apply_filters("bio_cap_mask", $res, $arr, $role);
}
add_action("init", function()
{
	Bio::get_instance();
	Bio_ajax::get_instance();
	Bio_Assistants::init();
	Bio_Biology_Theme::init();
	Bio_Olimpiad_Type::init();
	Bio_Test_Category::init();
	Bio_Class::init();
	Bio_User::init();
	Bio_Article::init();
	Bio_Event::init();
	Bio_Test::init();
	Bio_Question::init();
	Bio_Answer::init();
	Bio_Facultet::init();
	Bio_Course::init();
	Bio_Mailing::init();
	Bio_Mailing_Group::init();
	Bio_Category::init();
	Bio_Page::init();
	Bio_Current_User::init();
	Bio_Role_Taxonomy::init();
	Bio_Media_Taxonomy::init();
	Bio_Favorite_Article::init();
	Bio_REST_hooks::init();
	Bio_GraphQL::init();
	
	// for MSchool
	Bio_Messages::init();
	Bio_Tarif::init();
	Bio_Message::init();
	Bio_Discount::init();
	Bio_Notification::init();
	Bio_Avatar::init();
	Bio_ImageResult::init();
	Bio_Comment::init();
	Bio_Advantage::init();
	
	
	//wp_set_password( "111", 3 );
}, 1);



<?php
function load_event($data, $event_id)
{		
	require_once ( BIO_REAL_PATH . "/lib/PHPExcel/IOFactory.php"); 
	// get rid of everything up to and including the last comma
	$imgData1 = $data['data'];
	$imgData1 = substr($imgData1, 1 + strrpos($imgData1, ','));
	// write the decoded file
	$filePath	= BIO_REAL_PATH. 'temp/' . $data['media_name'];
	$fileURL	= BIO_URLPATH  . 'temp/' . $data['media_name'];
	file_put_contents($filePath, base64_decode($imgData1));
	
	// Открываем файл
	$xls = PHPExcel_IOFactory::load( $filePath );
	// Устанавливаем индекс активного листа
	$xls->setActiveSheetIndex(0);
	// Получаем активный лист
	$sheet = $xls->getActiveSheet();	
	$rowIterator = $sheet->getRowIterator();
	$boo = [];
	//
	for ($i = 1; $i <= $sheet->getHighestRow(); $i++) 
	{  
		$mu = [];
		$email = $sheet->getCellByColumnAndRow(0, $i)->getValue();
		$display_name = $sheet->getCellByColumnAndRow(1, $i)->getValue();
		if( !email_exists($email) ) 
		{
			$ID = register_new_user( sanitize_file_name($email), sanitize_email($email) );
			if(!is_wp_error($ID))
			{
				$names	= explode(" ", $display_name);			
				wp_update_user([
					"ID"			=> $ID,
					"display_name"	=> $display_name,
					"first_name"	=> $names[0],
					"last_name"		=> $names[1],
					"role"			=> "Pupil"
				]);
				
			/**/
			}
			else
			{
				continue;
			}
		}
		$user		= get_user_by( 'email', $email );
		if($event_id && $event_id > 0)
		{
			$event 	= Bio_Event::get_instance( $event_id );
			if(!$event->is_users_member( $user->ID ))
			{
				Bio_Event::set_access_request( $event_id, $user->ID );
			}
		}
		$display_name = $user->display_name;
		$credits 	= $sheet->getCellByColumnAndRow(2, $i)->getValue();
		$duration 	= $sheet->getCellByColumnAndRow(3, $i)->getValue();
		$right_count= $sheet->getCellByColumnAndRow(4, $i)->getValue();
		$members[] 		= [
			"ID"			=> $user->ID,
			"user_email"	=> $email, 
			"display_name"	=> $display_name, 
			"credits"		=> $credits, 
			"duration" 		=> $duration, 
			"right_count"	=> $right_count
		];
	}
	@unlink( $filePath );
	return $members;
}
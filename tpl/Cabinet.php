<?php

if(!is_user_logged_in())
{
	$html .= "
	<div class='alert alert-danger' role='alert'>".
		__("This page no used for you. Log in please.", BIO).
	"</div>";
	return $html;
}

require_once BIO_REAL_PATH. "class/Bio_Cabinet.class.php";


$html .= "				
	<!--div class='row'>
		<div class='col-12' >
			<div class='display-4'>" . __("Personal Cabinet", BIO) . "</div>			
		</div>
	</div-->
	<div class='row'>
		<div class='col-sm-4 col-12 bio_cab_menu' >
			<ul>";
$i	= 0;
$pid	= $_GET['cab'] ? $_GET['cab'] : "params";
// Здесь конструируется левая панель аертикальных переключателей
foreach(Bio_Cabinet::get_tabs() as $tab)
{
	global $wpdb;
	$active = $tab['id'] == $pid ? "active" : "";
	if($tab['id'] == 'separator')
	{
		$html .= "	<li class='separator'>
						<div class='spacer-5'></div>
					</li>";
	} 
	else if( count( Bio_User::is_user_roles( $tab['cap'] )) )
	{		
		$in		= $tab['req'] ? (int)$wpdb->get_var($tab['req']) : "";
		$int 	= $in ? "<div class='indic' id='indic_" . $tab['id'] . "'>$in</div>" : "";
		$html 	.= " <li class='$active'>
						<a href='" . $tab['id'] . "'>" . 
							$tab['icon'] . " " . $tab['title'] . $int .
							"<i class='fas fa-angle-right zz'></i>
						</a>
					</li>";
	}
	$i++;
}				
$html .= 	"</ul>			
		</div>
		<div class='col-sm-8 col-12' id='cab_plate'>" .
			// Сюда загружаются панели с контентом после нажатия кнопки слева
			Bio_Cabinet::get_plate( $pid ) .
		"</div>
	</div>
	";
	
	

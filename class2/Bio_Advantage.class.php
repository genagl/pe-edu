<?php 

class Bio_Advantage extends SMC_Post
{
	static function get_type()
	{
		return BIO_ADVANTAGE_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 10.2);	
		// add_action( 'registered_taxonomy', 	[ __CLASS__, 'registered_taxonomy' ], 10, 3 );
		parent::init();
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Advantage", BIO), // Основное название типа записи
			'singular_name'      => __("Advantage", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Advantage", BIO), 
			'all_items' 		 => __('Advantages', BIO),
			'add_new_item'       => __("add Advantage", BIO), 
			'edit_item'          => __("edit Advantage", BIO), 
			'new_item'           => __("add Advantage", BIO), 
			'view_item'          => __("see Advantage", BIO), 
			'search_items'       => __("search Advantage", BIO), 
			'not_found'          => __("no Advantages", BIO), 
			'not_found_in_trash' => __("no Advantages in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Advantages", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'taxonomies'		 => [ BIO_FACULTET_TYPE ],
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 90,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title', 'editor', "thumbnail" ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
	function registered_taxonomy( $taxonomy, $object_type, $args )
	{
		if($taxonomy == BIO_FACULTET_TYPE )
		{
			register_taxonomy_for_object_type(BIO_FACULTET_TYPE, BIO_ADVANTAGE_TYPE);
		}
	}
}
<?php 

class Bio_Tarif extends SMC_Post
{
	static function get_type()
	{
		return BIO_TARIF_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 2);	
		parent::init();
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Tarif", BIO), // Основное название типа записи
			'singular_name'      => __("Tarif", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Tarif", BIO), 
			'all_items' 		 => __('Tarifs', BIO),
			'add_new_item'       => __("add Tarif", BIO), 
			'edit_item'          => __("edit Tarif", BIO), 
			'new_item'           => __("add Tarif", BIO), 
			'view_item'          => __("see Tarif", BIO), 
			'search_items'       => __("search Tarif", BIO), 
			'not_found'          => __("no Tarifs", BIO), 
			'not_found_in_trash' => __("no Tarifs in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Tarifs", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_edu_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 4,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title' ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
}
<?php 

class Bio_Comment 
{
	static function get_type()
	{
		return BIO_COMMENT_TYPE;
	}
	static function init()
	{
		//add_action( 'wp_insert_comment', [__CLASS__,'wp_insert_comment'], 10, 2 );
		add_filter( 'manage_edit-comments_columns', [__CLASS__, 'rudr_add_comments_columns'] );
		add_filter( 'manage_comments_custom_column', [__CLASS__, 'manage_comments_custom_column'], 1, 2 );
	}
	static function manage_comments_custom_column(  $column, $comment_ID )
	{
		global $comment;
		switch ( $column ) :
			case 'response' : {
				echo $comment_ID; // or echo $comment->comment_ID;
				return;
			}
			case "disscussion_type":
				$comment = Bio_Comment::get_single_matrix( $comment_ID );
				echo $comment['discussion_type'] =="post" ? "post" : "taxonomy";
				break;
			case "disscussion_element":
				$comment = Bio_Comment::get_single_matrix( $comment_ID );
				$el = $comment['discussion_type'] =="post" 
					? 
					edit_post_link(get_post( $comment['discussion_id'])->post_title, "", "", get_post( $comment['discussion_id']))
					: 
					edit_term_link( get_term( $comment['discussion_id'])->name, '', '', get_term($comment['discussion_id']));
				break;
		endswitch;
	}
	static function rudr_add_comments_columns(  $my_cols )
	{
		unset($my_cols['response']);
		//$my_cols['disscussion_type'] = __("Disscussion Type", BIO);
		$my_cols['disscussion_element'] = __("Answer in", BIO);
		//$my_cols = ['ids'=>"IDs"];
		return  $my_cols;
	}
	static function get_all($params =-1)
	{
		if(!is_array($params))
		{
			$params = [
				"number" => $params['count'] 	? $params['count'] 	: 1000,
				"offset" => $params['offset'] 	? $params['offset'] : 0
			];
		}
		if($params['count'])
		{
			$params['number'] = $params['count'];
			unset($params['count']);
		}
		unset($params['is_admin']);
		if($params['discussion_id'])
			$params['post_id'] = $params['discussion_id'];
		unset($params['discussion_id']);
		$params = apply_filters("bio_get_comments_params", $params);
		//wp_die( $params );
		$comments = apply_filters("bio_get_comments", get_comments($params), $params);
		return $comments ;
	}
	static function get_single_matrix( $comment )
	{
		if(is_numeric($comment ))
		{
			$comment  = get_comment( $comment );
			
		}
		else
		{
			
		}
		if($comment)
		{
			$arr  = [
				"id"				=> $comment->comment_ID,
				"content"			=> $comment->comment_content,
				"discussion_id"		=> $comment->comment_post_ID,
				"discussion_type"	=> $comment->discussion_type,
				"parent_id"			=> $comment->comment_parent,
				"date"				=> strtotime($comment->comment_date),
				"is_approved"		=> $comment->comment_approved,
				"user_id"			=> $comment->user_id,
				"author"			=> $comment->comment_author,
				"email"				=> $comment->comment_author_email,
			];
			
			return $arr;
		}
		else
		{	
			return null;
		}
	}
	static function get_matrixes($params =-1 )
	{
		$all = static::get_all($params);
		$matrixes = [];
		foreach($all as $single)
		{
			//$params['discussion_type'] = $params['discussion_type'] == "taxonomy" ? "term" : $params['discussion_type'];
			//if(isset($params['discussion_type']) && $single->discussion_type != $params['discussion_type'])
			//	continue;//
			$matrixes[] = static::get_single_matrix( $single );
		}
		return $matrixes;
	}
}
var setmsg, test_timer, test_dur, test_interval;
var bio_popup = function(){};

function __(text)
{
	return text;
}	
function detach( d )
{
	(function($){
		$(d).detach();
	})(jQuery)
}
jQuery(document).ready(function($)
{	
	//
	jQuery('.datetimepicker').each(function(num,elem)
	{
		$(elem).datetimepicker({
			format:'d.m.Y H:i',
			timepicker:true,
			// startDate: $(elem).attr("min") ? $(elem).attr("min") : "-1",
			// maxDate:$(elem).attr("max"),
			// inline:true,
			lang:'ru'
		});
	});
	//bulks
	$("table[users_subscribers] input[type='checkbox'][name='users'],table[users_requires] input[type='checkbox'][name='users']").on({change:function(evt)
	{
		$(this).parents("table").find("input[type=checkbox]").prop("checked", $(this).is(":checked"));
	}});
	
	$("[role='users_requires'] select.bio_user_bulk").on({change:function(evt)
	{
		var xx = $(this).parents("[role='users_requires']").parent().find("[users_requires] input[type='checkbox']:checked");
		if(xx.length && $(this).val() !="-1")
		{
			$(this).siblings("descr").text("");
			$(this).siblings(".btn").removeClass("hidden");
		}
		else
		{
			$(this).siblings("descr").text("check somebody and continue anover.");			
			$(this).siblings(".btn").addClass("hidden");
			$(this).val(-1);
		}
	}})
	
	
	//popups
	$(".bio_conglom_user_deff").on({click:function(evt)
	{
		bio_popup( this, {title:"Ququ!", text:'Отправляемся с вами в далекое будущее, где судьба родной человеческой планеты висит на волоске. Именно поэтому люди предпринимают попытку спасения Земли.'} );
	}})
	$(".bio_user_order").on({click:function(evt)
	{
		bio_popup( this, { html:'<ul class="bio_btns"><li cmd="bio_f_rait">' + __("Raiting") + '</li><li cmd="bio_f_class">' + __("Class") + '</li></ul>'} );	
	}});
	$(".bio_aticle_req_cat").on({click:function(evt)
	{
		bio_popup( this, { html: __('Author request add to ') + "<b>" + $(this).attr("bio_cat") + "</b>"  } );	
	}});
	
	//ajax
	$(".save_menu_setting").on({click:function(evt)
	{
		var is_checks = $("[name='menu_is_check[]']").map(function( i, elem ){
			return $(elem).is(":checked") ? 1 : 0;
		});
		var labels = $("[name='menu_label[]']").map(function( i, elem ){
			return elem.value;
		});
		var uniq = $("[name='menu_uniq[]']").map(function( i, elem ){
			return elem.value;
		});
		var components = $("[name='menu_component[]']").map(function( i, elem ){
			return elem.value;
		});
		var is_archive = $("[name='menu_is_archive[]']").map(function( i, elem ){
			return $(elem).is(":checked") ? 1 : 0;
		});
		var params = $("[name='menu_params[]']").map(function( i, elem ){
			return elem.value;
		});
		var d = [];
		for(var i = 0; i < 7;i++)
		{
			var icon = $( "#menu_icon"+i+"_media_id"+i ).val();
			d.push({
				is_check:is_checks[i],
				label:labels[i],
				icon:icon,
				component:components[i],
				is_archive: is_archive[i],
				uniq: uniq[i],
				params:params[i]
			})
		}
		console.log(d);
		bio_send(['bio_options', "menu", d ]);	
	}})
	$(".bio-change-cat").on({change:function()
	{
		var cat = $(this).val();
		var id	= $(this).parents("[post_id]").attr("post_id");
		bio_send(['bio_change_cat_article', id, cat]);
	}});
	
	$("[name='export_android']").on({click:function()
	{
		bio_send(['bio_export_android']);
	}});
	
	//event request
	$("form.event_form").on({submit:function(evt)
	{
		evt.preventDefault();
		var par 		= $(this).parents("[event_id]");
		var event_id 	= par.attr("event_id");
		var data = { event_id : event_id };
		$(this).serializeArray().forEach(function( elem )
		{
			data[elem.name] = elem.value;
		});
		bio_send( ['bio_event_request', data] );	
	}})
	$(".bio_conglom_user_access").on({click:function(evt)
	{
		var post_id = $(this).parents("[post_id]").attr("post_id");
		var user_id	= $(this).parents("[user_id]").attr("user_id");
		bio_send(['bio_conglom_user_access', post_id, user_id]);
	}})
	
	$(".bio_course_user_access").on({click : function(evt)
	{
		
		var course_id = $(this).parents("[post_id]").attr("post_id");
		var user_id = $(this).parents("[user_id]").attr("user_id");
		bio_send([ "bio_course_user_access", course_id,  user_id]);	
	}})
	$(".admin_test_question_form > select").on({change:function(evt)
	{
		$question_id = $(this).parents("[admin_question_id]").attr("admin_question_id");
		bio_send([ "bio_admin_test_question", $question_id, $(this).val() ]);	
	}})
	$("[cmd]").on({click:function(evt)
	{
		bio_send([ $(this).attr("cmd") ]);	
	}})
	$("[ccmd]").on({change:function(evt)
	{
		bio_send([ $(this).attr("ccmd"), $(this).val() ]);	
	}})
	$("[cocmd]").on({click:function(evt)
	{
		bio_send([ $(this).attr("cocmd"), $(this).attr("data") ]);	
	}})
	$(".bio_add[post_id]").on({click:function(evt)
	{
		console.log("favor_article 0");
		bio_send(['favor_article', {id:$(this).attr("post_id"), chk: 0}]);
	}})
	$(".bio_added[post_id]").on({click:function(evt)
	{
		console.log("favor_article 1");
		bio_send(['favor_article', {id:$(this).attr("post_id"), chk: 1}]);
	}})
	$(".bio_test_next").on({click:function(evt)
	{
		var test_id = $(this).attr("test_id");
		var current = $(this).attr("current");
		test_dur = $(this).attr("test_dur");
		var selected= [];
		$("[name=test" + current + "]:checked").each(function()
		{
			selected.push( parseInt($(this).val()) );
		});
		
		if(test_dur)
		{
			$(this).parents(".bio_test_content").append("<div  class='progress' id='test_timer' duration='" + test_dur + "'><div class='progress-bar  bg-success progress-bar-striped progress-bar-animated' role='progressbar' aria-valuenow='75' aria-valuemin='0' aria-valuemax='100' style='width: 0%'></div></div><span id='mm'></span>");
			test_timer = 0;
			test_interval = setInterval(function()
			{
				test_timer++;
				var dl = 100 * test_timer / test_dur;
				$("#mm").text(dl);
				$("#test_timer [role='progressbar']").css({ "width": dl + "%" });
				if(test_timer == test_dur)
				{
					clearInterval(test_timer)
				}
			}, 1000);
			
		}
		if( $("[name=test" + current + "]:checked").size() || $(this).hasClass("no") )
		{	
			
			bio_send(['bio_test_next', {
				test_id:test_id, 
				current:current, 
				chk:selected,
				
			}]);
		}
		else
		{
			set_message("Choose someone variant");
			return;
		}
	}})
	$(".bio_options").on({change : function(evt)
	{
		console.log( $(this).attr("name"), $(this).val() );
		bio_send(['bio_options', $(this).attr("name"), $(this).val() ]);	
	}})
	$(".bio_cab_menu>ul>li>a").on({click : function(evt)
	{
		evt.preventDefault();
		$(".bio_cab_menu>ul>li.active").removeClass("active");
		$(this).parent().addClass("active");
		bio_send(['bio_cab_page', $(this).attr("href")]);
	}});
	//
	$("input[type=checkbox][role_user_id][role]").on({change:function(evt)
	{
		var role_user_id 	= $(this).attr("role_user_id");
		var role 			= $(this).attr("role");
		var is_check		= $(this).is(":checked") ? 1 : 0;
		bio_send(['bio_role_user_id', {role_user_id: role_user_id, role: role, is_check: is_check}]);
	}})
	$("[publish_id]").on({change:function(evt)
	{
		bio_send(['bio_publish_id',  $(this).attr("publish_id"),  $(this).is(":checked") ? 1 : 0]);
	}});
	$(".add_course[cid]").on({click:function(evt)
	{
		bio_send(['add_course', $(this).attr("cid")]);
	}})
	$(".leave_req[cid]").on({click:function(evt)
	{
		bio_send(['leave_req', $(this).attr("cid")]);
	}})
	$(".leave_course[cid]").on({click:function(evt)
	{
		bio_send(['leave_course', $(this).attr("cid")]);
	}})
	$("form.cab_edit").on({submit:function(evt)
	{
		evt.preventDefault();
		var par = $(this).children().parents("[post_id][command]");
		var post_id 	= par.attr("post_id");
		var command 	= par.attr("command");
		var data = {};
		$(this).serializeArray().forEach(function( elem )
		{
			// console.log( elem.value );
			// console.log( $(elem).map(function(i,el){ return $(el).val() } ) );
			data[elem.name] = elem.value;
		});
		//console.log($(this).serializeArray());
		bio_send([command, data, post_id]);
	}})
	
	
	
	$(".bio_close_answer").on({ click:function(evt)
	{
		$("[name=answer_count]").val( $(this).parents("ul").children("li:not(.bio-hidden)").size() - 1 );
		$(this).parents("li").detach();	
	}})
	$(".bio_add_answer").on({ click : function(evt)
	{
		var n = $("ul.bio_ans-form > li").size();
		var clone = $($("[ans_num=-1]").clone());
		clone.find("#answer_thumbnail_media_id-1")
			.attr("id","answer_thumbnail_media_id" + n)	
				.attr("name","answer_thumbnail[" + n + "]");	
		clone.find("#answer_thumbnail-1")
			.attr("id","answer_thumbnail" + n)	;	
		clone.find("[image_id=-1]").attr("image_id", n) ;	
		clone.find("[name='answer_id[-1]']").attr("name", "answer_id["+n+"]") ;	
		clone.find("[name='answer_text[-1]']").attr("name", "answer_text["+n+"]") ;	
		clone.find("[name='answer_is_right[-1]']").attr("name", "answer_is_right["+n+"]") ;	
		$("ul.bio_ans-form").append( clone );
		clone.attr("ans_num", n)
			.removeClass("bio-hidden")
				.fadeIn('slow');
		$("[name=answer_count]").val( n + 1);
	}});
	
	
	//dropdown bootstrap
	$(".btn-group a.dropdown-item").on({click:function(evt)
	{
		evt.preventDefault();
		var par = $(this).parents(".btn-group").find("[data-toggle='dropdown']");
		par.html($(this).html());
		var dat = {
			cur:this,
			par: par
		};
		var customEvent = new CustomEvent("_bio_dropdown_", {bubbles : true, cancelable : true, detail : dat})
		document.documentElement.dispatchEvent(customEvent);	
	}})
	
	// free action execute as Admin
	$("[exec_ajax]").on({click:function(evt)
	{
		var method = $(this).attr("exec_ajax");
		var arg1 = $(this).attr("arg1");
		var arg2 = $(this).attr("arg2");
		bio_send(['bio_exec_ajax', method, arg1, arg2 ]);
	}});
	
	//media
		
	var prefix, height, width;
	var cur_upload_id = 1;
	$( ".my_image_upload" ).on({click:function(evt) 
	{
		var cur_upload_id = $(this).attr("image_id");
		prefix = $(this).attr("prefix"); 
		height = $(this).attr("height") ? $(this).attr("height") : 90; 
		width = $(this).attr("width") ? $(this).attr("width") : 'auto'; 
		var downloadingImage = new Image();
		downloadingImage.cur_upload_id = cur_upload_id;
		on_insert_media = function(json)
		{
			$( "#" + prefix +"_media_id" + cur_upload_id ).val(json.id);
			downloadingImage.onload = function()
			{		
				$("#" + prefix + this.cur_upload_id).empty().append("<img src=\'"+this.src+"\' width='auto' height='"+height+"'>");
				$("#" + prefix + this.cur_upload_id).css({"height":""+height+"px", "width": width });
				var dat = {
					prefix :  prefix,
					src : this.src,
					cur_upload_id : this.cur_upload_id,
					id: json.id
				};
				var customEvent = new CustomEvent("my_image_upload", {bubbles : true, cancelable : true, detail : dat});
				document.documentElement.dispatchEvent(customEvent);
				
			};
			downloadingImage.src = json.url;		
			//
		}
		open_media_uploader_image();						
	}});
	$( ".my_image_upload2" ).on({click:function(evt) 
	{
		var cur_upload_id = $(this).attr("image_id");
		prefix = $(this).attr("prefix"); 
		on_insert_media = function(json)
		{
			$( "#" + prefix +"_media_id" + cur_upload_id ).val(json.id);
			var name = json.url.substring(json.url.lastIndexOf("/")+1);
			var ext = json.url.substring(json.url.lastIndexOf(".")+1);
			$("#" + prefix + cur_upload_id)
				.empty()
					.append(
						"<p><small>" + name + "</small> </p> <span class='fi fi-"+ext+" fi-size-xs'> <span class='fi-content'>"+ext+"</span> </span>"
					);
		}
		open_media_uploader_image();						
	}});
	$( ".my_image_upload" ).each(function(num,elem)
	{
		prefix = $(this).attr("prefix");// "pic_example";
		if($(elem).attr("height"))
			$(elem).height($(elem).attr("height"));
		else
			$( elem ).height( $("#" + prefix  + $(elem).attr("image_id")).height() + 0);
	})
	 
	var media_uploader = null;
	function open_media_uploader_image()
	{
		media_uploader = wp.media({
			frame:    "post", 
			state:    "insert", 
			multiple: false
		});
		media_uploader.on("insert", function()
		{
			var json = media_uploader.state().get("selection").first().toJSON();

			var image_url = json.url;
			var image_caption = json.caption;
			var image_title = json.title;
			on_insert_media(json);
		});
		media_uploader.open();
	}
	
	bio_popup = function(btn, data)
	{
		$("body > #bio_popup").detach();	
		$("body").append("<div id='bio_popup'></div>");	
		$("body").append("<div id='bio_popup_back'></div>");	
		if(data.title)
			$("#bio_popup").append("<subtitle>" + data.title + "</subtitle>");
		if( data.text)
			$("#bio_popup").append("<div class='w-100'>" + data.text + "</div>");
		if(data.html)
			$("#bio_popup").append("<div class='w-100'>" + data.html + "</div>");
		var offset 	= $(btn).offset();	
		var width	= $(btn).width();
		var height	= $(btn).height();
		$("#bio_popup").offset({
			top:  offset.top - 235,
			left: offset.left - height/2
		});
		$("#bio_popup_back").click(function(evt)
		{
			$("#bio_popup").detach();
			$("#bio_popup_back").detach();
		});
	}
	
	$(".bio-separator").parent().parent().css({ pointerEvents: "none", cursor:"none" });
	$("body").append("<div class='msgc' id='msgc'></div>");
	$(".msg .close").on({click:function(evt)
	{
		$(this).parents(".msg").detach();	
	}});
	
	document.documentElement.addEventListener("my_image_upload", function(evt) 
	{
		if( evt.detail.prefix == "default_img" )
		{
			bio_send(['bio_options', "default_img", evt.detail.id  ]);
		}
		if( evt.detail.prefix == "404" )
		{
			bio_send(['bio_options', "404", evt.detail.id  ]);
		}
	})
})
function set_message(text)
{
	//jQuery(".msg").detach();
	clearTimeout(setmsg);
	var msg = jQuery("<div class='msg' id='msg_"+msgn+"'><div class='close'><span aria-hidden='true'>&times;</span></div>" + text + "</div>").appendTo("#msgc").hide().fadeIn("slow");
	setTimeout( 
		function(msg) 
		{
			msg.fadeOut(700, msg.detach());
		}, 
		6000, 
		$("#msg_" + msgn)
	);
	msgn++;
}
var msgn=0;

/**
 * http://stackoverflow.com/a/10997390/11236
 */
function updateURLParameter(url, param, paramVal)
{
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (var i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }
    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;	
}
function updateURLSlash(url, param, paramVal)
{
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];	
	var paramsArray = baseURL.split( param + "/" );
	baseURL = paramsArray[0];
	//console.log(paramsArray[0]);
    var additionalURL = tempArray[1]; 
    var temp = "";
	if (additionalURL) 
	{
        tempArray = additionalURL.split("&");
        for (var i=0; i<tempArray.length; i++)
		{
            if(tempArray[i].split('=')[0] != param)
			{
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }
	if( newAdditionalURL !== "" )
		newAdditionalURL = "?" + newAdditionalURL;
    return baseURL  + param + "/" + paramVal + newAdditionalURL;
}

function conf_cab_tab()
{
	var dd = jQuery('.btn-group.btn-group-toggle > label.btn-link:first-of-type');
	dd.addClass('active');	
	bio_send( [ dd.attr('cmd') ] );
}
var body_wait;
function bio_send( params, type )
{
	console.log(params, type);
	jQuery("body")
		.addClass("blocked")
			.append(jQuery("<div class='bio_wait' id='wait'><i class='fas fa-sync-alt fa-spin'></i></div>")
				.fadeIn("slow"));
	body_wait = setTimeout(function()
	{
		jQuery("body").removeClass("blocked").remove("#wait");
		jQuery("#wait").detach();
	}, 7000);
	jQuery.post	(
		myajax.url,
		{
			action	: 'myajax',
			nonce	: myajax.nonce,
			params	: params
		},
		function( response ) 
		{
			console.log(response);
			try
			{
				var dat = JSON.parse(response);
			}
			catch (e)
			{
				return;
			}
			//alert(dat);
			var command	= dat[0];
			var datas	= dat[1];
			//console.log(command);
			switch(command)
			{
				case "test":
					break;
				case "bio_article_more":
					jQuery("[aid=" + datas['id'] + "]").empty().append(datas['text']);
					break;
				case "bio_conglom_user_access":
					jQuery("#pills-tabContent .card-body").empty().append( datas['text'] );
					if(datas["indic"] == 0)
						jQuery("#" + datas['indic_id']).hide();
					else
						jQuery("#" + datas['indic_id']).text(datas["indic"]);
					break;
				case "bio_course_user_access":
					jQuery("#indic_lead_courses").text(datas['indic']);
					jQuery("#course_pupils").empty().append(datas['text']).hide().fadeIn("slow");
					break;
				case "bio_admin_test_question":
					jQuery(".test_question_cont .admin_test_question_form:eq(" + datas['n'] + ")")
						.empty()
							.append(datas['text']);
					break;
				case "bio_edit_mailing":
				case "get_lead_articles_cab":
				case "get_lead_events_cab":
				case "get_lead_mailing_cab":
				case "get_lead_tests_cab":
				case "get_lead_question_cab":
				case "cab_choose_lead_course":
					jQuery("#cab_choose_lead_content").empty().append(datas['text']).hide().fadeIn("slow");
					break;
				case "get_lead_courses_cab":
				case "get_my_courses_cab":
					jQuery("#cab_tab_cont").empty().append(datas['text']).hide().fadeIn("slow");
					break;
				case "add_course":
					jQuery("#add").empty().append(datas['text']).hide().fadeIn("slow");
					break;
				case "leave_course":
				case "leave_req":
					jQuery("#add").empty().append(datas['text']).hide().fadeIn("slow");
					break;
				case "bio_cab_page":
					jQuery("#cab_plate").empty().append(datas['text']).hide().fadeIn("slow");
					window.history.replaceState('','', updateURLParameter(window.location.href, "cab", datas['page_id']));
					break;
				case "bio_test_next":
					jQuery("#bio_test_content").children().each(function(num, elem)
					{
						console.log( elem );
						if($(elem).attr("id") != "test_timer" )
							$(elem).detach();
					});
					jQuery("#bio_test_content").append(datas['text']);//
					break;
				case "favor_article":
						var cl = datas['chk'] == 0 ? ["bio_add", "bio_added"] : ["bio_added", "bio_add"];
						var ajax_clicked = $("[post_id=" +datas['id']+"]")
						ajax_clicked.removeClass(cl[0]).addClass(cl[1]).html(datas['label']);
					break;
				default:
					var customEvent = new CustomEvent("_bio_send_", {bubbles : true, cancelable : true, detail : dat})
					document.documentElement.dispatchEvent(customEvent);					
					break;
			}			
			if(datas['exec'] && datas['exec'] != "")
			{
				window[datas['exec']](datas['args']);
			}
			if(datas['a_alert'])
			{
				alert(datas['a_alert']);
			}
			if(datas.msg)
			{
				set_message(datas.msg)
			}
			jQuery("body").removeClass("blocked").remove("#wait");
			jQuery("#wait").detach();
		}		
	);
}  
<?php
/**
 * Festival page template (for FestPage)
 *
 * @since    1.0.0
 */ 

header("Cache-Control: no-store, no-cache, must-revalidate");?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<html <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Open+Sans:300,400,600,700" rel="stylesheet">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php wp_head(); ?>		
		<?php do_action("ermak_body_script"); ?>
		
	</head>
	<body id="<?php print get_stylesheet(); ?>" <?php body_class(); ?>> 
		<?php //FmRU::$options["include_header"] ? get_header() : ""; ?>

		<?php //phpinfo() ?>
		<?php //require_once( BIO_REAL_PATH . "/lib/example.php"); ?>
		<?php echo apply_filters( 'ermak_body_before', "" ); 
		wp_footer(); ?>
	</body>
	<script>
		
	</script>
